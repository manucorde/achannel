﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports AChannel
Imports System.Data.DataTableExtensions

Module Module1
#Region "Respuesta"
    '_cmd.Parameters.Add("PO_COD_ERR", SqlDbType.VarChar).Direction = ParameterDirection.Output
    '_cmd.Parameters.Add("PO_DESC_ERR", SqlDbType.VarChar).Direction = ParameterDirection.Output
    '_cmd.Parameters.Add("PO_SECUENCIA", SqlDbType.Number).Direction = ParameterDirection.Output

    Private _codError As String
    Public Property CodigoError() As String
        Get
            Return _codError
        End Get
        Set(ByVal value As String)
            _codError = value
        End Set
    End Property

    Private _errorDescription As String
    Public Property ErrorDescription() As String
        Get
            Return _errorDescription
        End Get
        Set(ByVal value As String)
            _errorDescription = value
        End Set
    End Property

    Private _secuencia As Integer
    Public Property Secuencia() As Integer
        Get
            Return _secuencia
        End Get
        Set(ByVal value As Integer)
            _secuencia = value
        End Set
    End Property

#End Region
    Sub Main()
        Dim _conn As SqlConnection = Nothing
        Dim _connString As String = "User Id=apayment;Password=apayment;Direct=true;Data Source=192.168.0.106;Port=1521;SID=AFUNDS"
        Try
            Dim _tmp As String = ConfigurationManager.ConnectionStrings("ALoyalConnection").ConnectionString
            If _tmp <> String.Empty Then
                _connString = _tmp
            End If

            _conn = New SqlConnection(_connString)
            Dim _cmd As New SqlCommand
            Console.WriteLine("DataSource:{0}", _conn.DataSource)

            _conn.Open()
            _cmd.Connection = _conn

            Dim customerDocType As Integer = 1
            Dim customerDocNumber As String = "028-0009939-8"
            Dim productType As Integer = AChannel.Core.Enumerations.ProductTypes.PesosCaribe
            Dim productId As String = "4249600000013557"
            CodigoError = 0
            ErrorDescription = String.Empty
            Secuencia = 0


            Dim db As New AChannel.Data.ALoyalDBDataContext(_connString)

            Dim result = db.AL_getProductData(productType, productId, CodigoError, ErrorDescription, Secuencia).ToList

            Console.WriteLine(String.Format("Cantidad de registros: {0}", result.Count))
            Console.WriteLine("Columnas Respuesta ----------------")
            For Each c In result
                Console.WriteLine(String.Format("Nombre: {0} Balance:{1} ProductID:{2}", c.customerName, c.availableBal, c.productID))
            Next
        Catch ex As Exception

            Console.WriteLine(ex.ToString)
            Console.WriteLine(ex.StackTrace)
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try
        Console.ReadLine()
    End Sub

End Module
