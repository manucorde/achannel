USE [ALoyal]
GO
alter PROCEDURE AL_getProductData
	@productType		int ,
    @productId          VARCHAR (max),
    @CodigoError	    int out,
    @ErrorDescription	VARCHAR(max) out,
    @Secuencia	        int OUT,
    @DatosRespuesta     CURSOR VARYING OUT
AS 
   /*
   *   Generated by SQL Server Migration Assistant for Oracle.
   *   Contact ora2sql@microsoft.com or visit http://www.microsoft.com/sql/migration for more information.
   */
   BEGIN
	   declare 
			@errornumber int,
            @errormessage nvarchar(4000),
            @exceptionidentifier nvarchar(4000)
			
		BEGIN TRY
			SET NOCOUNT ON;
			SET @DatosRespuesta = CURSOR
				FORWARD_ONLY STATIC FOR
				SELECT @productType, @productID, 1 as currency, c.descripcion as customerName, balance_online as availablebal
				   FROM dbo.productos_clientes p
				   left outer join clientes c on p.idcliente= c.idcliente;
				  OPEN @DatosRespuesta;

			set @CodigoError	    = 00 ;
			set @ErrorDescription	= 'Exitoso';
			set @Secuencia	        = @@IDENTITY ;

		END TRY
		BEGIN CATCH
			SET @errornumber = ERROR_NUMBER()
			SET @errormessage = ERROR_MESSAGE()
			set @CodigoError	    = 39;
			set @ErrorDescription	= @errormessage;
			set @Secuencia	        = @@IDENTITY ;
          INSERT dbo.ERRORES(FECHA, FUENTE, ERR_NUM, ERR_MSG, SEVERIDAD)
          VALUES (getdate(), 'AL_getProductData(' + CONVERT(varchar,@secuencia)+')',  @errornumber, @errormessage, '3')	  
		END CATCH
      END
   