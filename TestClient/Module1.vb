﻿Imports AChannel.Support
Imports System.Configuration
Imports Agilisa.Security
Imports log4net
Imports System.Web.Script.Serialization

<Assembly: log4net.Config.XmlConfigurator(ConfigFileExtension:="log4net", Watch:=True)> 

Module Module1

    Dim _responseHeader As AChannel.ResponseHeader
    Dim _request As New AChannel.getAccountsRequest
    Dim svc As New AChannel.IntegrationWSSoapClient()
    Private js As JavaScriptSerializer = New JavaScriptSerializer()

    Sub Main(ByVal args() As String)
        LogManager.GetLogger("root")

        Dim sourceName As String = "JUAN PEREZ PEREZ"
        Dim Cedula As String = "001-1148089-3"
        Dim TipoDoc As String = AChannel.DocumentType.Cedula

        Dim TipoDocDestino As String = AChannel.DocumentType.Cedula
        Dim CustomerDestino As String = "001-142831-2"

        Dim _productTypeDestino As Short = AChannel.ProductTypes.CheckingAccount
        Dim _productIdDestino As String = "0011428312"

        Dim _productTypeOrigen As Short = AChannel.ProductTypes.CheckingAccount
        Dim _productIdOrigen As String = "0011428312"

        Dim destinationBankSWIFT As String = "WHITUS44XXX"

        Dim _numberOfArguments As Integer = 0
        _numberOfArguments = My.Application.CommandLineArgs.Count

        If _numberOfArguments > 0 Then
            Cedula = My.Application.CommandLineArgs(0)
            If _numberOfArguments > 1 Then
                TipoDoc = My.Application.CommandLineArgs(1)
            End If
            If _numberOfArguments > 2 Then
                CustomerDestino = My.Application.CommandLineArgs(2)
            End If
            If _numberOfArguments > 3 Then
                TipoDocDestino = My.Application.CommandLineArgs(3)
            End If
        End If

        svc.Open()

        With _request
            .customerDocNumber = Cedula
            .customerDocType = TipoDoc

            .RequestHeader = New AChannel.RequestHeader
            .RequestHeader.IPUser = "127.0.0.1"
            .RequestHeader.MarchantKey = "ABCDEF--"
            .RequestHeader.UserID = "344"
            .RequestHeader.SessionID = "i3ifr2bs1eajqhb0mj5xyx45"
        End With

        'GenerateHeader()
        'Console.ReadLine()
        'Return

        'getProductData(AChannel.ProductTypes.PesosCaribe, "4249070000023164")
        Logger.Info("-------------------------------------------------------------")



        Dim _Accounts() As AChannel.Account = getAccounts()
        Logger.Info("-------------------------------------------------------------")

        If (Not IsDBNull(_Accounts.Count)) AndAlso _Accounts.Count > 0 Then
            Dim _productType As Short = _Accounts(0).ProductType
            Dim _productId As String = _Accounts(0).ProductId
            _productTypeOrigen = _productType
            _productIdOrigen = _Accounts(0).ProductId

            If (_Accounts.Count > 1) Then
                _productTypeDestino = _Accounts(1).ProductType
                _productIdDestino = _Accounts(1).ProductId
            End If

            getAccountDetail(_productType, _productId)
            Logger.Info("-------------------------------------------------------------")

            getAccountBalance(_productType, _productId)
            Logger.Info("-------------------------------------------------------------")

            getAccountBalance(AChannel.ProductTypes.PesosCaribe, _productId) 'Buscar Pesos Caribe
            Logger.Info("-------------------------------------------------------------")

            getProductData(_productType, _productId)
            Logger.Info("-------------------------------------------------------------")

            getExchange()
            Logger.Info("-------------------------------------------------------------")

            getAccountTrans(_productType, _productId)
            Logger.Info("-------------------------------------------------------------")

            getImageCheckURL(_productType, _productId, "001", New Date(2010, 1, 3))
            Logger.Info("-------------------------------------------------------------")

            LockChecks(_productType, _productId, "001", "005", 1)
            Logger.Info("-------------------------------------------------------------")

            getAccountStatementPDF(_productType, _productId, 1, 1)
            Logger.Info("-------------------------------------------------------------")

        Else
            Logger.InfoFormat("No hay cuentas disponibles")
        End If

        ' Busca Prestamos
        Dim _Loans() As AChannel.Loan = getLoans()
        Logger.Info("-------------------------------------------------------------")

        If _Loans.Count > 0 Then
            Dim _productType As Short = AChannel.ProductTypes.Loan
            Dim _productId As String = _Loans(0).ProductId

            getLoanDetail(_productType, _productId)
            Logger.Info("-------------------------------------------------------------")

        Else
            Logger.InfoFormat("No hay cuentas disponibles")
        End If

        ' Busca Certificados
        Dim _Investments() As AChannel.Investment = getInvestments()
        Logger.Info("-------------------------------------------------------------")
        If _Investments.Count > 0 Then
            Dim _productType As Short = AChannel.ProductTypes.Investment
            Dim _productId As String = _Investments(0).ProductId

            getInvestmentDetail(_productType, _productId)
            Logger.Info("-------------------------------------------------------------")
        Else
            Logger.InfoFormat("No hay cuentas disponibles")
        End If

        ' Busca todos los productos
        getProducts()
        Logger.Info("-------------------------------------------------------------")

        Dim _CreditCards() As AChannel.CreditCard = getCreditcards()
        Logger.Info("-------------------------------------------------------------")
        If _CreditCards.Count > 0 Then
            getCreditCardTrans(_CreditCards(0).productId)
            Logger.Info("-------------------------------------------------------------")
            getAdditionalCards(_CreditCards(0).productId)
            Logger.Info("-------------------------------------------------------------")
            getCreditCardDetail(_CreditCards(0).productId, 1)
            Logger.Info("-------------------------------------------------------------")
            getExtraLimitTrans(_CreditCards(0).productId)
            Logger.Info("-------------------------------------------------------------")
            getCreditCardBalance(_CreditCards(0).productId)
            Logger.Info("-------------------------------------------------------------")
            getAccountBalance(AChannel.ProductTypes.PesosCaribe, _CreditCards(0).productId)    'Buscar Pesos Caribe
            Logger.Info("-------------------------------------------------------------")
            getProductData(AChannel.ProductTypes.CreditCard, _CreditCards(0).productId)
            Logger.Info("-------------------------------------------------------------")
            getProductData(AChannel.ProductTypes.PesosCaribe, _CreditCards(0).productId)
            Logger.Info("-------------------------------------------------------------")
        End If

        getPesosCaribeAccounts()

        If _numberOfArguments > 2 Then
            _request.customerDocNumber = CustomerDestino
            _request.customerDocType = TipoDocDestino

            Dim _Accounts1() As AChannel.Account = getAccounts()
            Logger.Info("--------------------------Get Second Account-----------------------------------")

            If (Not IsDBNull(_Accounts1.Count)) AndAlso _Accounts1.Count > 0 Then
                _productTypeDestino = _Accounts1(0).ProductType
                _productIdDestino = _Accounts1(0).ProductId
            End If

        End If


        Logger.Info("-----------------------LBTR-----------------------")

        Transfer(3, TipoDoc, Cedula, _productTypeOrigen, _productIdOrigen, TipoDocDestino, CustomerDestino, "Agilisa Technologies", 0, _productTypeDestino, _productIdDestino, 348.0, 1, "Pago via LBTR", sourceName, destinationBankSWIFT)

        Logger.Info("-----------------------ACH-----------------------")

        Transfer(2, _request.customerDocType, _request.customerDocNumber, AChannel.ProductTypes.CheckingAccount, _Accounts(0).ProductId, _request.customerDocType, _request.customerDocNumber, "Agilisa Technologies", 0, AChannel.ProductTypes.CreditCard, _CreditCards(0).productId, 348.0, 1, "Pago via ACH", sourceName, destinationBankSWIFT)

        Logger.Info("--------TRANSFERENCIA FISA-----------------------")

        Transfer(1, _request.customerDocType, _request.customerDocNumber, AChannel.ProductTypes.CheckingAccount, _Accounts(0).ProductId, _request.customerDocType, _request.customerDocNumber, "Agilisa Technologies", 0, AChannel.ProductTypes.SavingAccount, _Accounts(1).ProductId, 349.0, 1, "Transferencia Fisa", sourceName, destinationBankSWIFT)

        Logger.Info("----------TRANSFERENCIA PUNTOS-------------------")

        Transfer(1, AChannel.DocumentType.Cedula, "001-0931437-7", AChannel.ProductTypes.PesosCaribe, "4021080000000033", AChannel.DocumentType.Cedula, "001-0931437-7", "Agilisa Technologies", 0, AChannel.ProductTypes.PesosCaribe, "4249610000034495", 15.0, 1, "Transferencia Puntos", sourceName, destinationBankSWIFT)

        Logger.Info("--------------PAGO DE TARJETA---------------------")

        Transfer(1, _request.customerDocType, _request.customerDocNumber, AChannel.ProductTypes.CheckingAccount, _Accounts(0).ProductId, _request.customerDocType, _request.customerDocNumber, "Agilisa Technologies", 0, AChannel.ProductTypes.CreditCard, _CreditCards(0).productId, 351.0, 1, "Pago de Tarjeta", sourceName, destinationBankSWIFT)

        Logger.Info("--------------PUNTOS A EFECTIVO-------------------")

        Transfer(1, _request.customerDocType, _request.customerDocNumber, AChannel.ProductTypes.PesosCaribe, _CreditCards(0).productId, _request.customerDocType, _request.customerDocNumber, "Agilisa Technologies", 0, AChannel.ProductTypes.CheckingAccount, _Accounts(0).ProductId, 35.0, 1, "Puntos a efectivo", sourceName, destinationBankSWIFT)

        Logger.Info("--------------PAGO DE TARJETA CON PUNTOS----------")

        Transfer(1, _request.customerDocType, _request.customerDocNumber, AChannel.ProductTypes.PesosCaribe, _CreditCards(0).productId, _request.customerDocType, _request.customerDocNumber, "Agilisa Technologies", 0, AChannel.ProductTypes.CreditCard, _CreditCards(0).productId, 354.0, 1, "Pago de Tarjeta con puntos", sourceName, destinationBankSWIFT)

        Logger.Info("--------------MASS TRANSFER-------------")

        MassTransfer(_request.customerDocType, _request.customerDocNumber)

        GetMassTransferDetail(AChannel.DocumentType.Rnc, "1-02-32240-6", 312)

        Console.ReadLine()

        svc.Close()
        Logger.Info("------ FIN")
    End Sub

    Private Function getAccounts() As AChannel.Account()
        Logger.Info("-------------------- LISTA DE CUENTAS")
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Dim _Accounts() As AChannel.Account = Nothing
        Logger.InfoFormat("Ejecutando: getAccounts(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        _responseHeader = svc.getAccounts(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _Accounts)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            Logger.InfoFormat("  >> OperationID={0} OperationDescription={1} OperationResult={2}", _responseHeader.OperationID, _responseHeader.OperationDescription, _responseHeader.OperationResult)
            If Not _Accounts Is Nothing AndAlso _Accounts.Length > 0 Then
                For Each _account As AChannel.Account In _Accounts
                    Logger.InfoFormat("  >> Cuenta:{0} Disponible:{1:N} Total:{2:N}", _account.ProductId, _account.AvailableBal, _account.TotalAmount)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

        Logger.InfoFormat("  >> Response OperationID:{0}", _responseHeader.OperationID)
        Logger.InfoFormat("  >> Response OperationResult:{0}", _responseHeader.OperationResult)
        Logger.InfoFormat("  >> Response OperationDescription:{0}", _responseHeader.OperationDescription)
        Logger.InfoFormat("  >> Response MarchantKey:{0}", _responseHeader.MarchantKey)
        Logger.InfoFormat("  >> Response UserID:{0}", _responseHeader.UserID)
        Logger.InfoFormat("  >> Response IPUser:{0}", _responseHeader.IPUser)
        Logger.InfoFormat("  >> Response SessionID:{0}", _responseHeader.SessionID)
        Logger.InfoFormat("  >> Response MessageKey:{0} ", _responseHeader.MessageKey)

        Return _Accounts
    End Function

    Private Sub getAccountDetail(ByVal _productType As Short, ByVal _productId As String)

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE DETALLE VISTA")
        Logger.InfoFormat("Ejecutando: getAccountDetail(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _AccountDetail As AChannel.AccountDetail = Nothing

        _responseHeader = svc.getAccountDetail(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, _AccountDetail)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _AccountDetail Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Balance:{1}", _AccountDetail.ProductId, _AccountDetail.AvailableBal)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getImageCheckURL(ByVal _productType As Short, ByVal _productId As String, ByVal checkNumber As String, ByVal checkDate As Date)
        SetMessageKey(New Object(3) {_request.customerDocNumber, _productId, checkNumber, checkDate}, _request.RequestHeader)

        Logger.Info("-------------------- IMAGEN DE CHEQUE")
        Logger.InfoFormat("Ejecutando: getImageCheck(Doctype={0}, DocNumber={1}, checkNumber={2}, checkDate={3})", _request.customerDocType, _request.customerDocNumber, checkNumber, checkDate)
        Dim _ImageCheck As AChannel.ImageCheck = Nothing

        _responseHeader = svc.getImageCheck(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, checkNumber, checkDate, _ImageCheck)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _ImageCheck Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Imagen Anverso:{1} Imagen Reverso:{2}", _productId, _ImageCheck.UrlCheckImageObverse, _ImageCheck.UrlCheckImageReverse)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getAccountStatementPDF(ByVal _productType As Short, ByVal _productId As String, ByVal TypeStatus As Integer, ByVal Currency As Integer)
        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- getAccountStatementPDF")
        Logger.InfoFormat("Ejecutando: getAccountStatementPDF(Doctype={0}, DocNumber={1}, TypeStatus={2}, Currency={3})", _request.customerDocType, _request.customerDocNumber, TypeStatus, Currency)
        Dim _AccountStatementPDF As AChannel.AccountStatementPDF = Nothing

        _responseHeader = svc.getAccountStatementPDF(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, TypeStatus, Currency, _AccountStatementPDF)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _AccountStatementPDF Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Url:{1}", _productId, _AccountStatementPDF.url)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub LockChecks(ByVal _productType As Short, ByVal _productId As String, ByVal fromCheck As String, ByVal toCheck As String, ByVal lockReasonID As Short)
        SetMessageKey(New Object(3) {_request.customerDocNumber, _productId, fromCheck, toCheck}, _request.RequestHeader)

        Logger.Info("-------------------- BLOQUEO DE CHEQUE")
        Logger.InfoFormat("Ejecutando: LockCheck(Doctype={0}, DocNumber={1}, checkNumber={2}, checkDate={3})", _request.customerDocType, _request.customerDocNumber, fromCheck, toCheck, lockReasonID)
        Dim _LockCheck As AChannel.LockCheck = Nothing

        _responseHeader = svc.LockChecks(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, fromCheck, toCheck, lockReasonID, _LockCheck)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _LockCheck Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} RequestId:{1}", _productId, _LockCheck.requestId)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getAccountTrans(ByVal _productType As Short, ByVal _productId As String)

        Dim _filterExpr As New AChannel.FilterExpr With {.Count = 0, .DateBegin = New Date(2010, 1, 1), .DateEnd = New Date(2010, 2, 1), .MinAmount = 0, .MaxAmount = 90000, .TransactionType = "DC"}
        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Dim _AccountTransactions() As AChannel.AccountTransaction = Nothing

        Logger.InfoFormat("-------------------- TRANSACCIONES DE CUENTA")
        Logger.InfoFormat("Ejecutando: getAccountTrans(ProductType={0}, ProductId={1}, Filter={2})", _productType, _productId, js.Serialize(_filterExpr))
        _responseHeader = svc.getAccountTrans(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, _filterExpr, _AccountTransactions)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            Logger.InfoFormat("  >> OperationID={0} OperationDescription={1} OperationResult={2}", _responseHeader.OperationID, _responseHeader.OperationDescription, _responseHeader.OperationResult)
            If Not _AccountTransactions Is Nothing AndAlso _AccountTransactions.Length > 0 Then
                For Each _account As AChannel.AccountTransaction In _AccountTransactions
                    Logger.InfoFormat("  >> ID:{0} Fecha:{1:d} Monto:{2:N}", _account.TransactionId, _account.TransactionDate, _account.Amount)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    Private Function getLoans() As AChannel.Loan()
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Logger.InfoFormat("-------------------- LISTA DE PRESTAMOS")
        Logger.InfoFormat("Ejecutando: getLoans(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _Loans() As AChannel.Loan = Nothing

        _responseHeader = svc.getLoans(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _Loans)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            Logger.InfoFormat("  >> OperationID={0} OperationDescription={1} OperationResult={2}", _responseHeader.OperationID, _responseHeader.OperationDescription, _responseHeader.OperationResult)
            If Not _Loans Is Nothing AndAlso _Loans.Length > 0 Then
                For Each _account As AChannel.Loan In _Loans
                    Logger.InfoFormat("  >> Prestamo:{0} Cuota:{1:N}", _account.ProductId, _account.MonthlyAmount)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
        Return _Loans
    End Function

    Private Sub getLoanDetail(ByVal _productType As Short, ByVal _productId As String)

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE DETALLE PRESTAMO")
        Logger.InfoFormat("Ejecutando: getLoanDetail(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _LoanDetail As AChannel.LoanDetail = Nothing

        _responseHeader = svc.getLoanDetail(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productId, _LoanDetail)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _LoanDetail Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Desembolso:{1} Fecha:{2}", _LoanDetail.ProductId, _LoanDetail.disbursementAmount, _LoanDetail.disbursementDate)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Function getInvestments() As AChannel.Investment()
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Logger.InfoFormat("-------------------- LISTA DE INVERSIONES")
        Logger.InfoFormat("Ejecutando: getInvestments(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _Investments() As AChannel.Investment = Nothing
        _responseHeader = svc.getInvestments(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _Investments)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            Logger.InfoFormat("  >> OperationID={0} OperationDescription={1} OperationResult={2}", _responseHeader.OperationID, _responseHeader.OperationDescription, _responseHeader.OperationResult)
            If Not _Investments Is Nothing AndAlso _Investments.Length > 0 Then
                For Each _account As AChannel.Investment In _Investments
                    Logger.InfoFormat("  >> Inversion:{0} Monto:{1:N}", _account.ProductId, _account.CreditLineCash)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
        Return _Investments
    End Function

    Private Sub getInvestmentDetail(ByVal _productType As Short, ByVal _productId As String)

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE DETALLE CERTIFICADO")
        Logger.InfoFormat("Ejecutando: getInvestmentDetail(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _InvestmentDetail As AChannel.InvestmentDetail = Nothing

        _responseHeader = svc.getInvestmentDetail(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productId, _InvestmentDetail)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _InvestmentDetail Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Desembolso:{1} Fecha:{2}", _InvestmentDetail.ProductId, _InvestmentDetail.creditLineCash, _InvestmentDetail.openDate)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getSocialReason()
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Logger.InfoFormat("-------------------- RAZON SOCIAL")
        Logger.InfoFormat("Ejecutando: getSocialReason(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _SocialReason As AChannel.SocialReason = Nothing

        _responseHeader = svc.getSocialReason(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _SocialReason)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _SocialReason Is Nothing Then
                Logger.InfoFormat("  >> CustomerNumber:{0} Social Reason:{1}", _SocialReason.socialReason)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getProducts()
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Logger.Info("-------------------- PRODUCTOS DEL CLIENTE")
        Logger.InfoFormat("Ejecutando: getProducts(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _Products() As AChannel.Products = Nothing
        Dim _Types(3) As Integer
        _Types(0) = 1
        _Types(1) = 2
        _Types(2) = 3
        _Types(3) = 4

        _responseHeader = svc.getProducts(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _Types, _Products)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _Products Is Nothing AndAlso _Products.Length > 0 Then
                For Each _account As AChannel.Products In _Products
                    Logger.InfoFormat("  >> Product:{0} Tipo:{1}", _account.ProductId, _account.ProductType)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Function getCreditcards() As AChannel.CreditCard()
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Logger.Info("-------------------- LISTA DE TARJETAS")
        Logger.InfoFormat("Ejecutando: getCreditCards(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)

        Dim _CreditCards() As AChannel.CreditCard = Nothing

        '_request.customerDocNumber = "001-0323059-5"

        _responseHeader = svc.getCreditCards(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _CreditCards)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _CreditCards Is Nothing AndAlso _CreditCards.Length > 0 Then
                For Each _account As AChannel.CreditCard In _CreditCards
                    Logger.InfoFormat("  >> CreditCard:{0} Marca:{1} Monto Disponible:{2:N}", _account.productId, _account.cardBrand, _account.availableLimit)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
        Return _CreditCards
    End Function

    Private Sub getCreditCardTrans(ByVal _productId As String)
        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- TRANSACCIONES DE TARJETAS")
        Logger.InfoFormat("Ejecutando: getCreditCardTrans(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _CreditCardTransaction() As AChannel.CreditCardTransaction = Nothing
        _responseHeader = svc.getCreditCardTrans(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productId, 1, 1, _CreditCardTransaction)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _CreditCardTransaction Is Nothing AndAlso _CreditCardTransaction.Length > 0 Then
                For Each _account As AChannel.CreditCardTransaction In _CreditCardTransaction
                    Logger.InfoFormat("  >> CreditCard:{0} Monto:{1:N} Fecha:{2}", _account.ProductId, _account.Amount, _account.TransactionDate)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    Private Sub getCreditCardBalance(ByVal _productId As String)
        Dim _currency As AChannel.Currencies = AChannel.Currencies.PesoDominicano
        _request.customerDocType = AChannel.DocumentType.Cedula
        _request.customerDocNumber = "001-0931437-7"
        _productId = "4021080000000033"
        _currency = 1

        SetMessageKey(New Object(2) {_request.customerDocNumber, _productId, _currency}, _request.RequestHeader)

        Logger.Info("-------------------- BALANCE DE UNA TARJETA")
        Logger.InfoFormat("Ejecutando: getCreditCardBalance(Doctype={0}, DocNumber={1}, _ProductId={2})", _request.customerDocType, _request.customerDocNumber, _productId)
        Dim _CreditCardBalance As AChannel.CreditCardBalance = Nothing
        _responseHeader = svc.getCreditCardBalance(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, AChannel.ProductTypes.CreditCard, _productId, _currency, _CreditCardBalance)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _CreditCardBalance Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Balance:{1} PagoMinimo:{2}", _CreditCardBalance.productId, _CreditCardBalance.closingBal, _CreditCardBalance.minPayment)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    Private Sub getAdditionalCards(ByVal _productId As String)
        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- TARJETAS ADICIONALES DE UNA TARJETA")
        Logger.InfoFormat("Ejecutando: getAdditionalCards(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _AdditionalCards() As AChannel.AdditionalCreditCard = Nothing
        _responseHeader = svc.getAdditionalCards(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productId, 1, _AdditionalCards)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _AdditionalCards Is Nothing AndAlso _AdditionalCards.Length > 0 Then
                For Each _account As AChannel.AdditionalCreditCard In _AdditionalCards
                    Logger.InfoFormat("  >> CreditCard:{0} Adicional:{1} Nombre:{2}", _account.productId, _account.additionalCardNum, _account.additionalCardHolder)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    Private Sub getExtraLimitTrans(ByVal _productId As String)
        _productId = "4249070000000030"

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- TRANSACCIONES DE EXTRALIMITE DE UNA TARJETA")
        Logger.InfoFormat("Ejecutando: getExtraLimitTrans(Doctype={0}, DocNumber={1}, ProductID={2})", _request.customerDocType, _request.customerDocNumber, _productId)
        Dim _ExtraLimitTransaction() As AChannel.ExtraLimitTransaction = Nothing
        _responseHeader = svc.getExtraLimitTrans(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productId, _ExtraLimitTransaction)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _ExtraLimitTransaction Is Nothing AndAlso _ExtraLimitTransaction.Length > 0 Then
                For Each _account As AChannel.ExtraLimitTransaction In _ExtraLimitTransaction
                    Logger.InfoFormat("  >> CreditCard:{0} Adicional:{1} Nombre:{2}", _account.ProductId, _account.Description, _account.Amount, _account.DebitOrCredit)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    Private Sub getCreditCardDetail(ByVal _productId As String, ByVal Currency As Integer)
        _request.customerDocType = AChannel.DocumentType.Cedula
        _request.customerDocNumber = "001-0931437-7"
        _productId = "4021080000000033"
        Currency = 1

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE DETALLE DE UNA TARJETA")
        Logger.InfoFormat("Ejecutando: getCreditCardDetail(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _CreditCardDetail As AChannel.CreditCardDetail = Nothing

        _responseHeader = svc.getCreditCardDetail(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productId, Currency, _CreditCardDetail)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _CreditCardDetail Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Balance:{1} PagoMinimo:{2} FechaPago:{3} PesosCaribe: {4}", _CreditCardDetail.productId, _CreditCardDetail.creditLineCash, _CreditCardDetail.minPayment, _CreditCardDetail.lastPaymentDate, _CreditCardDetail.pesosCaribeAmount)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getAccountBalance(ByVal _productType As AChannel.ProductTypes, ByVal _productId As String)

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE BALANCE DE UN PRODUCTO")
        Logger.InfoFormat("Ejecutando: getAccountBalance(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _AccountBalance As AChannel.AccountBalance = Nothing

        _responseHeader = svc.getAccountBalance(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, _AccountBalance)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _AccountBalance Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Balance:{1} MOneda:{2}", _AccountBalance.ProductId, _AccountBalance.AvailableBal, _AccountBalance.Currency)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getProductData(ByVal _productType As AChannel.ProductTypes, ByVal _productId As String)

        SetMessageKey(New Object(0) {_productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE DETALLE DE UNA PRODUCTO")
        Logger.InfoFormat("Ejecutando: getProductData(Doctype={0}, DocNumber={1})", _productType, _productId)
        Dim _ProductData As AChannel.ProductData = Nothing
        Dim currency As AChannel.Currencies = AChannel.Currencies.PesoDominicano

        _responseHeader = svc.getProductData(_request.RequestHeader, _productType, _productId, currency, _ProductData)


        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _ProductData Is Nothing Then
                Logger.InfoFormat("  >> Producto:{0} Moneda:{1} Nombre:{2}, Resultado:{3}", _ProductData.ProductId, _ProductData.Currency, _ProductData.customerName, _responseHeader.OperationResult)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

        Logger.InfoFormat("  >> Response OperationID:{0}", _responseHeader.OperationID)
        Logger.InfoFormat("  >> Response OperationResult:{0}", _responseHeader.OperationResult)
        Logger.InfoFormat("  >> Response OperationDescription:{0}", _responseHeader.OperationDescription)
        Logger.InfoFormat("  >> Response MarchantKey:{0}", _responseHeader.MarchantKey)
        Logger.InfoFormat("  >> Response UserID:{0}", _responseHeader.UserID)
        Logger.InfoFormat("  >> Response IPUser:{0}", _responseHeader.IPUser)
        Logger.InfoFormat("  >> Response SessionID:{0}", _responseHeader.SessionID)
        Logger.InfoFormat("  >> Response MessageKey:{0} ", _responseHeader.MessageKey)

    End Sub

    Private Sub getExchange()
        Dim _amount As Double = 35565.67
        Dim _sourceCurrency As AChannel.Currencies = AChannel.Currencies.PesoDominicano
        Dim _targetCurrency As AChannel.Currencies = AChannel.Currencies.Dolar

        SetMessageKey(New Object(0) {_amount}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE EL CALCULO DE LA CONVERSION DE UNA MONEDA A OTRA")
        Logger.InfoFormat("Ejecutando: getExchange(Amount={0}, sourceCurrendy={1}, targetCurrendy={2})", _amount, _sourceCurrency, _targetCurrency)
        Dim _exchange As AChannel.Exchange = Nothing

        _responseHeader = svc.getExchange(_request.RequestHeader, _sourceCurrency, _targetCurrency, _amount, Now.Date, _exchange)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _exchange Is Nothing Then
                Logger.InfoFormat("  >> Rate={0}, targetCurrendy={1}, Conversion={2}", _exchange.exchangeRate, _exchange.currency, _exchange.translatedAmount)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub getPesosCaribeAccounts()
        SetMessageKey(New Object(0) {_request.customerDocNumber}, _request.RequestHeader)

        Logger.Info("-------------------- BUSCA CUENTAS DE PESOS CARIBE ")
        Logger.InfoFormat("Ejecutando: getPesosCaribeAccounts(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _PesosCaribeAccounts() As AChannel.ALoyalAccount = Nothing
        _responseHeader = svc.getPesosCaribeAccounts(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _PesosCaribeAccounts)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _PesosCaribeAccounts Is Nothing AndAlso _PesosCaribeAccounts.Length > 0 Then
                For Each _account As AChannel.ALoyalAccount In _PesosCaribeAccounts
                    Logger.InfoFormat("  >> productId:{0} productType:{1} Pesos Caribe:{2}", _account.productId, _account.productType, _account.totalAmount)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    'Private Sub Transfer(ByVal transferType As AChannel.TransferTypes, ByVal customerDocType As AChannel.DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As AChannel.ProductTypes, ByVal sourceProductId As String, ByVal destinationDocType As AChannel.DocumentType, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As AChannel.ProductTypes, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As AChannel.Currencies, ByVal concept As String)
    '    'Dim _amount As Double = 365.67
    '    Dim _sourceCurrency As AChannel.Currencies = AChannel.Currencies.PesoDominicano

    '    SetMessageKey(New Object(5) {customerDocNumber, sourceProductId, destinationDocNum, destinationProductId, amount, currency}, _request.RequestHeader)

    '    Logger.Info("-------------------- EJECUTANDO TRANSFER -----------------")
    '    Logger.InfoFormat("Ejecutando: transfer(Amount={0}, TransferType={1}, sourceProduct={2}, targetProduct={3})", amount, transferType, sourceProductId, destinationProductId)

    '    Dim _result As Long = 99

    '    _responseHeader = svc.Transfer(_request.RequestHeader, transferType, customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept, _result)

    '    Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1} Resultado:{2}", _responseHeader.MessageKey, _responseHeader.OperationDescription, _responseHeader.OperationResult)
    '    If ValidateResponseHeader(_responseHeader) Then
    '        Logger.Info("  >> Respuesta Validada")
    '        Logger.InfoFormat("  >> Resultado={0}", _result)
    '    Else
    '        Logger.Info("Mensaje de Respuesta Invalido")
    '    End If
    'End Sub

    'INCLUYE Transferencia via LBTR - rsanchez Feb 1ro 2017
    Private Sub Transfer(ByVal transferType As AChannel.TransferTypes, ByVal customerDocType As AChannel.DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As AChannel.ProductTypes, ByVal sourceProductId As String, ByVal destinationDocType As AChannel.DocumentType, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As AChannel.ProductTypes, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As AChannel.Currencies, ByVal concept As String, ByVal sourceName As String, ByVal destinationBankSwift As String)
        'Dim _amount As Double = 365.67
        Dim _sourceCurrency As AChannel.Currencies = AChannel.Currencies.PesoDominicano

        SetMessageKey(New Object(5) {customerDocNumber, sourceProductId, destinationDocNum, destinationProductId, amount, currency}, _request.RequestHeader)

        Logger.Info("-------------------- EJECUTANDO TRANSFER -----------------")
        Logger.InfoFormat("Ejecutando: transfer(Amount={0}, TransferType={1}, sourceProduct={2}, targetProduct={3})", amount, transferType, sourceProductId, destinationProductId)

        Dim _result As Long = 99

        _responseHeader = svc.Transfer(_request.RequestHeader, transferType, customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept, sourceName, destinationBankSwift, _result)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1} Resultado:{2}", _responseHeader.MessageKey, _responseHeader.OperationDescription, _responseHeader.OperationResult)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            Logger.InfoFormat("  >> Resultado={0}", _result)
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub MassTransfer(ByVal customerDocType As AChannel.DocumentType, ByVal customerDocNumber As String)
        Dim _fileName As String = "HCP15062010_enc.TXT"
        customerDocType = AChannel.DocumentType.Grupo
        customerDocNumber = "169"

        SetMessageKey(New Object(0) {customerDocNumber}, _request.RequestHeader)

        Logger.Info("-------------------- EJECUTANDO MASSTRANSFER -----------------")
        Logger.InfoFormat("Ejecutando: masstransfer(sourceProduct={0})", customerDocNumber)

        Dim _result As AChannel.MassTransferOutput = Nothing

        _responseHeader = svc.MassTransfer(_request.RequestHeader, customerDocType, customerDocNumber, _fileName, _result)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            If Not _result Is Nothing Then
                Logger.InfoFormat("  >> Product={0}, MovmentCount={1}, Total={2}", _result.sourceProductId, _result.movmentCount, _result.totalAmount)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

    Private Sub GetMassTransferDetail(ByVal customerDocType As AChannel.DocumentType, ByVal customerDocNumber As String, ByVal transferid As Integer)

        SetMessageKey(New Object(0) {customerDocNumber}, _request.RequestHeader)

        Logger.Info("-------------------- EJECUTANDO MASSTRANSFER -----------------")
        Logger.InfoFormat("Ejecutando: getmasstransfer(sourceProduct={0})", customerDocNumber)

        Dim _result() As AChannel.TransferDetail = Nothing

        _responseHeader = svc.getMassTransferDetail(_request.RequestHeader, customerDocType, customerDocNumber, transferid, _result)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _result Is Nothing AndAlso _result.Length > 0 Then
                For Each _account As AChannel.TransferDetail In _result
                    Logger.InfoFormat("  >> MOnto:{0:N} concepto:{1} moneda:{2} Producto:{3} Nombre:{4}", _account.amount, _account.concept, _account.Currency, _account.destinationProductId, _account.destinationName)
                Next
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If

    End Sub

    Private Sub getTransferCommission(ByVal _productType As AChannel.ProductTypes, ByVal _productId As String)

        SetMessageKey(New Object(1) {_request.customerDocNumber, _productId}, _request.RequestHeader)

        Logger.Info("-------------------- OBTIENE LA COMISION PARA UNA TRANSFERENCIA VIA LBTR")
        Logger.InfoFormat("Ejecutando: getTransferCommission(Doctype={0}, DocNumber={1})", _request.customerDocType, _request.customerDocNumber)
        Dim _TransferCommission As AChannel.TransferCommission = Nothing

        _responseHeader = svc.getTransferCommission(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, _productId, AChannel.TransferTypes.ExtraBankLBTR, _TransferCommission)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            If Not _TransferCommission Is Nothing Then
                Logger.InfoFormat("  >> Tipo Producto:{0} Producto:{1} Comision:{2}", _TransferCommission.productType, _TransferCommission.productId, _TransferCommission.commission)
            Else
                Logger.Info("  >> Resultado vacio")
            End If
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub


    Private Sub GenerateHeader()

        _responseHeader = New AChannel.ResponseHeader With {.OperationID = 42142, .OperationResult = 8, .SessionID = "o3ozzx55x4k5eqzcocvie2jh", .IPUser = "127.0.0.1", .MarchantKey = "ABCDEF--", .OperationDescription = "Producto seleccionado no corresponde con el número", .UserID = "344", .MessageKey = "/Sp/zuQ7X8Z1w65giVGVetTbMarIucPlu/IrSGS2ujM="}

        '        "ABCDEF--","UserID":"344","SessionID":"o3ozzx55x4k5eqzcocvie2jh","IPUser":"127.0.0.1","MessageKey":"/Sp/zuQ7X8Z1w65giVGVetTbMarIucPlu/IrSGS2ujM=","OperationID":42142,"OperationResult":8,"OperationDescription":"Producto seleccionado no corresponde con el número
        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1}", _responseHeader.MessageKey, _responseHeader.OperationDescription)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
        Else
            Logger.Info("Mensaje de Respuesta Invalido")
        End If
    End Sub

End Module
