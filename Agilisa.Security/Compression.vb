﻿Imports System.IO
Imports System.IO.Compression

Public Module Deflate

    Public Function ReadAllBytesFromStream(ByVal stream As Stream, ByVal buffer() As Byte) As Integer
        ' Use this method is used to read all bytes from a stream.
        Dim offset As Integer = 0
        Dim totalCount As Integer = 0
        While True
            Dim bytesRead As Integer = stream.Read(buffer, offset, 100)
            If bytesRead = 0 Then
                Exit While
            End If
            offset += bytesRead
            totalCount += bytesRead
        End While
        Return totalCount
    End Function 'ReadAllBytesFromStream

    Public Function CompareData(ByVal buf1() As Byte, ByVal len1 As Integer, ByVal buf2() As Byte, ByVal len2 As Integer) As Boolean
        ' Use this method to compare data from two different buffers.
        If len1 <> len2 Then
            Debug.WriteLine("Number of bytes in two buffer are different" & len1 & ":" & len2)
            Return False
        End If

        Dim i As Integer
        For i = 0 To len1 - 1
            If buf1(i) <> buf2(i) Then
                Debug.WriteLine("byte " & i & " is different " & buf1(i) & "|" & buf2(i))
                Return False
            End If
        Next i
        Return True
    End Function 'CompareData

    Public Function DeflateCompress(ByVal source As String, ByVal target As String) As Boolean

        Dim result As Boolean = True
        Try
            Dim buffer As Byte() = File.ReadAllBytes(source)
            Dim outfile As FileStream = File.Create(target)
            ' Use the newly created memory stream for the compressed data.
            Dim zipStream As New DeflateStream(outfile, CompressionMode.Compress, True)
            zipStream.Write(buffer, 0, buffer.Length)
            zipStream.Close()
            outfile.Close()
            result = True
        Catch e As Exception
            Debug.WriteLine("Error: The file being read contains invalid data.")
            result = False
        End Try
        Return result
    End Function

    Public Function DeflateDecompress(ByVal source As String, ByVal target As String) As Boolean
        Dim result As Boolean = True
        Try
            Dim buffer As Byte() = File.ReadAllBytes(source)
            Dim outfile As FileStream = File.Create(target)
            Dim zipStream As New DeflateStream(outfile, CompressionMode.Decompress)
            zipStream.Write(buffer, 0, buffer.Length)
            zipStream.Close()

        Catch e As Exception
            Debug.WriteLine("Error: The file being read contains invalid data.")
            result = False
        End Try
        Return result
    End Function
End Module