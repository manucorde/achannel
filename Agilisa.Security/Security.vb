Imports System.Configuration
Imports System.Security
Imports System.Security.Cryptography

Public Module Security

#Region "Constant Definitions"
    Private Const _pss As String = "QUBpMWk1YQ=="
    Private Const _slt As String = "c0AxdFZhbHVl"
    Private Const _vct As String = "QDFCMmMzRDRlNUY2ZzdIOA=="
#End Region

#Region "DB Connection Parameters"
    Public configKey As String

    Public Function GetDataConn() As String
        If configKey = String.Empty Then
            configKey = "connString"
        End If

        Return GetDataConn(configKey)
    End Function

    Public Function GetDataConn(ByVal _key As String) As String
        configKey = _key

        Dim _conn As String
        Dim _param As String
        _conn = System.Configuration.ConfigurationManager.AppSettings(_key)
        _param = System.Configuration.ConfigurationManager.AppSettings("connEncrypt")

        If _param = "1" Then
            _conn = DecryptAES(_conn, System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_pss)))
        End If
        Return _conn
    End Function
#End Region

#Region "Cryptography"

    Public Function EncryptAES(ByVal data As String, ByVal key As String) As String
        Dim sym As New Encryption.Symmetric(Encryption.Symmetric.Provider.Rijndael)

        Dim encryptedData As Encryption.Data
        encryptedData = sym.Encrypt(New Encryption.Data(data), New Encryption.Data(key))
        Return encryptedData.ToBase64
    End Function

    Public Function DecryptAES(ByVal data As String, ByVal key As String) As String
        Dim sym As New Encryption.Symmetric(Encryption.Symmetric.Provider.Rijndael)

        Dim encryptedData As New Encryption.Data
        encryptedData.Base64 = data

        Dim decryptedData As Encryption.Data = sym.Decrypt(encryptedData, New Encryption.Data(key))

        Return decryptedData.ToString
    End Function


    Public Function HashMD5(ByVal _codigo As String) As String
        Dim _hashData As [Byte]() = (New System.Text.UnicodeEncoding).GetBytes(_codigo)
        Dim _hashValue As String = Convert.ToBase64String(New System.Security.Cryptography.MD5CryptoServiceProvider().ComputeHash(_hashData))
        Return _hashValue
    End Function

    Public Function HashSHA1(ByVal _codigo As String) As String
        Dim _hashData As [Byte]() = (New System.Text.UnicodeEncoding).GetBytes(_codigo)
        Dim _hashValue As String = Convert.ToBase64String(New System.Security.Cryptography.SHA1CryptoServiceProvider().ComputeHash(_hashData))
        Return _hashValue
    End Function

#End Region

#Region "Master Key Management"
    Private _lmkCached As String

    ' Obtiene la llave Interna de la Maquina "Local-Machine-Key" 
    Public Function GetLmk() As String
        If _lmkCached = String.Empty Then
            _lmkCached = GetFingerPrint()
        End If
        Return _lmkCached
    End Function

    ' Obtiene la llave Encriptada de la Maestra "Global-Master-Key" 
    ' con la Llave interna de la Maquina "Local-Machine-Key" 
    Public Function GetDiskKey(Optional ByVal _key As String = "diskKey") As String
        Dim _diskKey As String = System.Configuration.ConfigurationManager.AppSettings(_key)
        Return _diskKey
    End Function

    Public Function GetDiskKey(ByVal _gmk As String, ByVal _lmk As String) As String
        Return EncryptAES(_gmk, _lmk)
    End Function

    ' Obtiene la llave Maestra "Global-Master-Key" Clear
    ' Esta funcion debera estar protegida al maximo
    Public Function GetGmk(ByVal _diskKey As String) As String
        Dim _result As String = String.Empty
        Try
            If _result = String.Empty Then
                Dim _lmk As String = GetLmk()
                _result = DecryptAES(_diskKey, _lmk)
                '_result = DecryptAES(_diskKey, _lmk, _
                '                    System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_slt)), "MD5", 2, _
                '                    System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_vct)), 256)
            End If
        Catch ex As Exception
            Debug.WriteLine(ex.Message)
        End Try
        Return _result
    End Function

    ' Encripta utilizando la GMK, en base a un DiskKey 
    ' [Si no hay DiskKey, devuelve el mismo valor]
    Public Function EncryptGmk(ByVal _text As String) As String
        Return EncryptGmk(_text, GetDiskKey())
    End Function

    Public Function EncryptGmk(ByVal _text As String, ByVal _diskKey As String) As String
        Dim _result As String
        If Not _diskKey = Nothing AndAlso Not _diskKey = String.Empty Then
            _result = EncryptAES(_text, GetGmk(_diskKey))
            '_result = EncryptAES(_text, GetGmk(_diskKey), _
            '                    System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_slt)), "MD5", 2, _
            '                    System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_vct)), 256)
        Else
            _result = _text
        End If
        Return _result
    End Function

    ' Desencripta utilizando la GMK, en base a un DiskKey
    ' [Si no hay DiskKey, devuelve el mismo valor]
    Public Function DecryptGmk(ByVal _text As String) As String
        Return DecryptGmk(_text, GetDiskKey())
    End Function

    Public Function DecryptGmk(ByVal _text As String, ByVal _diskKey As String) As String
        Dim _result As String
        If Not _diskKey = Nothing AndAlso Not _diskKey = String.Empty Then
            _result = DecryptAES(_text, GetGmk(_diskKey))
            '_result = DecryptAES(_text, GetGmk(_diskKey), _
            '                    System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_slt)), "MD5", 2, _
            '                    System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_vct)), 256)
        Else
            _result = _text
        End If
        Return _result
    End Function

#End Region

    '#Region "Fuunciones Old de Master-Key"

    '    Public Function EncryptAES_old(ByVal plainText As String, ByVal passPhrase As String, ByVal saltValue As String, ByVal hashAlgorithm As String, ByVal passwordIterations As Integer, ByVal initVector As String, ByVal keySize As Integer) As String
    '        Dim initVectorBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(initVector)
    '        Dim saltValueBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(saltValue)
    '        Dim plainTextBytes As Byte() = System.Text.Encoding.UTF8.GetBytes(plainText)
    '        Dim password As PasswordDeriveBytes = New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)
    '        Dim keyBytes As Byte() = password.GetBytes(CInt(keySize / 8))
    '        Dim symmetricKey As RijndaelManaged = New RijndaelManaged
    '        symmetricKey.Mode = CipherMode.CBC
    '        Dim encryptor As ICryptoTransform = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes)
    '        Dim memoryStream As New System.IO.MemoryStream
    '        Dim cryptoStream As CryptoStream = New CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write)
    '        cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length)
    '        cryptoStream.FlushFinalBlock()
    '        Dim cipherTextBytes As Byte() = memoryStream.ToArray
    '        memoryStream.Close()
    '        cryptoStream.Close()
    '        Dim cipherText As String = Convert.ToBase64String(cipherTextBytes)
    '        Return cipherText
    '        'Dim buffer1 As Byte() = System.Text.Encoding.ASCII.GetBytes(initVector)
    '        'Dim buffer2 As Byte() = System.Text.Encoding.ASCII.GetBytes(saltValue)
    '        'Dim buffer3 As Byte() = System.Text.Encoding.UTF8.GetBytes(plainText)
    '        'Dim bytes1 As New PasswordDeriveBytes(passPhrase, buffer2, hashAlgorithm, passwordIterations)
    '        'Dim buffer4 As Byte() = bytes1.GetBytes((keySize / 8))
    '        'Dim managed1 As New RijndaelManaged
    '        'managed1.Mode = CipherMode.CBC
    '        'Dim transform1 As ICryptoTransform = managed1.CreateEncryptor(buffer4, buffer1)
    '        'Dim stream1 As New System.IO.MemoryStream
    '        'Dim stream2 As New CryptoStream(stream1, transform1, CryptoStreamMode.Write)
    '        'stream2.Write(buffer3, 0, buffer3.Length)
    '        'stream2.FlushFinalBlock()
    '        'Dim buffer5 As Byte() = stream1.ToArray
    '        'stream1.Close()
    '        'stream2.Close()
    '        'Return Convert.ToBase64String(buffer5)
    '    End Function

    '    Public Function DecryptAES_old(ByVal cipherText As String, ByVal passPhrase As String, ByVal saltValue As String, ByVal hashAlgorithm As String, ByVal passwordIterations As Integer, ByVal initVector As String, ByVal keySize As Integer) As String
    '        Dim initVectorBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(initVector)
    '        Dim saltValueBytes As Byte() = System.Text.Encoding.ASCII.GetBytes(saltValue)
    '        Dim cipherTextBytes As Byte() = Convert.FromBase64String(cipherText)
    '        Dim password As PasswordDeriveBytes = New PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations)
    '        Dim keyBytes As Byte() = password.GetBytes(CInt(keySize / 8))
    '        Dim symmetricKey As RijndaelManaged = New RijndaelManaged
    '        symmetricKey.Mode = CipherMode.CBC
    '        Dim decryptor As ICryptoTransform = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes)
    '        Dim memoryStream As New System.IO.MemoryStream(cipherTextBytes)
    '        Dim cryptoStream As New CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read)
    '        Dim plainTextBytes(cipherTextBytes.Length) As Byte
    '        Dim decryptedByteCount As Integer = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length)
    '        memoryStream.Close()
    '        cryptoStream.Close()
    '        Dim plainText As String = System.Text.Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount)
    '        Return plainText

    '        'Dim buffer1 As Byte() = System.Text.Encoding.ASCII.GetBytes(initVector)
    '        'Dim buffer2 As Byte() = System.Text.Encoding.ASCII.GetBytes(saltValue)
    '        'Dim buffer3 As Byte() = Convert.FromBase64String(cipherText)
    '        'Dim bytes1 As New PasswordDeriveBytes(passPhrase, buffer2, hashAlgorithm, passwordIterations)
    '        'Dim buffer4 As Byte() = bytes1.GetBytes((keySize / 8))
    '        'Dim managed1 As New RijndaelManaged
    '        'managed1.Mode = CipherMode.CBC
    '        'Dim transform1 As ICryptoTransform = managed1.CreateDecryptor(buffer4, buffer1)
    '        'Dim stream1 As New System.IO.MemoryStream(buffer3)
    '        'Dim stream2 As New CryptoStream(stream1, transform1, CryptoStreamMode.Read)
    '        'Dim buffer5 As Byte() = New Byte(buffer3.Length - 1) {}
    '        'Dim num1 As Integer = stream2.Read(buffer5, 0, buffer5.Length)
    '        'stream1.Close()
    '        'stream2.Close()
    '        'Return System.Text.Encoding.UTF8.GetString(buffer5, 0, num1)
    '    End Function

    '    Public Function EncryptGmk_old(ByVal _text As String) As String
    '        Return EncryptGmk_old(_text, GetDiskKey())
    '    End Function

    '    Public Function EncryptGmk_old(ByVal _text As String, ByVal _diskKey As String) As String
    '        Dim _result As String
    '        If Not _diskKey = Nothing AndAlso Not _diskKey = String.Empty Then
    '            _result = EncryptAES_old(_text, GetGmk(_diskKey), _
    '                                System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_slt)), "MD5", 2, _
    '                                System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_vct)), 256)
    '        Else
    '            _result = _text
    '        End If
    '        Return _result
    '    End Function

    '    Public Function DecryptGmk_old(ByVal _text As String) As String
    '        Return DecryptGmk_old(_text, GetDiskKey())
    '    End Function

    '    Public Function DecryptGmk_old(ByVal _text As String, ByVal _diskKey As String) As String
    '        Dim _result As String
    '        If Not _diskKey = Nothing AndAlso Not _diskKey = String.Empty Then
    '            _result = DecryptAES_old(_text, GetGmk(_diskKey), _
    '                                System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_slt)), "MD5", 2, _
    '                                System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(_vct)), 256)
    '        Else
    '            _result = _text
    '        End If
    '        Return _result
    '    End Function
    '#End Region

    Public Const goodPasswordRegEx As String = "^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,12}$" ' Min:8 Max:12, must:[A-Z, a-z, 0-9]
    ' "^.*(?=.{10,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$"   Min:10 Max:10 must:[A-Z, a-z, 0-9] opt:[@#$%^&+=]
    Public Function GetPasswordRegEx() As String
        Dim _regEx As String = System.Configuration.ConfigurationManager.AppSettings("PassRegEx")
        If _regEx Is Nothing OrElse _regEx = String.Empty Then
            _regEx = goodPasswordRegEx
        End If
        Return _regEx
    End Function

    Public Function ValidatePasswordExpression(ByVal userName As String) As Boolean
        Return ValidatePasswordExpression(userName, GetPasswordRegEx())
    End Function

    Public Function ValidatePasswordExpression(ByVal userName As String, ByVal regEx As String) As Boolean
        Dim r As Text.RegularExpressions.Regex = New Text.RegularExpressions.Regex(regEx)
        Return r.IsMatch(userName)
    End Function

End Module
