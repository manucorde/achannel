﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports log4net
Imports System.Xml


Public Class SqlOperations
    Inherits OperationsDB

    Dim svc As AChannel.Data.ALoyalWS.ALoyalWS = New ALoyalWS.ALoyalWS()

    Dim _request As New AChannel.Data.LBTRWebService.DoTransactionRequestBody
    Dim svc1 As New AChannel.Data.LBTRWebService.TrxLBTRWebServiceSoapClient
    Dim result_svc1 As AChannel.Data.LBTRWebService.TransactionResponse

    Private _connString As String

    Public Sub New()
        _connString = ConfigurationManager.ConnectionStrings("ALoyalConnection").ConnectionString
        LogManager.GetLogger("root")

    End Sub

    Public Function getALoyalData(ByVal productType As String, ByVal productId As String) As AL_getProductDataResult
        Dim result As AL_getProductDataResult = Nothing

        Try
            Dim db As New ALoyalDBDataContext(_connString)
            result = db.AL_getProductData(productType, productId, Me.CodigoError, Me.ErrorDescription, Me.Secuencia).ToList.FirstOrDefault

            Me.ErrorDescription = "Error - " + If(Me.ErrorDescription Is Nothing OrElse Me.ErrorDescription.Length > 0, Me.ErrorDescription, String.Empty)
            If Me.CodigoError = 0 Then
                Me.ErrorDescription = "Exitoso"     'IIf(result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)    'result_svc.Mensaje
            End If


        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        End Try
        Return result
    End Function

    Public Function getALoyalAccounts(ByVal customerDocType As Integer, ByVal customerDocNumber As String) As List(Of AL_getALoyalAccountsResult)
        Dim result As List(Of AL_getALoyalAccountsResult) = Nothing

        Try
            Dim db As New ALoyalDBDataContext(_connString)
            result = db.AL_getALoyalAccounts(customerDocType, customerDocNumber, Me.CodigoError, Me.ErrorDescription, Me.Secuencia).ToList

            Me.ErrorDescription = "Error - " + If(Me.ErrorDescription Is Nothing OrElse Me.ErrorDescription.Length > 0, Me.ErrorDescription, String.Empty)
            If Me.CodigoError = 0 Then
                Me.ErrorDescription = "Exitoso"     'IIf(result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)    'result_svc.Mensaje
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        End Try
        Return result
    End Function

    Public Function TransferALoyal(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal sourceProductType As Short, ByVal sourceProductId As String, ByVal destinationDocType As Short, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As Short, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Short, ByVal concept As String) As Long
        Dim result_svc As ALoyalWS.RespuestaTransaccion
        Dim result As New Long
        Dim _reference As New Random
        Dim _adquirente As String = ConfigurationManager.AppSettings("ALoyalAdquirente").ToString
        Dim _afiliado As String = ConfigurationManager.AppSettings("ALoyalAfiliado").ToString
        Dim _usuario As String = ConfigurationManager.AppSettings("ALoyalUsuario").ToString
        Dim _password As String = ConfigurationManager.AppSettings("ALoyalPassword").ToString
        Dim _canal As String = If(Not IsNothing(ConfigurationManager.AppSettings("ALoyalCanal")), ConfigurationManager.AppSettings("ALoyalCanal").ToString, String.Empty)
        Dim _referencia As String = _reference.Next(99999, 999999)

        If String.IsNullOrEmpty(_canal) Then
            _canal = "01"
        End If

        svc.Url = ConfigurationManager.AppSettings("ALoyalWsUrl").ToString   '"http://srvALoyal/ALoyalWS/ALoyalWS.asmx"  

        Try
            result_svc = svc.Transferencia_Puntos(sourceProductId, destinationProductId, amount, "C", _referencia, concept, _canal, _afiliado, _adquirente, _usuario, _password)
            result = result_svc.NoAutorizacion

            Me.CodigoError = result_svc.ResponseCode
            Me.ErrorDescription = "Error - " + If(result_svc.Mensaje Is Nothing OrElse result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)

            If Me.CodigoError = 0 Then
                Me.ErrorDescription = "Exitoso"     'IIf(result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)    'result_svc.Mensaje
            End If
            Me.Secuencia = result_svc.NoAutorizacion

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally

        End Try

        Return result

    End Function
    Public Function TransferLBTR(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal sourceProductType As Short, ByVal sourceProductId As String, ByVal destinationDocType As Short,
                                 ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As Short, ByVal destinationProductId As String,
                                 ByVal amount As Decimal, ByVal currency As Short, ByVal concept As String, ByVal sourceName As String, ByVal destinationBankSwift As String) As Long

        Dim result As New Long
        Dim _reference As New Random
        '' Carga valores por defecto para realizar la transaccion
        'Dim _adquirente As String = ConfigurationManager.AppSettings("ALoyalAdquirente").ToString
        'Dim _afiliado As String = ConfigurationManager.AppSettings("ALoyalAfiliado").ToString
        'Dim _usuario As String = ConfigurationManager.AppSettings("ALoyalUsuario").ToString
        'Dim _password As String = ConfigurationManager.AppSettings("ALoyalPassword").ToString
        'Dim _referencia As String = _reference.Next(99999, 999999)

        '       svc1.DoTransaction.URL = ConfigurationManager.AppSettings("ePayitWsUrl").ToString   '"http://srvALoyal/ALoyalWS/ALoyalWS.asmx"  

        Try
            svc1.Open()
            'With _request

            '    .customerDocType = customerDocType
            '    .customerDocNumber = customerDocNumber
            '    .sourceProductType = sourceProductType
            '    .sourceProductId = sourceProductId
            '    .destinationDocType = destinationDocType
            '    .destinationDocNum = destinationDocNum
            '    .destinationName = destinationName
            '    .destinationBank = destinationBank
            '    .destinationProductType = destinationProductType
            '    .destinationProductId = destinationProductId
            '    .amount = amount
            '    .currency = currency
            '    .concept = concept

            'End With

            result_svc1 = svc1.DoTransaction(3, customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType,
                                 destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId,
                                 amount, currency, concept, sourceName, destinationBankSwift)

            Me.CodigoError = result_svc1.OperationResult

            Me.ErrorDescription = result_svc1.OperationDescription

            Me.Secuencia = result_svc1.transferId

            svc1.Close()

            result = Me.Secuencia

            If Me.CodigoError <> 0 Then
                Logger.InfoFormat(" Transaccion LBTR no exitosa. Cliente:{0} IdTransacccion:{1} Error:{2} Detalle:{3}", customerDocNumber, Me.Secuencia, Me.CodigoError, Me.ErrorDescription)
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = "Ha ocurrido un error durante el proceso de la transaccion."  'ex.Message
            Me.CodigoError = 1
        Finally

        End Try

        Return result

    End Function

    Public Function DebitoALoyal(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal sourceProductType As Short, ByVal sourceProductId As String, ByVal destinationDocType As Short, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As Short, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Short, ByVal concept As String) As Long
        Dim result_svc As ALoyalWS.RespuestaTransaccion
        Dim result As New Long
        Dim _reference As New Random
        Dim _adquirente As String = ConfigurationManager.AppSettings("ALoyalAdquirente").ToString
        Dim _afiliado As String = ConfigurationManager.AppSettings("ALoyalAfiliado").ToString
        Dim _usuario As String = ConfigurationManager.AppSettings("ALoyalUsuario").ToString
        Dim _password As String = ConfigurationManager.AppSettings("ALoyalPassword").ToString
        Dim _tipoTransaccion As String = ConfigurationManager.AppSettings("ALoyalDrEfectivo").ToString    '"02"  ' Debito
        Dim _canal As String = If(Not IsNothing(ConfigurationManager.AppSettings("ALoyalCanal")), ConfigurationManager.AppSettings("ALoyalCanal").ToString, String.Empty)    '"02"  ' Debito
        Dim _referencia As String = _reference.Next(99999, 999999)

        svc.Url = ConfigurationManager.AppSettings("ALoyalWsUrl").ToString   '"http://srvALoyal/ALoyalWS/ALoyalWS.asmx"  

        If String.IsNullOrEmpty(_canal) Then
            _canal = "03"
        End If

        If destinationProductType = 5 Or destinationProductType = 6 Then
            _tipoTransaccion = ConfigurationManager.AppSettings("ALoyalDrPagoTarjeta").ToString
        End If
        Try
            'result_svc = svc.Transaccion_Generica(_tipoTransaccion, sourceProductId, "C", _canal, amount, _referencia, concept, _afiliado, _adquirente, _usuario, _password, 100, 0)
            result_svc = svc.Transaccion_Generica(_tipoTransaccion, sourceProductId, amount, _referencia, concept, "C", _afiliado, _adquirente, _usuario, _password)
            result = result_svc.NoAutorizacion

            Me.CodigoError = result_svc.ResponseCode
            Me.ErrorDescription = "Error - " + If(result_svc.Mensaje Is Nothing OrElse result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)
            If Me.CodigoError = 0 Then
                Me.ErrorDescription = "Exitoso"     'IIf(result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)    'result_svc.Mensaje
            End If
            Me.Secuencia = result_svc.NoAutorizacion '_reference.Next(99999, 999999)

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally

        End Try

        Return result

    End Function

    Public Function CreditoALoyal(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal sourceProductType As Short, ByVal sourceProductId As String, ByVal destinationDocType As Short, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As Short, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Short, ByVal concept As String) As Long
        Dim result_svc As ALoyalWS.RespuestaTransaccion
        Dim result As New Long
        Dim _reference As New Random
        Dim _adquirente As String = ConfigurationManager.AppSettings("ALoyalAdquirente").ToString
        Dim _afiliado As String = ConfigurationManager.AppSettings("ALoyalAfiliado").ToString
        Dim _usuario As String = ConfigurationManager.AppSettings("ALoyalUsuario").ToString
        Dim _password As String = ConfigurationManager.AppSettings("ALoyalPassword").ToString
        Dim _tipoTransaccion As String = ConfigurationManager.AppSettings("ALoyalCr").ToString '"22"  ' Credito
        Dim _referencia As String = _reference.Next(99999, 999999)
        Dim _canal As String = If(Not IsNothing(ConfigurationManager.AppSettings("ALoyalCanal")), ConfigurationManager.AppSettings("ALoyalCanal").ToString, String.Empty)    '"02"  ' Debito

        If String.IsNullOrEmpty(_canal) Then
            _canal = "03"
        End If

        svc.Url = ConfigurationManager.AppSettings("ALoyalWsUrl").ToString   '"http://srvALoyal/ALoyalWS/ALoyalWS.asmx"  

        Try
            'result_svc = svc.Transaccion_Generica(_tipoTransaccion, sourceProductId, "C", _canal, amount, _referencia, concept, _afiliado, _adquirente, _usuario, _password, 100, 0)
            result_svc = svc.Transaccion_Generica(_tipoTransaccion, sourceProductId, amount, _referencia, concept, "C", _afiliado, _adquirente, _usuario, _password)
            result = result_svc.NoAutorizacion

            Me.CodigoError = result_svc.ResponseCode
            Me.ErrorDescription = "Error - " + If(result_svc.Mensaje Is Nothing OrElse result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)

            If Me.CodigoError = 0 Then
                Me.ErrorDescription = "Exitoso"     'IIf(result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)    'result_svc.Mensaje
            End If
            Me.Secuencia = result_svc.NoAutorizacion '_reference.Next(99999, 999999)

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally

        End Try

        Return result

    End Function

    Public Function ReversoALoyal(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal sourceProductType As Short, ByVal sourceProductId As String, ByVal destinationDocType As Short, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As Short, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Short, ByVal concept As String) As Long
        Dim result_svc As ALoyalWS.RespuestaTransaccion
        Dim result As New Long
        Dim _reference As New Random
        Dim _adquirente As String = ConfigurationManager.AppSettings("ALoyalAdquirente").ToString
        Dim _afiliado As String = ConfigurationManager.AppSettings("ALoyalAfiliado").ToString
        Dim _usuario As String = ConfigurationManager.AppSettings("ALoyalUsuario").ToString
        Dim _password As String = ConfigurationManager.AppSettings("ALoyalPassword").ToString
        Dim _tipoTransaccion As String = ConfigurationManager.AppSettings("ALoyalCrReverso").ToString '"14"  ' Reverso de Debito
        Dim _referencia As String = _reference.Next(99999, 999999)
        Dim _canal As String = If(Not IsNothing(ConfigurationManager.AppSettings("ALoyalCanal")), ConfigurationManager.AppSettings("ALoyalCanal").ToString, String.Empty)    '"02"  ' Debito

        If String.IsNullOrEmpty(_canal) Then
            _canal = "03"
        End If

        svc.Url = ConfigurationManager.AppSettings("ALoyalWsUrl").ToString   '"http://srvALoyal/ALoyalWS/ALoyalWS.asmx"  

        Try
            'result_svc = svc.Transaccion_Generica(_tipoTransaccion, sourceProductId, "C", _canal, amount, _referencia, concept, _afiliado, _adquirente, _usuario, _password, 100, 0)
            result_svc = svc.Transaccion_Generica(_tipoTransaccion, sourceProductId, amount, _referencia, concept, "C", _afiliado, _adquirente, _usuario, _password)
            result = result_svc.NoAutorizacion

            Me.CodigoError = result_svc.ResponseCode
            Me.ErrorDescription = "Error - " + If(result_svc.Mensaje Is Nothing OrElse result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)

            If Me.CodigoError = 0 Then
                Me.ErrorDescription = "Exitoso"     'IIf(result_svc.Mensaje.Length > 0, result_svc.Mensaje, String.Empty)    'result_svc.Mensaje
            End If
            Me.Secuencia = result_svc.NoAutorizacion '_reference.Next(99999, 999999)

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally

        End Try

        Return result

    End Function

End Class
