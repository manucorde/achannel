﻿
Public Class OperationsDB
    Implements Operations

    Public Property CodigoError As Short Implements Operations.CodigoError
    Public Property ErrorDescription As String Implements Operations.ErrorDescription
    Public Property Secuencia As Long Implements Operations.Secuencia

    Friend Sub LogResult(ByVal dt As DataTable)
        If Logger.IsDebugEnabled Then
            Logger.DebugFormat("{0} Registros. Columnas Respuesta ----------------", dt.Rows.Count)

            Dim i As Integer
            For Each r As DataRow In dt.Rows
                i = 1
                For Each c As DataColumn In dt.Columns
                    Logger.DebugFormat("ROW {3} >  Campo: {0} Tipo:{1} Valor:{2}", c.ColumnName, c.DataType, r.Item(c.ColumnName), i)
                    i += 1
                Next
            Next
        End If
    End Sub

End Class

Public Interface Operations

    Property CodigoError As Short
    Property ErrorDescription As String
    Property Secuencia As Long

End Interface