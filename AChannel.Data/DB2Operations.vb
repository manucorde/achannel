﻿Imports System.Configuration
Imports AChannel.Data
Imports log4net
'Imports IBM.Data.DB2
Imports DDTek.DB2

Public Class DB2Operations
    Inherits OperationsDB

    Private _conn As DB2Connection
    Private _cmd As DB2Command

    Public Sub New()
        Dim _connString As String = ConfigurationManager.ConnectionStrings("AS400Connection").ConnectionString

        LogManager.GetLogger("root")
        _conn = New DB2Connection(_connString)
        _cmd = New DB2Command

    End Sub

    Public Function PRC_OBTIENE_TARJETAS(ByVal customerDocType As String, ByVal customerDocNumber As String) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random
        Try

            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETCRECRDS"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@DOCTYPE", DB2DbType.VarChar).Value = customerDocType
            _cmd.Parameters.Add("@DOCUMENT", DB2DbType.VarChar).Value = customerDocNumber

            '_cmd.Parameters.Add("PO_COD_ERR", DbType.VarChar).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("PO_DESC_ERR", DbType.VarChar).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("PO_SECUENCIA", DbType.Number).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("RC_CUENTAS_VISTA", DbType.Cursor).Direction = ParameterDirection.Output

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

            LogResult(dt)

        Catch ex As Exception
            Logger.Error(ex.Message)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt
    End Function

    Public Function PRC_MOVIMIENTOS_TARJETA(ByVal customerDocType As Integer, ByVal customerDocNumber As String, ByVal ProductId As String, ByVal period As Short, ByVal Currency As Short) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETCCTRANS"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@doctype", DB2DbType.Integer).Value = customerDocType
            _cmd.Parameters.Add("@document", DB2DbType.VarChar).Value = customerDocNumber
            _cmd.Parameters.Add("@product", DB2DbType.VarChar).Value = ProductId
            _cmd.Parameters.Add("@period", DB2DbType.Integer).Value = period
            _cmd.Parameters.Add("@currency", DB2DbType.Integer).Value = Currency

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

        Catch ex As Exception
            Logger.Error(ex.Message)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function GETCDETAIL(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal productId As String, ByVal Currency As Integer) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETCDETAIL"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@DocType", DB2DbType.Integer).Value = customerDocType
            _cmd.Parameters.Add("@document", DB2DbType.VarChar).Value = customerDocNumber
            _cmd.Parameters.Add("@ProductId", DB2DbType.VarChar).Value = productId
            _cmd.Parameters.Add("@Currency", DB2DbType.Integer).Value = Currency

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

        Catch ex As Exception
            Logger.Error(ex.Message, ex)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function GETCRDBAL(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal productType As Integer, ByVal productId As String, ByVal Currency As Integer) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETCRDBAL"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@customerDocType", DB2DbType.Integer).Value = customerDocType
            _cmd.Parameters.Add("@customerDocNumber", DB2DbType.VarChar).Value = customerDocNumber
            _cmd.Parameters.Add("@ProductType", DB2DbType.Integer).Value = productType
            _cmd.Parameters.Add("@ProductId", DB2DbType.VarChar).Value = productId
            _cmd.Parameters.Add("@Currency", DB2DbType.Integer).Value = Currency

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

        Catch ex As Exception
            Logger.Error(ex.Message, ex)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function GETADCCRDS(ByVal customerDocType As Integer, ByVal customerDocNumber As String, ByVal ProductId As String, ByVal Currency As Integer) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETADCCRDS"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@customerDocType", DB2DbType.Integer).Value = customerDocType
            _cmd.Parameters.Add("@customerDocNumber", DB2DbType.VarChar).Value = customerDocNumber
            _cmd.Parameters.Add("@ProductId", DB2DbType.VarChar).Value = ProductId
            _cmd.Parameters.Add("@Currency", DB2DbType.Integer).Value = Currency

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

        Catch ex As Exception
            Logger.Error(ex.Message)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function GETEXTLIMT(ByVal customerDocType As Integer, ByVal customerDocNumber As String, ByVal ProductId As String) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETEXTLIMT"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@DocType", DB2DbType.Integer).Value = customerDocType
            _cmd.Parameters.Add("@document", DB2DbType.VarChar).Value = customerDocNumber
            _cmd.Parameters.Add("@ProductId", DB2DbType.VarChar).Value = ProductId

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

            LogResult(dt)

        Catch ex As Exception
            Logger.Error(ex.Message)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function GETPRODUCTDATA(ByVal productType As Integer, ByVal productId As String, ByVal currency As Short) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.GETPRDTDTA"
            _cmd.CommandType = CommandType.StoredProcedure
            '1 Registros. Columnas Respuesta ----------------
            'ROW 1 >  Campo: PRODUCTTYPE Tipo:System.Int32 Valor:6
            'ROW 2 >  Campo: PRODUCTID Tipo:System.Decimal Valor:4249070000000030
            'ROW 3 >  Campo: CURRENCY Tipo:System.Int32 Valor:1
            'ROW(4 > Campo): CUSTOMERNAME(Tipo) : System.String(Valor) : KEYLIN(FONDEUR)
            'ROW 5 >  Campo: OPERATIONRESULT Tipo:System.Int32 Valor:0
            'ROW(6 > Campo): OPERATIONDESCRIPTION(Tipo) : System.String(Valor) : CONSULTA(EXITOSA)
            ' PRODUCTTYPE, PRODUCTID, CURRENCY, CUSTOMERNAME, OPERATIONRESULT, OPERATIONDESCRIPTION

            _cmd.Parameters.Add("ProductType", DB2DbType.Integer).Value = productType
            _cmd.Parameters.Add("ProductId", DB2DbType.VarChar).Value = productId
            _cmd.Parameters.Add("Currency", DB2DbType.VarChar).Value = currency

            '_cmd.Parameters.Add("OperationResult", DbType.Integer).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("OperationDescription", DbType.VarChar).Direction = ParameterDirection.Output

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0
            Me.ErrorDescription = "Exitosa"
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

        Catch ex As Exception
            Logger.Error(ex.Message, ex)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function CreditoITransCard(ByVal customerDocType As String, ByVal customerDocNumber As String, ByVal sourceProductType As Short, ByVal sourceProductId As String, ByVal destinationDocType As Short, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As Short, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Short, ByVal concept As String) As DataTable
        Dim result As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            'destinationDocType
            'destinationDocNum
            'destinationProductType()
            'destinationProductId
            'amount()
            'currency()
            'transferId()
            'OperationResult()
            'OperationDescripcion()


            _cmd.CommandText = "IBSLIBOBJ.PAGO_TARJ"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("destinationDocType", DB2DbType.Integer).Value = destinationDocType
            _cmd.Parameters.Add("destinationDocNum", DB2DbType.VarChar).Value = destinationDocNum
            _cmd.Parameters.Add("destinationProductType", DB2DbType.VarChar).Value = destinationProductType
            _cmd.Parameters.Add("destinationProductId", DB2DbType.VarChar).Value = destinationProductId
            _cmd.Parameters.Add("amount", DB2DbType.Decimal).Value = amount
            _cmd.Parameters.Add("currency", DB2DbType.Integer).Value = currency

            '_cmd.Parameters.Add("transferId", DbType.Decimal).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("OperationResult", DbType.Integer).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("OperationDescription", DbType.VarChar).Direction = ParameterDirection.Output

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(result)

            Me.CodigoError = 0
            Me.ErrorDescription = "Exitosa"
            Me.Secuencia = _reference.Next(99999, 999999)

            '._cmd.Parameters("PO_SECUENCIA").Value
            'Me.CodigoError = _cmd.Parameters("OperationResult").Value
            'Me.ErrorDescription = _cmd.Parameters("OperationDescription").Value
            'Me.Secuencia = _cmd.Parameters("transferId").Value
            'result = _cmd.Parameters("transferId").Value

        Catch ex As Exception
            Logger.Error(ex.Message, ex)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return result

    End Function

    Public Function ChangeDistributionStatus(ByVal productType As Integer, ByVal productId As String,
                                             ByVal RequestId As String, ByVal Status As Integer, ByVal Fecha As String, ByVal Hora As String) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.DISTRIBUIR"
            _cmd.CommandType = CommandType.StoredProcedure

            Select Case Status
                Case 1
                    _cmd.CommandText = "IBSLIBOBJ.DISTRIBUIR"
                Case 2
                    _cmd.CommandText = "IBSLIBOBJ.SP_ENTREGA"
                Case 3
                    _cmd.CommandText = "IBSLIBOBJ.SP_TRITURA"
            End Select

            _cmd.Parameters.Add("@ProductId", DB2DbType.VarChar, 16).Value = productId
            _cmd.Parameters.Add("@RequestId", DB2DbType.VarChar, 11).Value = RequestId
            _cmd.Parameters.Add("@Status", DB2DbType.Integer).Value = Status
            _cmd.Parameters.Add("@Fecha", DB2DbType.VarChar, 10).Value = Fecha
            _cmd.Parameters.Add("@Hora", DB2DbType.VarChar, 10).Value = Hora


            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Me.CodigoError = 0
            Me.ErrorDescription = "Exitosa"
            Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

        Catch ex As Exception
            Logger.Error(ex.Message, ex)
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function VCASH_ADVC(via As Integer, sourceProductId As String, sourceProductType As Integer, concept As String, amount As Decimal) As DataTable

        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.VCASH_ADVC"
            _cmd.CommandType = CommandType.StoredProcedure

            '_cmd.Parameters.Add("@Via", DbType.Integer).Value = via
            _cmd.Parameters.Add("@ViaEntrada", DB2DbType.Integer).Value = via

            '_cmd.Parameters.Add("@sourceProductId", DbType.VarChar).Value = sourceProductId
            _cmd.Parameters.Add("@F002", DB2DbType.Char).Value = sourceProductId

            '_cmd.Parameters.Add("@sourceProductType", DbType.Integer).Value = sourceProductType
            _cmd.Parameters.Add("@TipoProducto", DB2DbType.VarChar).Value = sourceProductType.ToString

            '_cmd.Parameters.Add("@concept", DbType.VarChar).Value = concept
            _cmd.Parameters.Add("@DescripcionTransaccion", DB2DbType.VarChar).Value = concept

            '_cmd.Parameters.Add("@amount", DbType.Decimal).Value = amount
            _cmd.Parameters.Add("@ValorTransaccion", DB2DbType.Decimal).Value = amount

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            'Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            'Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            'Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value
            LogResult(dt)

            'Me.CodigoError = _cmd.Parameters("CODIGO_RESPUESTA").Value
            'Me.ErrorDescription = _cmd.Parameters("DESCRIPCION_RESPUESTA").Value
            'Me.Secuencia = _cmd.Parameters("REFERENCE_ID").Value

            'Me.CodigoError = 0
            'Me.ErrorDescription = "Exitoso"
            'Me.Secuencia = _reference.Next(99999, 999999)

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || { If(not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "ND") } ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function SCASH_ADVC(via As Integer, sourceProductId As String, sourceProductType As Integer, concept As String, amount As Decimal) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.SCASH_ADVC"
            _cmd.CommandType = CommandType.StoredProcedure

            '_cmd.Parameters.Add("@Via", DbType.Integer).Value = via
            _cmd.Parameters.Add("@ViaEntrada", DB2DbType.Integer).Value = via

            '_cmd.Parameters.Add("@sourceProductId", DbType.VarChar).Value = sourceProductId
            _cmd.Parameters.Add("@F002", DB2DbType.Char).Value = sourceProductId

            '_cmd.Parameters.Add("@sourceProductType", DbType.Integer).Value = sourceProductType
            _cmd.Parameters.Add("@TipoProducto", DB2DbType.VarChar).Value = sourceProductType.ToString

            '_cmd.Parameters.Add("@concept", DbType.VarChar).Value = concept
            _cmd.Parameters.Add("@DescripcionTransaccion", DB2DbType.VarChar).Value = concept

            '_cmd.Parameters.Add("@amount", DbType.Decimal).Value = amount
            _cmd.Parameters.Add("@ValorTransaccion", DB2DbType.Decimal).Value = amount

            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            'Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            'Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            'Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value
            LogResult(dt)

            'Me.CodigoError = 0
            'Me.ErrorDescription = "Exitoso"
            Me.Secuencia = _reference.Next(99999, 999999)

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || { If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "ND") } ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt

    End Function

    Public Function RCASH_ADVC(via As Integer, sourceProductId As String, sourceProductType As Integer, amount As Decimal, TransferID As Decimal) As DataTable
        Dim dt As New DataTable
        Dim _reference As New Random

        Try
            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "IBSLIBOBJ.RCASH_ADVC"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("@VIAENTRADA", DB2DbType.Integer).Value = via
            _cmd.Parameters.Add("@F002", DB2DbType.Char, 16).Value = sourceProductId
            _cmd.Parameters.Add("@TIPOPRODUCTO", DB2DbType.Char, 1).Value = sourceProductType.ToString
            _cmd.Parameters.Add("@VALORTRANSACCION", DB2DbType.Decimal).Value = amount
            _cmd.Parameters.Add("@F037", DB2DbType.Char, 12).Value = TransferID.ToString

            Logger.Debug("CommandParameters:" + _cmd.Parameters.ToString)
            Logger.Debug("Ejecutando:" + _cmd.CommandText)

            Dim da As New DB2DataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            'Me.CodigoError = 0 '  _cmd.Parameters("PO_COD_ERR").Value
            'Me.ErrorDescription = "Exitoso" '_cmd.Parameters("PO_DESC_ERR").Value
            'Me.Secuencia = _reference.Next(99999, 999999)  '._cmd.Parameters("PO_SECUENCIA").Value

            'Me.CodigoError = _cmd.Parameters("Codigo_Respuesta").Value
            'Me.ErrorDescription = _cmd.Parameters("Descripcion_Respuesta").Value
            'Me.Secuencia = _reference.Next(99999, 999999)  '_cmd.Parameters("REFERENCE_ID").Value

            LogResult(dt)

        Catch ex As Exception
            Logger.Error($"ExceptionRCASH_ADVC:{ex.Message}  || { If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "ND") } ")
            Me.ErrorDescription = ex.Message
            Me.CodigoError = 1
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Return dt
    End Function
End Class
