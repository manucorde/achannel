﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.IO
Imports System.Xml.Serialization
Imports System.Globalization

Public Module Serializer
    Sub New()
    End Sub

    Public Function Deserialize(ByVal type As Type, ByVal serializedObject As String) As Object
        Dim result As Object
        Dim textReader As TextReader = Nothing

        Try
            Dim xmlSerializer As New XmlSerializer(type)
            textReader = New StringReader(serializedObject)
            result = xmlSerializer.Deserialize(textReader)
        Finally
            If textReader IsNot Nothing Then
                textReader.Close()
            End If
        End Try
        Return result
    End Function

    Public Function Deserialize(Of T)(ByVal serializedObject As String) As T
        Dim result As T
        Dim textReader As TextReader = Nothing

        Try
            Dim xmlSerializer As New XmlSerializer(GetType(T))
            textReader = New StringReader(serializedObject)
            result = DirectCast(xmlSerializer.Deserialize(textReader), T)
        Finally
            If textReader IsNot Nothing Then
                textReader.Close()
            End If
        End Try
        Return result
    End Function

    Public Function Serialize(ByVal serializableObject As Object) As String
        Return Serialize(serializableObject.[GetType](), serializableObject)
    End Function

    Public Function Serialize(ByVal type As Type, ByVal serializableObject As Object) As String
        Dim result As String = Nothing
        Dim textWriter As TextWriter = Nothing
        Try
            Dim xmlSerializer As New XmlSerializer(type)
            textWriter = New StringWriter(CultureInfo.InvariantCulture)
            xmlSerializer.Serialize(textWriter, serializableObject)
            result = textWriter.ToString()
        Finally
            If textWriter IsNot Nothing Then
                textWriter.Close()
            End If
        End Try

        Return result
    End Function

    Public Function Serialize(Of T)(ByVal serializableObject As T) As String
        Dim result As String = Nothing
        Dim textWriter As TextWriter = Nothing
        Try
            Dim xmlSerializer As New XmlSerializer(GetType(T))
            textWriter = New StringWriter(CultureInfo.InvariantCulture)
            xmlSerializer.Serialize(textWriter, serializableObject)
            result = textWriter.ToString()
        Finally
            If textWriter IsNot Nothing Then
                textWriter.Close()
            End If
        End Try

        Return result
    End Function
End Module
