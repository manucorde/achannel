﻿Imports System
Imports System.Reflection
Imports System.Collections.Generic
Imports System.Text

Public Class HashPropertyReflector
    ''' <summary>
    ''' Gets the string to hash from the given object inspecting
    ''' object properties decorated with HashIncludeAttribute
    ''' </summary>
    ''' <typeparam name="T">The object type</typeparam>
    ''' <param name="objectToReflect">The object to get the hash string</param>
    ''' <returns>The string to be hashed</returns>
    Public Shared Function GetHashFromObject(Of T As Class)(ByVal objectToReflect As T) As String
        Dim result As String = Nothing
        Dim orderedValues As SortedList(Of UInteger, Object) = Nothing

        Try
            If objectToReflect IsNot Nothing Then
                orderedValues = New SortedList(Of UInteger, Object)()

                For Each [property] As PropertyInfo In GetType(T).GetProperties()
                    Dim attributes As Object() = [property].GetCustomAttributes(GetType(HashIncludeAttribute), False)
                    If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                        Dim att As HashIncludeAttribute = TryCast(attributes(0), HashIncludeAttribute)

                        If att IsNot Nothing Then
                            orderedValues.Add(att.Order, [property].GetValue(objectToReflect, Nothing).ToString())
                        End If
                    End If
                Next
            End If

            If orderedValues IsNot Nothing AndAlso orderedValues.Values.Count > 0 Then
                For Each obj As Object In orderedValues.Values
                    result += obj.ToString()
                Next
            End If
        Catch generatedExceptionName As Exception
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Gets the string corresponding to the message content
    ''' in order to compute the content hash value
    ''' </summary>
    ''' <param name="contentFields">The array containing the content fields in the corresponding order</param>
    ''' <returns>A concatenated string containing all the object fields</returns>
    Public Shared Function GetHashFromArray(ByVal contentFields As Object()) As String
        Dim result As String = String.Empty

        If contentFields IsNot Nothing Then
            For Each obj As Object In contentFields
                If obj IsNot Nothing Then
                    If TypeOf obj Is Date Then
                        result += CType(obj, Date).ToString("dd/MM/yyyy")
                    Else
                        result += obj.ToString()
                    End If
                End If
            Next
        End If

        Return result
    End Function
End Class
