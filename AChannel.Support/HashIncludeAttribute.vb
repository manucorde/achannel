﻿
<AttributeUsage(AttributeTargets.[Property], AllowMultiple:=False, Inherited:=False)> _
Public Class HashIncludeAttribute
    Inherits Attribute
    Public Sub New()
        Order = 0
    End Sub

    Public Sub New(ByVal order__1 As UInteger)
        Order = order__1
    End Sub

    Private _Order As UInteger
    Public Property Order() As UInteger
        Get
            Return _Order
        End Get
        Set(ByVal value As UInteger)
            _Order = value
        End Set
    End Property
End Class
