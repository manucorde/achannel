﻿Imports log4net

Public Module Utils

    Private _isInfoEnabled As Boolean = False
    Private _isWarnEnabled As Boolean = False
    Private _isDebugEnabled As Boolean = False
    Private _logger As ILog
    Public Property Logger() As ILog
        Get
            If (_logger Is Nothing) Then
                _logger = LogManager.GetLogger("root")
                _isInfoEnabled = _logger.IsInfoEnabled
                _isWarnEnabled = _logger.IsWarnEnabled
                _isDebugEnabled = _logger.IsDebugEnabled
            End If
            Return _logger
        End Get
        Set(ByVal value As ILog)
            If (value Is Nothing) Then
                _logger = LogManager.GetLogger(Reflection.MethodBase.GetCurrentMethod.DeclaringType)
            Else
                _logger = value
            End If
        End Set
    End Property

    Public Property LoggerName() As String
        Get
            Return Logger.Logger.Name
        End Get
        Set(ByVal value As String)
            If value Is Nothing OrElse value = String.Empty Then
                Logger = Nothing
            Else
                Logger = LogManager.GetLogger(value)
            End If
        End Set
    End Property

    Public Enum LogType
        Debug
        Info
        Warn
        Error_
        Fatal
    End Enum

    Public Sub WriteLogError(ByVal _message As String, ByVal ex As Exception)
        If Logger.IsErrorEnabled Then
            Logger.Error(_message, ex)
        End If
    End Sub

    Public Sub WriteLogInfo(ByVal _message As String)
        If _isInfoEnabled Then
            Logger.Info(_message)
        End If
    End Sub

    Public Sub WriteLogWarn(ByVal _message As String)
        If _isWarnEnabled Then
            Logger.Warn(_message)
        End If
    End Sub

    Public Sub WriteLogDebug(ByVal _message As String)
        If Logger.IsDebugEnabled Then
            Logger.Debug(_message)
        End If
    End Sub

End Module
