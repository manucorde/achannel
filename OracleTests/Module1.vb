﻿Imports Devart.Data.Oracle
Imports System.Configuration
Imports AChannel.Core
Imports System.Data.DataTableExtensions

Module Module1

    Sub Main()
        Dim _conn As OracleConnection = Nothing
        Dim _connString As String = "User Id=FISA_OWNER;Password=DESA2009;Server=172.24.6.223;Direct=True;Sid=FISADES;Persist Security Info=True"
        Dim dt As New DataTable

        Try
            '' Dim _tmp As String = ConfigurationManager.ConnectionStrings("connString").ConnectionString
            'If _tmp <> String.Empty Then
            '    _connString = _tmp
            'End If

            _conn = New OracleConnection(_connString)
            Dim _cmd As New OracleCommand()
            Console.WriteLine("Server:{0} Home:{1} SID:{2} UID:{3} DataSource:{4}", _conn.Server, _conn.Home, _conn.Sid, _conn.UserId, _conn.DataSource)

            Dim customerDocType As String = "3"
            Dim customerDocNumber As String = "1-02-32240-6"
            Dim productId As String = "4010108468"

            Dim result As New Long
            Dim _item_debit As Short = 10
            Dim _item_credit As Short = 10
            Dim _operator As Integer = 738
            Dim _branch As Integer = 1
            Dim _office As Integer = 1
            Dim currency As Short = 1
            Dim _fisaCurrency As Integer = IIf(currency = 1, 0, IIf(currency = 3, 7, currency))
            Dim _terminal As String = "172.24.117.10"
            Dim _department As Integer = 16
            Dim transferId As Integer = 312


            _conn.Open()
            _cmd.Connection = _conn

            _cmd.CommandText = "pkg_ib_nomina.PRC_CONSULTA_NOM_IB"
            _cmd.CommandType = CommandType.StoredProcedure

            _cmd.Parameters.Add("pi_CustomerDocType", OracleDbType.VarChar).Value = customerDocType
            _cmd.Parameters.Add("pi_CustomerDocNumber", OracleDbType.VarChar).Value = customerDocNumber
            _cmd.Parameters.Add("pi_transferid", OracleDbType.VarChar).Value = transferId

            _cmd.Parameters.Add("Po_Cod_Respuesta_Gral", OracleDbType.VarChar).Direction = ParameterDirection.Output
            _cmd.Parameters.Add("Po_Descrip_Respuesta", OracleDbType.VarChar).Direction = ParameterDirection.Output
            _cmd.Parameters.Add("Po_Secuencia", OracleDbType.Number).Direction = ParameterDirection.Output
            _cmd.Parameters.Add("CS_SALIDA", OracleDbType.Cursor).Direction = ParameterDirection.Output


            Console.WriteLine("antes de ejecutar comando: " + _cmd.CommandText)

            Dim da As New OracleDataAdapter
            da.SelectCommand = _cmd

            da.Fill(dt)

            Console.WriteLine("despues de ejecutar comando")

            Console.WriteLine(String.Format("Po_Cod_Respuesta_Gral: {0}", _cmd.Parameters("Po_Cod_Respuesta_Gral").Value))
            Console.WriteLine(String.Format("Po_Descrip_Respuesta: {0}", _cmd.Parameters("Po_Descrip_Respuesta").Value))
            Console.WriteLine(String.Format("Po_Secuencia: {0}", _cmd.Parameters("Po_Secuencia").Value))

            'If IsDBNull(_cmd.Parameters("pdo_processdate").Value) Then
            '    Console.WriteLine("pdo_processdate: -Nulo-")
            'Else
            '    Console.WriteLine(String.Format("pdo_processdate: -{0}-", _cmd.Parameters("pdo_processdate").Value))
            'End If

            LogResult(dt)

        Catch ex As Exception
            Console.WriteLine(ex.ToString)
            Console.WriteLine(ex.StackTrace)
        Finally
            If _conn.State = ConnectionState.Open Then _conn.Close()
        End Try
        Console.ReadLine()
    End Sub
    Private Sub LogResult(ByVal dt As DataTable)
        Console.WriteLine("{0} Registros. Columnas Respuesta ----------------", dt.Rows.Count)

        Dim i As Integer
        For Each r As DataRow In dt.Rows
            i = 1
            For Each c As DataColumn In dt.Columns
                Console.WriteLine("ROW {3} >  Campo: {0} Tipo:{1} Valor:'{2}'", c.ColumnName, c.DataType, r.Item(c.ColumnName), i)
                i += 1
            Next
        Next

    End Sub


End Module


          