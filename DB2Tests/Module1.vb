﻿Imports System.Configuration
Imports AChannel.Core
Imports AChannel.Data
Imports log4net
Imports AChannel.Core.Enumerations
Imports IBM.Data.DB2

'Imports IBM.Data.DB2

Module Module1

    Sub Main()
        ' Dim _conn As OdbcConnection = Nothing
        Dim _conn As DB2Connection = Nothing
        Dim da As New DB2DataAdapter
        Dim _cmd As New DB2Command()

        Dim _connString As String = System.Configuration.ConfigurationManager.ConnectionStrings("AS400Connection").ConnectionString


        'System.Configuration."Provider=IBMDA400; Data Source = 172.24.6.233; User Id= Agilisa; Password = prueba; Default Collection=IBANKING; providerName="System.Data.OleDb"/>

        'Dim _connString As String = "Driver={Client Access ODBC Driver (32-bit)};System=172.24.6.233;Uid=Agilisa;Pwd=prueba; Default                Collection=IBANKING;"
        'DATADIRECT "Host=172.24.6.233;Port=446;User ID=Agilisa;Password=prueba;Database=B10DA5B3" '
        'IBM "Server=172.24.6.233;Port=446;Database=B110DA5B3;CurrentSchema=IBANKING;UId=Agilisa;Pwd=prueba;"
        'ODBC: Driver={Client Access ODBC Driver (32-bit)};System=172.24.6.233;Uid=Agilisa;Pwd=prueba; Default                Collection=IBANKING;
        'Dim _tmp As String = System.Configuration.ConfigurationManager.ConnectionStrings("AS400Connection").ConnectionString
        'If _tmp <> String.Empty Then
        '    _connString = _tmp
        'End If

        Dim c1 As DB2ConnectionStringBuilder = New DB2ConnectionStringBuilder()
        'c1.Database = "B10DA5B3"
        'c1.DatabaseName = "B10DA5B3"
        'c1.Host = "172.24.6.233"
        'c1.Port = "446"
        'c1.UserID = "agilisa"
        'c1.Password = "prueba"
        'c1.CurrentSchema = "IBANKING"
        '_connString = c1.ToString

        _conn = New DB2Connection(_connString)

        Console.WriteLine(Now.ToString + ":" + Now.Millisecond.ToString)
            Console.WriteLine(_connString)

            Dim customerDocType As String = "1"
            'Dim customerDocNumber As String = "001-0197603-3"
            Dim customerDocNumber As String = "001-0323059-5"
            Dim productId As String = "4249070000023164"
            Dim period As Integer = 1
            Dim productType As ProductTypes = ProductTypes.CreditCard
            Dim currency As Integer = 1
            Dim dt As New DataTable
            Dim _result As New OperationsDB
            Dim _codigoerror As Int32
            Dim _errordescripcion As String
            Dim _secuencia As Int64
            Dim amount As Decimal = 50

            _conn.Open()
            _cmd.Connection = _conn



            '------------------------------
            customerDocNumber = "001-0931437-7"
            productId = "4021080000000033"
            currency = 1



            Dim Fecha As String = Now.ToString("yyyyMMdd")
            Dim Hora As String = Now.ToString("hhmmss")
            Dim _count As Integer = 0
            Dim _codigoNotificacion As Integer = 0
            Try

                '    Dim sqlCmd As String = GetCommand(_CmdString, item.PAN, _constSolicitud, Fecha, Hora)
                '    Dim sqlCmd As String = "IBSLIBOBJ.DISTRIBUIR"

                _cmd.CommandText = "IBSLIBOBJ.GETCRECRDS"
                _cmd.CommandType = CommandType.StoredProcedure

                _cmd.Parameters.Add("@DOCTYPE", DbType.String).Value = customerDocType
                _cmd.Parameters.Add("@DOCUMENT", DbType.String).Value = customerDocNumber

                Logger.Debug("Ejecutando:" + _cmd.CommandText)

                Console.WriteLine(Now.ToString + ":menos" + Now.Millisecond.ToString)
                Console.WriteLine("Ejecutando:" + _cmd.CommandText)
                Console.WriteLine(String.Format("Parametros. DocNumber: {0}, ProductType: {1}, product: {2}, Currency: {3} ", customerDocNumber, productType, productId, currency))

                da.SelectCommand = _cmd

                Console.WriteLine(Now.ToString + ":respondio:" + Now.Millisecond.ToString)

                da.Fill(dt)

                Console.WriteLine(Now.ToString + ":cero:" + Now.Millisecond.ToString)
                Console.WriteLine(String.Format("Cantidad de registros: {0}", dt.Rows.Count))

                LogResult(dt)

                Console.WriteLine(Now.ToString + ":uno:" + Now.Millisecond.ToString)

                'If dt.Rows.Count > 0 Then
                '        _result = (From i In dt.AsEnumerable() Select New OperationsDB With {.CodigoError = i("ProductId"), _
                '                                                                             .ErrorDescription = i("OPERATIONDESCRIPTION")}).ToList.FirstOrDefault
                'End If

                Console.WriteLine(Now.ToString + ":dos:" + Now.Millisecond.ToString)

                _codigoerror = _result.CodigoError
                Console.WriteLine(String.Format("_CodigoError: {0}", _codigoerror))
                _errordescripcion = _result.ErrorDescription
                Console.WriteLine(String.Format("_ErrorDescription: {0}", _errordescripcion))
                _secuencia = _result.Secuencia
                Console.WriteLine(String.Format("_Secuencia: {0}", _secuencia))






                Dim _result1 As List(Of CreditCard) = Nothing
            Dim nfo As Globalization.CultureInfo = New Globalization.CultureInfo("en-US")

            ''RC_CUENTAS_VISTA =numero, CUENTA, NOMBRE, MONEDA, MANCOMUNADA, ESTATUS, DISPONIBLE, BALANCE 
            ''If False Then
            'Dim _oper As New AChannel.Data.DB2Operations
            '_dt1 = _oper.PRC_OBTIENE_TARJETAS(customerDocType, customerDocNumber)

            'Me.OperationResult = _oper.CodigoError
            'Me.OperationDescription = _oper.ErrorDescription
            'Me.OperationId = _oper.Secuencia

            _result1 = (From i In dt.AsEnumerable() Select New CreditCard With {.productType = i("PRODUCTTYPE"),
                                                                             .productId = i("PRODUCTID"),
                                                                             .cardBrand = i("CARDBRAND"),
                                                                             .currency = i("CURRENCY"),
                                                                             .closingBal = i("CLOSINGBAL"),
                                                                             .paymentDeadline = DateTime.ParseExact(i("PAYMENTDEADLINE").ToString, "yyyyMMdd", nfo),
                                                                             .availableLimit = i("AVAILABLELIMIT"),
                                                                             .overdueAmount = i("OVERDUEAMOUNT")}).ToList
                'Else
                '    'Tipo de producto – 5           Producto – 4249070000002804
                '    'Tipo Tarjeta – 1               Moneda – 1
                '    'Balance al corte – 1,236.00    Fecha límite de pago – Junio 25/2010
                '    'Límite disponible – 3,764.00   Monto en atraso – 0.00

                '    _result = New List(Of CreditCard)
                '    _result.Add(New CreditCard With {.productType = ProductTypes.CreditCard, .productId = "4249070000002804", .currency = Currencies.PesoDominicano, _
                '                                   .closingBal = 1236.0, .paymentDeadline = New Date(2010, 6, 25), .availableLimit = 3764.0, .overdueAmount = 0, .cardBrand = CardBrand.VisaLocal})
                'End If






                Console.WriteLine("  >> Respuesta Validada")
                If Not _result1 Is Nothing AndAlso _result1.Count > 0 Then
                    For Each _account As CreditCard In _result1
                    Console.WriteLine(String.Format("  >> CreditCard:{0} Marca:{1} Monto Disponible:{2:N}", _account.productId, _account.cardBrand, _account.availableLimit))
                Next
                Else
                    Console.WriteLine("  >> Resultado vacio")
                End If


            Catch ex As Exception
            Console.WriteLine(Now.ToString + ":" + Now.Millisecond.ToString)
            Console.WriteLine(ex.ToString)
        Finally
            If _conn IsNot Nothing AndAlso _conn.State = ConnectionState.Open Then _conn.Close()
        End Try

        Console.WriteLine("Presione cualquier tecla...")
        Console.ReadLine()

    End Sub


    Private Sub LogResult(ByVal dt As DataTable)
        Console.WriteLine("{0} Registros. Columnas Respuesta ----------------", dt.Rows.Count)

        Dim i As Integer
        For Each r As DataRow In dt.Rows
            i = 1
            For Each c As DataColumn In dt.Columns
                Console.WriteLine("ROW {3} >  Campo: {0} Tipo:{1} Valor:'{2}'", c.ColumnName, c.DataType, r.Item(c.ColumnName), i)
                i += 1
            Next
        Next

    End Sub


End Module
