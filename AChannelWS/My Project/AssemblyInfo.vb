﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes
<Assembly: AssemblyTitle("AChannelWS")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Agilisa Technologies")> 
<Assembly: AssemblyProduct("AChannelWS")> 
<Assembly: AssemblyCopyright("Copyright © Agilisa Technologies 2010")> 
<Assembly: AssemblyTrademark("")> 
<Assembly: log4net.Config.XmlConfigurator(ConfigFile:="web.config", Watch:=True)> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("30c726e0-2e98-44ff-bab9-bbd411a8e9ee")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
