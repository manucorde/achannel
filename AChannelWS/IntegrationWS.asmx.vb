﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports AChannel.Core
Imports AChannel.Core.Enumerations
Imports AChannel.Support
Imports Agilisa.Security
Imports log4net
Imports System.Web.Script.Serialization

' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://Agilisa.com/")>
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)>
<ToolboxItem(False)>
Public Class IntegrationWS
    Inherits System.Web.Services.WebService

    Public requestHeader As RequestHeader
    Public responseHeader As ResponseHeader

    <WebMethod(Description:="Lista de tarjetas del usuario individual actualmente logueado al sistema.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getCreditCards(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of CreditCard)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)

        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of CreditCard) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getCreditCards(customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene los movimientos de una tarjeta de crédito (no extralímite), del usuario individual actualmente logueado al sistema. El filtro período permite especificar los movimientos a recuperar")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getCreditCardTrans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String, ByVal period As Short, ByVal currency As Currencies) As List(Of CreditCardTransaction)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)

        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of CreditCardTransaction) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getCreditCardTrans(customerDocType, customerDocNumber, productId, period, currency)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)
        Return result
    End Function

    <WebMethod(Description:="Obtiene el detalle de una tarjeta del usuario individual actualmente logueado al sistema..")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getCreditCardDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String, ByVal Currency As Integer) As CreditCardDetail
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As CreditCardDetail = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getCreditCardDetail(customerDocType, customerDocNumber, productId, Currency)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene el pago mínimo y balance al corte de una tarjeta de crédito a partir de la identificación del usuario y del producto.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getCreditCardBalance(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal Currency As Currencies) As CreditCardBalance
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)

        Dim fecha_inicio As DateTime = Now

        Dim result As CreditCardBalance = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(2) {customerDocNumber, productId, Currency}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getCreditCardBalance(customerDocType, customerDocNumber, productType, productId, Currency)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene las adicionales de una tarjeta del usuario individual actualmente logueado al sistema.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getAdditionalCards(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String, ByVal currency As Currencies) As List(Of AdditionalCreditCard)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)

        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of AdditionalCreditCard) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getAdditionalCards(customerDocType, customerDocNumber, productId, currency)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)
        Return result
    End Function

    <WebMethod(Description:="Obtiene movimientos de una tarjeta de crédito extralímite del usuario individual actualmente logueado al sistema (El servicio deberá traer todos los movimientos de los últimos 36 meses).")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getExtraLimitTrans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String) As List(Of ExtraLimitTransaction)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of ExtraLimitTransaction) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getExtraLimitTrans(customerDocType, customerDocNumber, productId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)
        Return result
    End Function

    <WebMethod(Description:="Obtiene la lista de cuentas de tipo cuenta corriente y caja de ahorro, asociadas al identificador (tipo y número de documento)")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getAccounts(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of Account)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of Account) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getAccounts(customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene el detalle de un producto partir del identificador del cliente, tipo de producto y numero de producto.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getAccountDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String) As AccountDetail
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As AccountDetail = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getAccountDetail(customerDocType, customerDocNumber, productType, productId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene el balance de una cuenta a partir de la identificación del usuario y el producto.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getAccountBalance(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String) As AccountBalance
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As AccountBalance = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getAccountBalance(customerDocType, customerDocNumber, productType, productId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene los datos de una cuenta a partir de la identificación del usuario y el producto.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getProductData(ByVal productType As ProductTypes, ByVal productId As String, ByVal currency As Currencies) As ProductData
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, productType, productId)
        Dim fecha_inicio As DateTime = Now

        Dim result As ProductData = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(0) {productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getProductData(productType, productId, currency)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene una lista con los movimientos de una cuenta a partir del identificador del usuario, la cuenta y de un filtro específico que indica los movimientos a recuperar.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getAccountTrans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal filter As FilterExpr) As List(Of AccountTransaction)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of AccountTransaction) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getAccountTrans(customerDocType, customerDocNumber, productType, productId, filter)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene los préstamos activos de un cliente a partir del identificador del usuario.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getLoans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of Loan)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of Loan) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getLoans(customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene Razon Social, asociadas al identificador (tipo y número de documento)")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getSocialReason(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As SocialReason
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As SocialReason = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getSocialReason(customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene las Inversiones (certificados de depósito) a partir del identificador del usuario.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getInvestments(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of Investment)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of Investment) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getInvestments(customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene los prestamos a partir del identificador del usuario.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getLoanDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String) As LoanDetail
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As LoanDetail = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getLoanDetail(customerDocType, customerDocNumber, productId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene el detalle de un certificado de deposito a partir del identificador del usuario.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getInvestmentDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String) As InvestmentDetail
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As InvestmentDetail = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getInvestmentDetail(customerDocType, customerDocNumber, productId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene la lista de cuentas, asociadas al identificador (tipo y número de documento)")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getProducts(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productTypes As Integer()) As List(Of Products)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber, productTypes)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of Products) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getProducts(customerDocType, customerDocNumber, productTypes)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod()>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function DoTransfer(ByVal request As TransferRequest) As TransferResponse ', Required:=True

        Dim result As New TransferResponse()
        Dim fecha_inicio As DateTime = Now

        Try

            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            'Create an array containing the content fields
            'included in the hash string (in the corresponding order)
            Dim contentFields As Object() = New Object(5) {request.CustomerDocNumber, request.SourceProductId, request.DestinationDocNum, request.DestinationProductId, request.Amount, request.Currency}

            If Not ValidateRequestHeader(contentFields, requestHeader) Then
                'Check message validity
                Throw New Exception("Invalid message header")
            End If

            'If message is valid, construct the response
            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey,
                                                        .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID,
                                                        .IPUser = requestHeader.IPUser,
                                                        .OperationID = 1,
                                                        .OperationResult = 1,
                                                        .OperationDescription = "Test"}

            'Sets the response message key
            'Dim result As New TransferResponse()
            SetResponseMessageKey(responseHeader, HashPropertyReflector.GetHashFromObject(Of TransferResponse)(result))

        Catch ex As Exception

            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0

        End Try

        Return result


    End Function

    <WebMethod(Description:="Obtiene las rutas de las imágenes de un cheque (anverso y reverso) dada la identificación del usuario, del producto y del cheque.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getImageCheck(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal checkNumber As String, ByVal checkDate As Date) As ImageCheck
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As ImageCheck = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(3) {customerDocNumber, productId, checkNumber, checkDate}, requestHeader) Then Throw New Exception("Invalid message")

            Dim _transaction As New Transactions
            result = _transaction.getImageCheck(customerDocType, customerDocNumber, productType, productId, checkNumber, checkDate)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Solicitud de Bloqueo de Cheques a partir del identificador del usuario y los datos del cheque.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function LockChecks(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal fromCheck As String, ByVal toCheck As String, ByVal lockReasonId As Short) As LockCheck
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As LockCheck = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(3) {customerDocNumber, productId, fromCheck, toCheck}, requestHeader) Then Throw New Exception("Invalid message")

            Dim _transaction As New Transactions
            result = _transaction.LockChecks(customerDocType, customerDocNumber, productType, productId, fromCheck, toCheck, lockReasonId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Busca el Estado de cuenta de un producto a partir del tipo y numero de producto.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getAccountStatementPDF(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal TypeStatus As TypeStatus, ByVal Currency As Currencies) As AccountStatementPDF
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As AccountStatementPDF = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message")

            Dim _transaction As New Transactions
            result = _transaction.getAccountStatementPDF(customerDocType, customerDocNumber, productType, productId, TypeStatus, Currency)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene la conversion de un monto de una moneda a otra.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getExchange(ByVal sourceCurrency As Currencies, ByVal targetCurrency As Currencies, ByVal amount As Decimal, ByVal translationDate As Date) As Exchange
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, sourceCurrency, targetCurrency)

        Dim result As Exchange = Nothing

        Dim fecha_inicio As DateTime = Now

        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(0) {amount}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getExchange(sourceCurrency, targetCurrency, amount, translationDate)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene la lista de cuentas de tipo pesos caribe, asociadas al identificador (tipo y número de documento).")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getPesosCaribeAccounts(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of PesosCaribeAccount)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)

        Dim result As List(Of PesosCaribeAccount) = Nothing

        Dim fecha_inicio As DateTime = Now

        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                           .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getALoyalAccounts(customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="El servicio realiza una transferencia de un producto (propio) a otro (propio, de terceros mismo banco o de terceros otro banco). Los datos enviados en el servicio corresponden a la transacción a realizar.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function Transfer(ByVal transferType As TransferTypes, ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As ProductTypes, ByVal sourceProductId As String, ByVal destinationDocType As DocumentType, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As ProductTypes, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Currencies, ByVal concept As String, ByVal sourceName As String, ByVal destinationBankSwift As String) As Long
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber, sourceProductId, destinationDocNum, destinationProductId, amount, currency)
        Dim fecha_inicio As DateTime = Now
        Dim result As Long = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(5) {customerDocNumber, sourceProductId, destinationDocNum, destinationProductId, amount, currency}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.Transfer(transferType, customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept, sourceName, destinationBankSwift)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Servicio de débito a cuenta. Los datos enviados en el servicio corresponden a la transacción a realizar.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function DebitTransac(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As ProductTypes, ByVal sourceProductId As String, ByVal amount As Decimal, ByVal currency As Currencies, ByVal concept As String, ByVal RubroCode As Integer) As Long
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber, sourceProductType, sourceProductId, amount, currency, concept, RubroCode)
        Dim fecha_inicio As DateTime = Now
        Dim result As Long = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(4) {customerDocNumber, sourceProductId, amount, currency, RubroCode}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.DebitTransac(customerDocType, customerDocNumber, sourceProductType, sourceProductId, amount, currency, concept, RubroCode)

            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function


    <WebMethod(Description:="El servicio notifica que se ha enviado un nuevo archivo. se ejecuta despúes de dejar en una carpeta un archivo de pagos masivos.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function MassTransfer(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal filename As String) As MassTransferOutput
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As MassTransferOutput = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.MassTransfer(customerDocType, customerDocNumber, filename)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="El servicio ordena la transferencia masiva cargada previamente en el Servicio de envio de archivo de pagos masivos.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function doMassTransfer(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal transferId As Long) As MassTransferResult
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As MassTransferResult = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.doMassTransfer(customerDocType, customerDocNumber, transferId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="El servicio devuelve el detalle de la transferencia masiva cargada previamente en el Servicio de envio de archivo de pagos masivos.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getMassTransferDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal transferId As Long) As List(Of TransferDetail)
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)
        Dim fecha_inicio As DateTime = Now

        Dim result As List(Of TransferDetail) = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(0) {customerDocNumber}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.GetMassTransferDetail(customerDocType, customerDocNumber, transferId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Notifica cambio de estatus en una tarjeta.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function ChangeDistributionStatus(ByVal productType As ProductTypes, ByVal productId As String, ByVal RequestId As String,
                                             ByVal Status As Integer, ByVal Fecha As String, ByVal Hora As String) As DistributionStatus
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, productType, productId)
        Dim fecha_inicio As DateTime = Now
        Dim result As DistributionStatus = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(2) {productType, productId, RequestId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.ChangeDistributionStatus(productType, productId, RequestId, Status, Fecha, Hora)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Busca los datos de un cliente.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getCustomerData(ByVal productId As String, ByVal RequestId As String) As Customer

        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, "", productId)

        Dim fecha_inicio As DateTime = Now
        Dim result As Customer = Nothing
        Dim productType As ProductTypes = ProductTypes.CreditCard

        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(1) {productId, RequestId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getCustomerData(productId, RequestId)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="A partir de una tarjeta de crédito, el concepto y el monto, se valida que el Avance de efectivo sea correcto y viable.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function validateCreditCardCashAdvance(sourceProductId As String, sourceProductType As Integer, concept As String, amount As Decimal) As CashAdvanceValidation
        'TODO: temporal: Esperando respuesta de Lilliam Reyes
        Dim currency As Currencies = Currencies.PesoDominicano
        Dim customerDocType As DocumentType = DocumentType.Cedula
        Dim customerDocNumber As String = "12345678912"
        Dim via As ViaType = ViaType.Internet_Banking

        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, DocumentType.NoValue, "", sourceProductId:=sourceProductId, amount:=amount, sourceProductType:=sourceProductType)
        Dim result As New CashAdvanceValidation
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                        .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(2) {sourceProductId, sourceProductType, amount}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.validateCreditCardCashAdvance(sourceProductId, sourceProductType, concept, amount, currency, customerDocType, customerDocNumber, via)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId
        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)
        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="A partir de una tarjeta de crédito origen, una cuenta (corriente o de ahorro) destino, el concepto y el monto, se procesa el avance, debitando la tarjeta y acreditando el monto en la cuenta destino, devolviendo una confirmación con el resultado de la transacción.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function storeCreditCardCashAdvance(sourceProductId As String, sourceProductType As Integer, destinationProductId As String, destinationProductType As String, concept As String, amount As Decimal) As CashAdvanceStore

        'TODO: temporal: Esperando respuesta de Lilliam Reyes
        Dim currency As Currencies = Currencies.PesoDominicano
        Dim customerDocType As DocumentType = DocumentType.Cedula
        Dim customerDocNumber As String = "12345678912"
        Dim via As ViaType = ViaType.Internet_Banking

        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, DocumentType.NoValue, "", sourceProductId:=sourceProductId, amount:=amount, sourceProductType:=sourceProductType)
        Dim result As New CashAdvanceStore
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                      .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}

            If Not ValidateRequestHeader(New Object(2) {sourceProductId, sourceProductType, amount}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.storeCreditCardCashAdvance(sourceProductId, sourceProductType, destinationProductId, destinationProductType, concept, amount, currency, via, customerDocType, customerDocNumber)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId
        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)
        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

    <WebMethod(Description:="Obtiene la comisión para una transferencia externa a partir de la identificación del usuario,  el producto y la via de la transferencia.")>
    <SoapHeader("requestHeader", Direction:=SoapHeaderDirection.[In])>
    <SoapHeader("responseHeader", Direction:=SoapHeaderDirection.Out)>
    Public Function getTransferCommission(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal transfervia As TransferTypes) As TransferCommission
        Me.LogInput(System.Reflection.MethodBase.GetCurrentMethod().Name, requestHeader, customerDocType, customerDocNumber)

        Dim result As TransferCommission = Nothing
        Try
            If requestHeader Is Nothing Then
                'Check for headers
                responseHeader = New ResponseHeader()
                Throw New Exception("Invalid headers")
            End If

            responseHeader = New ResponseHeader() With {.MarchantKey = requestHeader.MarchantKey, .UserID = requestHeader.UserID,
                                                       .SessionID = requestHeader.SessionID, .IPUser = requestHeader.IPUser}
            If Not ValidateRequestHeader(New Object(1) {customerDocNumber, productId}, requestHeader) Then Throw New Exception("Invalid message header")

            Dim _transaction As New Transactions
            result = _transaction.getTransferCommission(customerDocType, customerDocNumber, productType, productId, transfervia)
            responseHeader.OperationResult = _transaction.OperationResult
            responseHeader.OperationDescription = _transaction.OperationDescription
            responseHeader.OperationID = _transaction.OperationId

        Catch ex As Exception
            responseHeader.OperationResult = 1
            responseHeader.OperationDescription = ex.Message
            responseHeader.OperationID = 0
        End Try

        SetResponseMessageKey(responseHeader)

        LogOutput(System.Reflection.MethodBase.GetCurrentMethod().Name, responseHeader, result)
        Return result
    End Function

#Region "Validacion"

    Private Function ValidateRequestHeader(ByVal header As RequestHeader, ByVal contentHash As String) As Boolean
        Dim salt As String = ConfigurationManager.AppSettings("HashSalt").ToString()

        Dim messageString As String = header.MarchantKey + header.UserID + header.IPUser + header.SessionID + contentHash

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(messageString), New Encryption.Data(salt))
        Dim calculatedMessageKey As String = _security.Value.ToBase64

        Return header.MessageKey = calculatedMessageKey
    End Function

    ''' <summary>
    ''' Validates the information inside the request header
    ''' using the hash value received on the MessageKey
    ''' </summary>
    ''' <returns>True if valid, false if not</returns>
    Private Function ValidateRequestHeader(ByVal contentFields As Object(), ByVal header As RequestHeader) As Boolean
        If header Is Nothing Then
            'Check for headers
            responseHeader = New ResponseHeader()
            Throw New Exception("Invalid headers")
        End If

        Dim salt As String = ConfigurationManager.AppSettings("HashSalt").ToString()
        Dim saltBytes As Byte() = Encoding.[Default].GetBytes(salt)

        Dim valueToHash As String = requestHeader.MarchantKey + requestHeader.UserID + requestHeader.IPUser + requestHeader.SessionID + HashPropertyReflector.GetHashFromArray(contentFields)

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(valueToHash), New Encryption.Data(salt))
        Dim calculatedMessageKey As String = _security.Value.ToBase64

        ' Return SHA256Wrapper.VerifyHash(valueToHash, requestHeader.MessageKey)
        Return header.MessageKey = calculatedMessageKey
    End Function

    ''' <summary>
    ''' Computes the hash value to be sent on the response header
    ''' and sets the value of the response object
    ''' </summary>
    ''' <param name="header">The heder object used to compute and set the hash value</param>
    ''' <param name="contentHash">The message content hash</param>
    Private Sub SetResponseMessageKey(ByVal header As ResponseHeader, ByVal contentHash As String)
        Dim salt As String = ConfigurationManager.AppSettings("HashSalt").ToString()
        Dim saltBytes As Byte() = Encoding.[Default].GetBytes(salt)

        Dim messageString As String = header.OperationID.ToString() + header.OperationResult.ToString() + header.OperationDescription + header.MarchantKey + header.UserID.ToString() + header.IPUser + header.SessionID + contentHash

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(messageString), New Encryption.Data(salt))
        header.MessageKey = _security.Value.ToBase64
    End Sub

    ''' <summary>
    ''' Computes the hash value to be sent on the response header 
    ''' and sets the value of the response object
    ''' </summary>
    ''' <param name="header">The heder object used to compute and set the hash value</param>
    Private Sub SetResponseMessageKey(ByVal header As ResponseHeader)
        Dim salt As String = ConfigurationManager.AppSettings("HashSalt").ToString()
        Dim saltBytes As Byte() = Encoding.[Default].GetBytes(salt)

        Dim messageString As String = header.OperationID.ToString() _
                                      + header.OperationResult.ToString() _
                                      + header.OperationDescription _
                                      + header.MarchantKey _
                                      + header.UserID.ToString() _
                                      + header.IPUser _
                                      + header.SessionID ' + HashPropertyReflector.GetHashFromArray(New Object(0) {header.OperationID})

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(messageString), New Encryption.Data(salt))
        header.MessageKey = _security.Value.ToBase64
    End Sub

#End Region

#Region "Logging"

    Private js As JavaScriptSerializer = New JavaScriptSerializer()

    Private Sub LogInput(ByVal method As String, ByVal requestHeader As RequestHeader, ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, Optional ByVal sourceProductId As String = "", Optional ByVal destinationDocNum As String = "", Optional ByVal destinationProductId As String = "", Optional ByVal amount As Decimal = 0, Optional ByVal currency As Currencies = 1, Optional ByVal sourceProductType As Integer = -1, Optional ByVal sourceName As String = "", Optional ByVal destinationBankSwift As String = "")
        LogManager.GetLogger("root")
        If Logger.IsDebugEnabled Then
            Logger.DebugFormat("{0}. requestHeader={1}", method, js.Serialize(requestHeader))
            Logger.DebugFormat("{0}. Valores Entrada: customerType={1} customerDocNumber={2} source={3} destDocNum={4} destination={5} amount={6} currency={7} sourceProductType={8}", method, customerDocType, customerDocNumber, sourceProductId, destinationDocNum, destinationProductId, amount, currency, sourceProductType)
        End If
    End Sub

    Private Sub LogInput(ByVal method As String, ByVal requestHeader As RequestHeader, ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As ProductTypes, ByVal sourceProductId As String, ByVal amount As Decimal, ByVal currency As Currencies, ByVal concept As String, ByVal RubroCode As Integer)
        LogManager.GetLogger("root")
        If Logger.IsDebugEnabled Then
            Logger.DebugFormat("{0}. requestHeader={1}", method, js.Serialize(requestHeader))
            Logger.DebugFormat("{0}. Valores Entrada: customerDocType={1}, customerDocNumber={2}, sourceProductType={3}, sourceProductId={4}, amount={5}, currency={6}, concept={7}, RubroCode={8}", method, customerDocType, customerDocNumber, sourceProductType, sourceProductId, amount, currency, concept, RubroCode)
        End If
    End Sub

    Private Sub LogInput(ByVal method As String, ByVal requestHeader As RequestHeader, ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productTypes As Integer())
        LogManager.GetLogger("root")
        If Logger.IsDebugEnabled Then
            Logger.DebugFormat("{0}. requestHeader={1}", method, js.Serialize(requestHeader))
            Logger.DebugFormat("{0}. Valores Entrada: customerType={1} customerDocNumber={2} productTypes={3}", method, customerDocType, customerDocNumber, js.Serialize(productTypes))
        End If
    End Sub

    Private Sub LogOutput(ByVal method As String, ByVal responseHeader As ResponseHeader, ByVal result As Object)
        If Logger.IsDebugEnabled Then
            Logger.DebugFormat("{0}. Valores Salida: responseHeader={1} result={2}", method, js.Serialize(responseHeader), js.Serialize(result))
        End If
    End Sub



#End Region

End Class

