﻿using AChannelWS;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting.Web;
using AChannel.Core;
using System.Collections.Generic;

namespace TestProject1
{
    
    
    /// <summary>
    ///This is a test class for IntegrationWSTest and is intended
    ///to contain all IntegrationWSTest Unit Tests
    ///</summary>
    [TestClass()]
    public class IntegrationWSTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for getAccountTrans
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/AChannelWS")]
        public void getAccountTransTest()
        {
            IntegrationWS target = new IntegrationWS(); // TODO: Initialize to an appropriate value
            Enumerations.DocumentType customerDocType = new Enumerations.DocumentType(); // TODO: Initialize to an appropriate value
            string customerDocNumber = string.Empty; // TODO: Initialize to an appropriate value
            Enumerations.ProductTypes productType = new Enumerations.ProductTypes(); // TODO: Initialize to an appropriate value
            string productId = string.Empty; // TODO: Initialize to an appropriate value
            FilterExpr filter = null; // TODO: Initialize to an appropriate value
            List<AccountTransaction> expected = null; // TODO: Initialize to an appropriate value
            List<AccountTransaction> actual;
            actual = target.getAccountTrans(customerDocType, customerDocNumber, productType, productId, filter);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for getAccounts
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/AChannelWS")]
        public void getAccountsTest()
        {
            IntegrationWS target = new IntegrationWS(); // TODO: Initialize to an appropriate value
            Enumerations.DocumentType customerDocType = new Enumerations.DocumentType(); // TODO: Initialize to an appropriate value
            string customerDocNumber = string.Empty; // TODO: Initialize to an appropriate value
            List<Account> expected = null; // TODO: Initialize to an appropriate value
            List<Account> actual;
            actual = target.getAccounts(customerDocType, customerDocNumber);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for getInvestments
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/AChannelWS")]
        public void getInvestmentsTest()
        {
            IntegrationWS target = new IntegrationWS(); // TODO: Initialize to an appropriate value
            Enumerations.DocumentType customerDocType = new Enumerations.DocumentType(); // TODO: Initialize to an appropriate value
            string customerDocNumber = string.Empty; // TODO: Initialize to an appropriate value
            List<Investment> expected = null; // TODO: Initialize to an appropriate value
            List<Investment> actual;
            actual = target.getInvestments(customerDocType, customerDocNumber);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for getLoans
        ///</summary>
        // TODO: Ensure that the UrlToTest attribute specifies a URL to an ASP.NET page (for example,
        // http://.../Default.aspx). This is necessary for the unit test to be executed on the web server,
        // whether you are testing a page, web service, or a WCF service.
        [TestMethod()]
        [HostType("ASP.NET")]
        [UrlToTest("http://localhost/AChannelWS")]
        public void getLoansTest()
        {
            IntegrationWS target = new IntegrationWS(); // TODO: Initialize to an appropriate value
            Enumerations.DocumentType customerDocType = new Enumerations.DocumentType(); // TODO: Initialize to an appropriate value
            string customerDocNumber = string.Empty; // TODO: Initialize to an appropriate value
            List<Loan> expected = null; // TODO: Initialize to an appropriate value
            List<Loan> actual;
            actual = target.getLoans(customerDocType, customerDocNumber);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }
    }
}
