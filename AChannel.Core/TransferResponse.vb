﻿Imports System
Imports System.Xml.Serialization
Imports AChannel.Support

<Serializable()> _
Public Class TransferResponse
    Private _TransferId As Long
    <HashInclude(0)> _
    Public Property TransferId() As Long
        Get
            Return _TransferId
        End Get
        Set(ByVal value As Long)
            _TransferId = value
        End Set
    End Property

    Public Sub New()
        TransferId = 0
    End Sub
End Class
