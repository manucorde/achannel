﻿Imports System
Imports AChannel.Support

Public Class TransferRequest
    Public Sub New()

    End Sub

    Private _TransferType As Enumerations.TransferTypes
    Public Property TransferType() As Enumerations.TransferTypes
        Get
            Return _TransferType
        End Get
        Set(ByVal value As Enumerations.TransferTypes)
            _TransferType = value
        End Set
    End Property

    Private _CustomerDocType As Enumerations.DocumentType
    Public Property CustomerDocType() As Enumerations.DocumentType
        Get
            Return _CustomerDocType
        End Get
        Set(ByVal value As Enumerations.DocumentType)
            _CustomerDocType = value
        End Set
    End Property

    Private _CustomerDocNumber As String
    <HashInclude(0)> _
    Public Property CustomerDocNumber() As String
        Get
            Return _CustomerDocNumber
        End Get
        Set(ByVal value As String)
            _CustomerDocNumber = value
        End Set
    End Property

    Private _SourceProductType As Enumerations.ProductTypes
    Public Property SourceProductType() As Enumerations.ProductTypes
        Get
            Return _SourceProductType
        End Get
        Set(ByVal value As Enumerations.ProductTypes)
            _SourceProductType = value
        End Set
    End Property
    'Hay que revisar el enumerado
    Private _SourceProductId As String
    <HashInclude(1)> _
    Public Property SourceProductId() As String
        Get
            Return _SourceProductId
        End Get
        Set(ByVal value As String)
            _SourceProductId = value
        End Set
    End Property

    Private _DestinationDocType As Enumerations.DocumentType
    Public Property DestinationDocType() As Enumerations.DocumentType
        Get
            Return _DestinationDocType
        End Get
        Set(ByVal value As Enumerations.DocumentType)
            _DestinationDocType = value
        End Set
    End Property

    Private _DestinationDocNum As String
    <HashInclude(2)> _
    Public Property DestinationDocNum() As String
        Get
            Return _DestinationDocNum
        End Get
        Set(ByVal value As String)
            _DestinationDocNum = value
        End Set
    End Property

    Private _DestinationName As String
    Public Property DestinationName() As String
        Get
            Return _DestinationName
        End Get
        Set(ByVal value As String)
            _DestinationName = value
        End Set
    End Property

    Private _DestinationBank As Bank
    Public Property DestinationBank() As Bank
        Get
            Return _DestinationBank
        End Get
        Set(ByVal value As Bank)
            _DestinationBank = value
        End Set
    End Property

    Private _DestinationProductType As Enumerations.ProductTypes
    Public Property DestinationProductType() As Enumerations.ProductTypes
        Get
            Return _DestinationProductType
        End Get
        Set(ByVal value As Enumerations.ProductTypes)
            _DestinationProductType = value
        End Set
    End Property
    'Hay que revisar el enumerado
    Private _DestinationProductId As String
    <HashInclude(3)> _
    Public Property DestinationProductId() As String
        Get
            Return _DestinationProductId
        End Get
        Set(ByVal value As String)
            _DestinationProductId = value
        End Set
    End Property

    Private _Amount As Decimal
    <HashInclude(4)> _
    Public Property Amount() As Decimal
        Get
            Return _Amount
        End Get
        Set(ByVal value As Decimal)
            _Amount = value
        End Set
    End Property

    Private _Currency As Enumerations.Currencies
    <HashInclude(5)> _
    Public Property Currency() As Enumerations.Currencies
        Get
            Return _Currency
        End Get
        Set(ByVal value As Enumerations.Currencies)
            _Currency = value
        End Set
    End Property

    Private _Concept As String
    Public Property Concept() As String
        Get
            Return _Concept
        End Get
        Set(ByVal value As String)
            _Concept = value
        End Set
    End Property

    Private _CardPayType As Enumerations.CardPaymentType
    Public Property CardPayType() As Enumerations.CardPaymentType
        Get
            Return _CardPayType
        End Get
        Set(ByVal value As Enumerations.CardPaymentType)
            _CardPayType = value
        End Set
    End Property
End Class
