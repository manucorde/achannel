﻿Imports AChannel.Core.Enumerations
Imports System.Xml

Public Class Transactions

#Region "Respuesta"
    Public Property OperationResult() As Short

    Public Property OperationDescription() As String

    Public Property OperationId() As Long

#End Region

    Dim nfo As Globalization.CultureInfo = New Globalization.CultureInfo("en-US")

    ''' <summary>
    ''' Lista de tarjetas del usuario individual actualmente logueado al sistema.
    ''' </summary>
    ''' <param name="customerDocNumber">Numero de Tarjeta</param>
    ''' <param name="customerDocType">Tipo de Producto</param>
    Public Function getCreditCards(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of CreditCard)

        Dim _result As List(Of CreditCard) = Nothing
        Dim _dt1 As DataTable

        Try
            'RC_CUENTAS_VISTA =numero, CUENTA, NOMBRE, MONEDA, MANCOMUNADA, ESTATUS, DISPONIBLE, BALANCE 
            'If False Then
            Dim _oper As New AChannel.Data.DB2Operations
            _dt1 = _oper.PRC_OBTIENE_TARJETAS(customerDocType, customerDocNumber)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            _result = (From i In _dt1.AsEnumerable() Select New CreditCard With {.productType = i("PRODUCTTYPE"),
                                                                             .productId = i("PRODUCTID"),
                                                                             .cardBrand = i("CARDBRAND"),
                                                                             .currency = i("CURRENCY"),
                                                                             .closingBal = i("CLOSINGBAL"),
                                                                             .paymentDeadline = DateTime.ParseExact(i("PAYMENTDEADLINE").ToString, "yyyyMMdd", nfo),
                                                                             .availableLimit = i("AVAILABLELIMIT"),
                                                                             .overdueAmount = i("OVERDUEAMOUNT")}).ToList
            'Else
            '    'Tipo de producto – 5           Producto – 4249070000002804
            '    'Tipo Tarjeta – 1               Moneda – 1
            '    'Balance al corte – 1,236.00    Fecha límite de pago – Junio 25/2010
            '    'Límite disponible – 3,764.00   Monto en atraso – 0.00

            '    _result = New List(Of CreditCard)
            '    _result.Add(New CreditCard With {.productType = ProductTypes.CreditCard, .productId = "4249070000002804", .currency = Currencies.PesoDominicano, _
            '                                   .closingBal = 1236.0, .paymentDeadline = New Date(2010, 6, 25), .availableLimit = 3764.0, .overdueAmount = 0, .cardBrand = CardBrand.VisaLocal})
            'End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return _result
    End Function

    ''' <summary>
    ''' Obtiene el detalle de una tarjeta 
    ''' </summary>
    ''' <remarks>Obtiene el detalle de una tarjeta del usuario individual actualmente logueado al sistema.</remarks>
    Public Function getCreditCardDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String, ByVal Currency As Currencies) As CreditCardDetail
        Dim result As New CreditCardDetail
        Dim _dt1 As DataTable
        Dim _oper As New AChannel.Data.DB2Operations

        Try

            _dt1 = _oper.GETCDETAIL(customerDocType, customerDocNumber, productId, Currency)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() Select New CreditCardDetail With {.productType = i("productType"), _
                                                                                            .productId = i("productId"), _
                                                                                            .currency = i("currency"), _
                                                                                            .closingBal = i("closingBal").ToString, _
                                                                                            .creditLineCash = i("creditLineCash").ToString, _
                                                                                            .paymentDeadline = DateTime.ParseExact(i("PAYMENTDEADLINE").ToString, "yyyyMMdd", nfo), _
                                                                                            .minPayment = i("minPayment").ToString, _
                                                                                            .availableLimit = i("availableLimit").ToString, _
                                                                                            .closingDate = DateTime.ParseExact(i("closingDate").ToString, "yyyyMMdd", nfo), _
                                                                                            .expirationDate = DateAdd("d", -1, DateAdd("m", 1, Date.ParseExact(i("expirationDate").ToString + "01", "yyyyMMdd", nfo))), _
                                                                                            .lastPaymentDate = DateTime.ParseExact(i("lastPaymentDate").ToString, "yyyyMMdd", nfo), _
                                                                                            .lastPaymentAmount = i("lastPaymentAmount").ToString, _
                                                                                            .overdueAmount = i("overdueAmount").ToString, _
                                                                                            .cashBal = i("cashBal").ToString, _
                                                                                            .sheetDateBal = i("sheetDateBal").ToString, _
                                                                                            .overdueFees = i("overdueFees").ToString, _
                                                                                            .pesosCaribeAmount = getALoyalBalance(productId)}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function getCreditCardBalance(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal Currency As Currencies) As CreditCardBalance
        Dim result As New CreditCardBalance
        Dim _dt1 As DataTable
        Dim _oper As New AChannel.Data.DB2Operations

        Try

            _dt1 = _oper.GETCRDBAL(customerDocType, customerDocNumber, productType, productId, Currency)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() Select New CreditCardBalance With {.productType = i("productType"),
                                                                                          .productId = i("productId"),
                                                                                            .currency = i("Currency"),
                                                                                            .closingBal = i("closingBal").ToString,
                                                                                            .minPayment = i("minPayment").ToString}).ToList.FirstOrDefault
                ' TODO: las dos siguientes lineas fueron comentadas hasta que se implemente este campo en los Sistemas de Banco Caribe...  tambien debe habilitarse el campo en la estructura CreditCardbalance 
                ',
                '                               .Available = i("Available").ToString}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Movimientos de una tarjeta de crédito
    ''' </summary>
    ''' <remarks>Obtiene los movimientos de una tarjeta de crédito (no extralímite), del usuario individual actualmente logueado al sistema. El filtro “período” permite especificar los movimientos a recuperar.</remarks>
    Public Function getCreditCardTrans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String, ByVal period As Short, ByVal currency As Short) As List(Of CreditCardTransaction)
        Dim _result As List(Of CreditCardTransaction) = Nothing
        Dim _dt1 As DataTable

        Try
            'If False Then
            Dim _oper As New AChannel.Data.DB2Operations
            _dt1 = _oper.PRC_MOVIMIENTOS_TARJETA(customerDocType, customerDocNumber, productId, period, currency)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            _result = (From i In _dt1.AsEnumerable() Select New CreditCardTransaction With {.ProductId = i("productId"), _
                                                                                             .TransactionId = i("TransactionId"), _
                                                                                             .TransactionDate = DateTime.ParseExact(i("Date").ToString, "yyyyMMdd", nfo), _
                                                                                             .Description = i("Description"), _
                                                                                             .cardHolder = i("cardHolder"), _
                                                                                             .Amount = i("Amount").ToString, _
                                                                                             .DebitOrCredit = i("debitOrCredit"), _
                                                                                             .Currency = i("currency")}).ToList
            'Else
            '    _result = New List(Of CreditCardTransaction)
            '    'Producto – 4249070000002804
            '    'Número de Transacción – 100
            '    'Fecha – Abril 01/2010
            '    'Descripción – Prueba 1
            '    'Cliente – Prueba 1
            '    'Monto – 100.00
            '    'Débito o Crédito – Débito
            '    'Moneda – 1
            '    _result.Add(New CreditCardTransaction With {.ProductId = "4249070000002804", .TransactionId = 100, .TransactionDate = New Date(2010, 4, 1), .Description = "Prueba 1", _
            '                                                .cardHolder = "Prueba 1", .Amount = 100, .DebitOrCredit = 0, .Currency = Currencies.PesoDominicano})

            '    'Producto – 4249070000002804
            '    'Número de Transacción – 200
            '    'Fecha – Abril 02/2010
            '    'Descripción – Prueba 2
            '    'Cliente – Prueba 2
            '    'Monto – 200.00
            '    'Débito o Crédito – Débito
            '    'Moneda – 1
            '    _result.Add(New CreditCardTransaction With {.ProductId = "4249070000002804", .TransactionId = 200, .TransactionDate = New Date(2010, 4, 2), .Description = "Prueba 2", _
            '                                                .cardHolder = "Prueba 2", .Amount = 200, .DebitOrCredit = 0, .Currency = Currencies.PesoDominicano})

            '    'Producto – 4249070000002804
            '    'Número de Transacción – 300
            '    'Fecha – Abril 03/2010
            '    'Descripción – Prueba 3
            '    'Cliente – Prueba 3
            '    'Monto – 300.00
            '    'Débito o Crédito – Débito
            '    'Moneda – 1
            '    _result.Add(New CreditCardTransaction With {.ProductId = "4249070000002804", .TransactionId = 300, .TransactionDate = New Date(2010, 4, 3), .Description = "Prueba 3", _
            '                                                .cardHolder = "Prueba 3", .Amount = 300, .DebitOrCredit = 0, .Currency = Currencies.PesoDominicano})

            '    'Producto – 4249070000002804
            '    'Número de Transacción – 400
            '    'Fecha – Abril 04/2010
            '    'Descripción – Prueba 4
            '    'Cliente – Prueba 4
            '    'Monto – 400.00
            '    'Débito o Crédito – Débito
            '    'Moneda – 1
            '    _result.Add(New CreditCardTransaction With {.ProductId = "4249070000002804", .TransactionId = 400, .TransactionDate = New Date(2010, 4, 4), .Description = "Prueba 4", _
            '                                .cardHolder = "Prueba 4", .Amount = 400, .DebitOrCredit = 1, .Currency = Currencies.PesoDominicano})

            '    'Producto – 4249070000002804
            '    'Número de Transacción – 500
            '    'Fecha – Abril 05/2010
            '    'Descripción – Prueba 5
            '    'Cliente – Prueba 5
            '    'Monto – 500.00
            '    'Débito o Crédito – Débito
            '    'Moneda – 1
            '    _result.Add(New CreditCardTransaction With {.ProductId = "4249070000002804", .TransactionId = 500, .TransactionDate = New Date(2010, 4, 5), .Description = "Prueba 5", _
            '                                .cardHolder = "Prueba 5", .Amount = 500, .DebitOrCredit = 1, .Currency = Currencies.PesoDominicano})


            'End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try
        Return _result
    End Function

    ''' <summary>
    ''' Obtiene movimientos de una tarjeta de crédito extralímite
    ''' </summary>
    ''' <remarks>Obtiene movimientos de una tarjeta de crédito extralímite del usuario individual actualmente logueado al sistema. (El servicio deberá traer todos los movimientos de los últimos 36 meses)</remarks>
    Public Function getExtraLimitTrans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String) As List(Of ExtraLimitTransaction)
        Dim _result As List(Of ExtraLimitTransaction) = Nothing
        Dim _dt1 As DataTable

        Try

            ' If False Then
            Dim _oper As New AChannel.Data.DB2Operations
            _dt1 = _oper.GETEXTLIMT(customerDocType, customerDocNumber, productId)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            _result = (From i In _dt1.AsEnumerable() Select New ExtraLimitTransaction With {.ProductId = i("productId"), _
                                                                                            .TransactionId = i("TransactionId"), _
                                                                                            .TransactionDate = DateTime.ParseExact(i("Date").ToString, "yyyyMMdd", nfo), _
                                                                                            .Description = i("Description"), _
                                                                                            .Amount = i("Amount").ToString, _
                                                                                            .DebitOrCredit = i("DebitOrCredit"), _
                                                                                            .Currency = i("Currency"), _
                                                                                            .cashBal = i("cashBal").ToString, _
                                                                                            .term = i("term").ToString, _
                                                                                            .outstandingQuota = i("outstandingQuota").ToString}).ToList
            'Else
            '    _result = New List(Of ExtraLimitTransaction)
            '    _result.Add(New ExtraLimitTransaction With {.ProductId = productId, .TransactionId = 1, .TransactionDate = Now.ToString("dd/mmm/yyyy"), .Description = "PRUEBAS", .Amount = 11234.54, .DebitOrCredit = "0", .Currency = 1, .cashBal = 6544.34, .term = 24, .outstandingQuota = 15})

            'End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try
        Return _result
    End Function

    ''' <summary>
    ''' Obtiene las adicionales de una tarjeta 
    ''' </summary>
    ''' <remarks>Obtiene las adicionales de una tarjeta del usuario individual actualmente logueado al sistema..</remarks>
    Public Function getAdditionalCards(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String, ByVal currency As Short) As List(Of AdditionalCreditCard)
        Dim _result As List(Of AdditionalCreditCard) = Nothing
        Dim _dt1 As DataTable

        Try

            ' If False Then
            Dim _oper As New AChannel.Data.DB2Operations
            _dt1 = _oper.GETADCCRDS(customerDocType, customerDocNumber, productId, currency)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            _result = (From i In _dt1.AsEnumerable() Select New AdditionalCreditCard With {.productId = i("productId"), _
                                                                             .additionalCardNum = i("additionalCardNum"), _
                                                                             .additionalCardHolder = i("additionalCardHolder")}).ToList
            'Else
            '_result = New List(Of AdditionalCreditCard)
            '_result.Add(New AdditionalCreditCard With {.productId = productId, .additionalCardNum = "4249070000002804", .additionalCardHolder = "JUAN PEREZ"})
            '_result.Add(New AdditionalCreditCard With {.productId = productId, .additionalCardNum = "4249070000002805", .additionalCardHolder = "JOSE PEREZ"})

            'End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try
        Return _result
    End Function

    ''' <summary>
    ''' Obtiene las cuentas
    ''' </summary>
    ''' <remarks>Obtiene la lista de cuentas de tipo cuenta corriente y caja de ahorro, asociadas al identificador (tipo y número de documento).</remarks>
    ''' <param name="customerDocType">Tipo de Documento</param>
    ''' <param name="customerDocNumber">Numero de Documento</param>
    ''' <returns></returns>
    Public Function getAccounts(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of Account)

        Dim result As New List(Of Account)
        Dim _dt1 As DataTable

        Try
            'RC_CUENTAS_VISTA =numero, CUENTA, NOMBRE, MONEDA, MANCOMUNADA, ESTATUS, DISPONIBLE, BALANCE 
            Dim _oper As New AChannel.Data.OracleOperations
            _dt1 = _oper.PRC_OBTIENE_CUENTAS_VISTA(customerDocType, customerDocNumber)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result = (From i In _dt1.AsEnumerable() Select New Account With {.ProductType = i("TIPOPRODUCTO"), _
                                                                             .ProductId = i("VIS_NUMCUE"), _
                                                                             .SocialReason = i("CLI_NOMBRE"), _
                                                                             .Currency = i("MONEDA"), _
                                                                             .JointAccount = (i("MANCOMUNADA") = "S"), _
                                                                             .TransferEnable = (i("PERMITEDEBITO") = "S"), _
                                                                             .AvailableBal = i("SALDO_DISPONIBLE"), _
                                                                             .TotalAmount = i("SALDO_TOTAL"), _
                                                                             .regionalAccountID = i("ctaregional")}).ToList
        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Movimientos de una Cuenta
    ''' </summary>
    ''' <remarks>Obtiene una lista con los movimientos de una cuenta a partir del identificador del usuario, la cuenta y de un filtro específico que indica los movimientos a recuperar.</remarks>
    Public Function getAccountTrans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal filter As FilterExpr) As List(Of AccountTransaction)
        Dim result As New List(Of AccountTransaction)
        Dim _dt1 As DataTable
        Dim _oper As New AChannel.Data.OracleOperations

        Try
            If filter.Count > 0 Then ' De acuerdo a las especificaciones, si la cantidad de registros es cero se atendera al filtro
                _dt1 = _oper.PRC_ULTIMOS_MOVIMIENTOS_CUENTA(productId, filter.Count)
                result = (From i In _dt1.AsEnumerable() Select New AccountTransaction With {.ProductId = i("NUM_CUENTA"), _
                                                                                    .TransactionDate = i("TMO_FECHCON"), _
                                                                                    .Currency = i("MONEDA"), _
                                                                                    .Description = i("DESC_CUENTA"), _
                                                                                    .CheckNum = i("TMO_REF"), _
                                                                                    .Amount = i("TMO_VAL"), _
                                                                                    .DebitOrCredit = i("DB_O_CR"), _
                                                                                    .TotalBalance = i("SALDO_CUENTA")}).ToList
            Else
                _dt1 = _oper.PRC_MOVIMIENTOS_CUENTA(customerDocType, customerDocNumber, productType, productId, filter.DateBegin, filter.DateEnd, filter.MinAmount, filter.MaxAmount, filter.TransactionType, filter.Count)
                result = (From i In _dt1.AsEnumerable() Select New AccountTransaction With {.ProductId = i("TMO_NUMCUE"), _
                                                                                    .TransactionDate = i("TMO_FECHOR"), _
                                                                                    .Currency = i("MONEDA"), _
                                                                                    .Description = i("DESCRIPCION_MVTO"), _
                                                                                    .CheckNum = i("NUMERO_CHEQUE"), _
                                                                                    .Amount = i("MON_TRA"), _
                                                                                    .DebitOrCredit = i("DB_O_CR"), _
                                                                                    .TotalBalance = i("SALDO")}).ToList
            End If

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Préstamos activos de un cliente
    ''' </summary>
    ''' <remarks>Obtiene los préstamos activos de un cliente a partir del identificador del usuario.</remarks>
    Public Function getLoans(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of Loan)
        Dim result As New List(Of Loan)
        Dim _dt1 As DataTable

        Try
            'PKG_IB_CARIBE.PRC_OBTIENE_PRESTAMOS_CLIENTES
            ' RC_PRESTAMOS = numero, NOMBRE, TIPO_PRESTAMO, MONEDA, CUOTA, FECHA, ATRASO 

            Dim _oper As New AChannel.Data.OracleOperations
            _dt1 = _oper.PRC_OBTIENE_PRESTAMOS_CLIENTES(customerDocType, customerDocNumber)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result = (From i In _dt1.AsEnumerable() Select New Loan With {.LoanType = i("PRE_TIP"), _
                                                                             .ProductId = i("PRE_CREDITO"), _
                                                                             .SocialReason = i("CLI_NOMBRE"), _
                                                                             .Currency = i("MONEDA"), _
                                                                             .MonthlyAmount = i("VALORCUOTA"), _
                                                                             .NextPaymentDate = i("TPR_FECHPROXVENC"), _
                                                                             .OverdueAmount = i("MONTO_ATRASO")}).ToList
        Catch ex As Exception
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Inversiones (certificados de depósito)
    ''' </summary>
    ''' <remarks>Obtiene las Inversiones (certificados de depósito) a partir del identificador del usuario.</remarks>
    Public Function getInvestments(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of Investment)
        Dim result As New List(Of Investment)
        Dim _dt1 As DataTable

        Try
            'PKG_IB_CARIBE.PRC_OBTIENE_CERTIFICADOS_CLIENTES
            ' RC_CERTIFICADOS =  'numero, NOMBRE, FECHA_APERTURA, MONTO, MONEDA, FECHA_EXPIRACION

            Dim _oper As New AChannel.Data.OracleOperations
            _dt1 = _oper.PRC_OBTIENE_CERTIFICADOS_CLIENTES(customerDocType, customerDocNumber)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result = (From i In _dt1.AsEnumerable() Select New Investment With {.ProductId = i("PDA_CUENTA"), _
                                                                             .SocialReason = i("CLI_NOMBRE"), _
                                                                             .Currency = i("MONEDA"), _
                                                                             .CreditLineCash = i("PDA_CAPITAL"), _
                                                                             .OpenDate = i("PDA_FECHAPER"), _
                                                                             .ExpirationDate = i("PDA_FECVEN")}).ToList
        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Social Reason (Razon Social)
    ''' </summary>
    ''' <remarks>Obtiene la Razon Social a partir del identificador del usuario.</remarks>
    Public Function getSocialReason(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As SocialReason
        Dim result As New SocialReason
        Dim _socialReason As String

        Try
            Dim _oper As New AChannel.Data.OracleOperations
            _socialReason = _oper.Prc_Obtiene_Razon_Social(customerDocType, customerDocNumber)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result.socialReason = _socialReason

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene todos los productos del cliente
    ''' </summary>
    ''' <remarks>Obtiene la lista de productos, asociadas al identificador (tipo y número de documento).</remarks>
    Public Function getProducts(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productTypes As Integer()) As List(Of Products)

        Dim result As New List(Of Products)
        Dim _dt1 As DataTable
        Dim _test As Integer() = Nothing

        Try
            'RC_CUENTAS_VISTA =numero, CUENTA, NOMBRE, MONEDA, MANCOMUNADA, ESTATUS, DISPONIBLE, BALANCE 
            Dim _oper As New AChannel.Data.OracleOperations
            _dt1 = _oper.PRC_OBTIENE_PRODUCTOS_CLIENTE(customerDocType, customerDocNumber, productTypes)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result = (From i In _dt1.AsEnumerable() Select New Products With {.ProductType = i("TIP_PROD"), _
                                                                                 .ProductId = i("CUENTA"), _
                                                                                 .TransferEnable = (i("STATUS_TRANS") = "S"), _
                                                                                 .JointAccount = (i("MANCOMUNADO") = "S"), _
                                                                                 .SocialReason = i("NOM_CLIENTE"), _
                                                                                 .Currency = i("MONEDA")}).ToList
        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene detalles de un producto
    ''' </summary>
    ''' <remarks>Obtiene el detalle de un producto, asociadas al identificador (tipo y número de documento).</remarks>
    Public Function getAccountDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String) As AccountDetail
        Dim result As New AccountDetail
        Dim _dt1 As DataTable
        Dim _oper As New AChannel.Data.OracleOperations

        Try
            'PKG_IB_CARIBE.PRC_ULTIMOS_MOVIMIENTOS_CUENTA
            ' CS_SALIDA = numero, MONEDA, FECHA, DESCRIPCION, 0344 DOCUMENTO, MONTO, ORIGEN

            _dt1 = _oper.PRC_OBTIENE_DETALLE_VISTA(customerDocType, customerDocNumber, productType, productId)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() Select New AccountDetail With {.ProductType = i("PRODUCTO"), _
                                                                                        .ProductId = i("NUM_CUE"), _
                                                                                        .Currency = i("MONEDA"), _
                                                                                        .JointAccount = (i("MANCOMUNADO") = "S"), _
                                                                                        .AvailableBal = i("DISPONIBLE"), _
                                                                                        .TotalAmount = i("SALDOTOTAL"), _
                                                                                        .seizedAmount = i("SALDOBLQ"), _
                                                                                        .pledgedAmount = i("SALGAR"), _
                                                                                        .unpaidInterests = i("INTNOPAGADO"), _
                                                                                        .receivableBal = i("SALDOCXCOB").ToString, _
                                                                                        .officerName = i("OFICIAL").ToString, _
                                                                                        .officerLastName = i("APE_OFI").ToString, _
                                                                                        .officermail = i("EMAIL_OFICIAL").ToString, _
                                                                                        .transit1 = i("TRAN_1D"), _
                                                                                        .transit2 = i("TRAN_2D"), _
                                                                                        .transit3 = i("TRAN_3D"), _
                                                                                        .transit4 = i("TRAN_4D"), _
                                                                                        .transit5 = i("TRAN_5D"), _
                                                                                        .transitN = i("V_MONTO_TRAN_MAS_5D")}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene balance de cuentas
    ''' </summary>
    ''' <remarks>Obtiene el balance de una cuenta a partir de la identificación del usuario y el producto.</remarks>
    Public Function getAccountBalance(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String) As AccountBalance
        Dim result As New AccountBalance
        Try


            Select Case productType

                Case ProductTypes.CheckingAccount, ProductTypes.SavingAccount, ProductTypes.Investment, ProductTypes.Loan
                    Dim _dt1 As New DataTable
                    Dim _oper As New AChannel.Data.OracleOperations

                    _dt1 = _oper.PRC_OBTIENE_DISPONIBLE_VISTA2(customerDocType, customerDocNumber, productType, productId)

                    Me.OperationResult = _oper.CodigoError
                    Me.OperationDescription = _oper.ErrorDescription
                    Me.OperationId = _oper.Secuencia

                    If _dt1.Rows.Count > 0 Then
                        result = (From i In _dt1.AsEnumerable() Select New AccountBalance With {.ProductType = i("productType"), _
                                                                                                .ProductId = i("productId"), _
                                                                                                .Currency = i("currency"), _
                                                                                                .AvailableBal = i("availablebal").ToString}).ToList.FirstOrDefault
                    End If


                Case ProductTypes.PesosCaribe
                    Dim _oper As New AChannel.Data.SqlOperations
                    Dim _resultDB As AChannel.Data.AL_getProductDataResult

                    _resultDB = _oper.getALoyalData(productType, productId)

                    If _resultDB IsNot Nothing Then
                        result.AvailableBal = _resultDB.AvailableBal
                        result.Currency = _resultDB.Currency
                        result.ProductId = productId
                        result.ProductType = productType
                    End If

                    Me.OperationDescription = _oper.ErrorDescription
                    Me.OperationId = _oper.Secuencia
                    Me.OperationResult = ConvertErrorCode(_oper.CodigoError)

                Case Else
                    Me.OperationResult = 39
                    Me.OperationDescription = "Tipo de Producto Invalido"
                    Me.OperationId = 999999
            End Select

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene lista de cuentas de Pesos Caribe
    ''' </summary>
    ''' <remarks>Obtiene la lista de cuentas de tipo banco caribe, asociadas al identificador (tipo y número de documento).</remarks>
    Public Function getALoyalAccounts(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String) As List(Of PesosCaribeAccount)
        Dim _result As New List(Of PesosCaribeAccount)
        Dim _dt As New List(Of Data.AL_getALoyalAccountsResult)

        Try

            Dim _oper As New AChannel.Data.SqlOperations

            _dt = _oper.getALoyalAccounts(customerDocType, customerDocNumber)

            If Not IsNothing(_dt) Then

                _result = (From i In _dt Select New PesosCaribeAccount With {.productType = i.productType,
                                                                         .productId = i.productID,
                                                                        .totalAmount = i.availableBal}).ToList
            Else
                If Not String.IsNullOrEmpty(_oper.ErrorDescription) Then
                    Throw New Exception(_oper.ErrorDescription)
                Else
                    Throw New Exception("Not Data Found")
                End If
            End If

            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia
            Me.OperationResult = ConvertErrorCode(_oper.CodigoError)

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return _result
    End Function

    ''' <summary>
    ''' Obtiene imagenes de cheques
    ''' </summary>
    ''' <remarks>Obtiene la imagen de un cheque Obverse and Reverse.</remarks>
    Public Function getImageCheck(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal checkNumber As String, ByVal checkDate As Date) As ImageCheck
        Dim result As ImageCheck = Nothing
        Dim _oper As New AChannel.Data.OracleOperations

        Try

            Dim resultDB As Hashtable = _oper.pro_returnCheck(customerDocType, customerDocNumber, productType, productId, checkNumber, checkDate)

            result = New ImageCheck With {.UrlCheckImageObverse = resultDB("OBSERVE"), _
                                          .UrlCheckImageReverse = resultDB("REVERSE")}

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            'If _dt1.Rows.Count > 0 Then
            '    result = (From i In _dt1.AsEnumerable() Select New ImageCheck With {.UrlCheckImageObverse = i("OBSERVE"), _
            '                                                                        .UrlCheckImageReverse = i("REVERSE")}).ToList.FirstOrDefault
            'End If
            'Else
            'result = New ImageCheck With {.UrlCheckImageObverse = "\\172.24.107.207\bmchome\archenvio\imgchk\10000472f.tif", _
            '                              .UrlCheckImageReverse = "\\172.24.107.207\bmchome\archenvio\imgchk\10000472r.tif"}
            'End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene la tasa de cambio y convierte un monto de una moneda dada a otra. 
    ''' </summary>
    ''' <remarks>Obtiene la tasa de cambio y convierte un monto de una moneda dada a otra.</remarks>
    Public Function getExchange(ByVal sourceCurrency As Currencies, ByVal targetCurrency As Currencies, ByVal amount As Decimal, ByVal translationDate As Date) As Exchange
        Dim result As Exchange = Nothing
        Dim _oper As New AChannel.Data.OracleOperations
        Try
            Dim resultDB As Hashtable = _oper.Prc_Obtiene_Cambio_Divisa(sourceCurrency, targetCurrency, amount, translationDate)

            result = New Exchange With {.currency = resultDB("currency"), _
                                        .exchangeRate = resultDB("exchangeRate"), _
                                        .translatedAmount = resultDB("translatedAmount")}

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene detalles de un PRESTAMO
    ''' </summary>
    ''' <remarks>Obtiene el detalle de un prestamo, asociadas al identificador (tipo y número de documento).</remarks>
    Public Function getLoanDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String) As LoanDetail
        Dim result As New LoanDetail
        Dim _dt1 As DataTable
        Dim _oper As New AChannel.Data.OracleOperations

        Try
            '           RC_OBTIENE_DETALLE_PRESTAMO (PI_ID_TP_CLTE       IN NUMBER,
            '			     PI_ID_CLIENTE       IN VARCHAR2,
            '			     PI_PRESTAMO         IN NUMBER,
            '  			     PO_COD_ERR	         OUT VARCHAR2,
            '			     PO_DESC_ERR         OUT VARCHAR2,
            '			     PO_SECUENCIA        OUT NUMBER,
            '                             RC_DETALLE_PRESTAMO OUT SYS_REFCURSOR )
            'EStructura del refcursor:
            '            pre_credito, producto, tipoproducto, pre_monto, tpr_fechproxvenc, montoatrasoprestamo, moneda,
            '            fecha_ult_pago, montoultpagoprestamo, pre_fecemi, montooriginal, tpr_saldo_capital, tpr_saldo_interes,
            '            pre_plazo, cuotaspagadasprestamo, cuenta_debitar, nombre_del_oficial, email_del_oficial,
            '            estatus, plazo_meses, tasa, saldo_cancelacion, valorcuota
            ' Te indico que para el detalle de préstamos se deben ignorar los campos:
            ' tpr_saldo_capital, tpr_saldo_interes, estatus, montooriginal, pre_plazo

            _dt1 = _oper.PRC_OBTIENE_DETALLE_PRESTAMO(customerDocType, customerDocNumber, productId)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() Select New LoanDetail With {.ProductId = productId, _
                                                                                    .Currency = i("MONEDA"), _
                                                                                    .loanType = (i("producto")), _
                                                                                    .nextPaymentDate = i("tpr_fechproxvenc"), _
                                                                                    .monthlyAmount = i("valorcuota").ToString, _
                                                                                    .overdueAmount = i("montoatrasoprestamo").ToString, _
                                                                                    .lastPaymentDate = i("fecha_ult_pago"), _
                                                                                    .lastPaymentAmount = i("montoultpagoprestamo").ToString, _
                                                                                    .disbursementDate = i("pre_fecemi"), _
                                                                                    .disbursementAmount = i("montooriginal").ToString, _
                                                                                    .cancellationBal = i("saldo_cancelacion").ToString, _
                                                                                    .interestRate = i("tasa"), _
                                                                                    .loanTerm = i("plazo_meses"), _
                                                                                    .paymentsMade = i("cuotaspagadasprestamo").ToString, _
                                                                                    .debitAccount = i("cuenta_debitar"), _
                                                                                    .officerName = i("nombre_del_oficial").ToString, _
                                                                                    .officerLastName = i("ape_ofi").ToString, _
                                                                                    .officermail = i("email_del_oficial").ToString}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene detalles de un Certificado
    ''' </summary>
    ''' <remarks>Obtiene el detalle de un Certificado, asociadas al identificador (tipo y número de documento).</remarks>
    Public Function getInvestmentDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productId As String) As InvestmentDetail
        Dim result As New InvestmentDetail
        Dim _dt1 As DataTable
        Dim _oper As New AChannel.Data.OracleOperations

        Try
            '            Procedure Prc_Obtiene_Detalle_Certificad(Pi_TipoId             varchar2,
            '                                         Pi_Identificacion     varchar2,
            '                                         Pi_Num_Certificado    number,
            '                                         Po_Cod_Respuesta_Gral out varchar2, 
            '                                         Po_Descrip_Respuesta  out varchar2,
            '                                         Po_Secuencia          out number,
            '                                         Po_Rc_Detalle_Plazos  out sys_refcursor)

            'Estructura variable cursor:
            '            num_cue, fechaper, monto_inicial, Moneda, fecven, monto_actual, Capitaliza, monto_embargado, monto_pignorado,
            '            tasa, plazo, fecrenov, Interes_pendiente, cuenta_acreditar, Oficial, ape_ofi, Email_oficial

            _dt1 = _oper.Prc_Obtiene_Detalle_Certificad(customerDocType, customerDocNumber, productId)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() Select New InvestmentDetail With {.ProductId = i("num_cue"), _
                                                                                            .Currency = i("MONEDA"), _
                                                                                            .openDate = (i("fechaper")), _
                                                                                            .creditLineCash = i("monto_inicial").ToString, _
                                                                                            .expirationDate = i("fecven"), _
                                                                                            .sheetDateBal = i("monto_actual").ToString, _
                                                                                            .paymentType = i("FORMA_PAGO"), _
                                                                                            .seizedAmount = i("monto_embargado").ToString, _
                                                                                            .pledgedAmount = i("monto_pignorado").ToString, _
                                                                                            .interestRate = i("tasa").ToString, _
                                                                                            .term = i("plazo").ToString, _
                                                                                            .lastRenovation = i("fecrenov"), _
                                                                                            .lateCharge = i("Interes_pendiente").ToString, _
                                                                                            .CreditAccount = i("cuenta_acreditar").ToString, _
                                                                                            .officerName = i("Oficial").ToString, _
                                                                                            .officerLastName = i("ape_ofi").ToString, _
                                                                                            .officermail = i("Email_oficial").ToString}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Bloqueo de Cheques
    ''' </summary>
    ''' <remarks>El servicio recibe datos de una solicitud de Bloqueo de Cheques a partir del identificador del usuario y los datos del cheque.</remarks>
    Public Function LockChecks(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal fromCheck As String, ByVal toCheck As String, ByVal lockReasonId As Short) As LockCheck
        Dim result As LockCheck = Nothing
        Dim RequestId As String = String.Empty
        Dim _oper As New AChannel.Data.OracleOperations

        Try

            'Procedure Prc_Suspende_Cheque_De_Cliente(Pi_Tipo_Id            varchar2,	
            '                             Pi_Identificacion     varchar2,
            '                             Pi_Cuenta             number,		
            '                             Pi_Num_Ck_Inicial     varchar2,
            '                             Pi_Num_Ck_Final       varchar2,
            '                             Pi_Causa_Susp_Anul    number,
            '                             Pi_Fecha_Susp_Anul    date,
            '                             Po_Cod_Respuesta_Gral out varchar2,
            '                             Po_Descrip_Respuesta  out varchar2,
            '                             Po_Secuencia          out number)                   ' Este mismo campo sera el RequestID

            RequestId = _oper.Prc_Suspende_Cheque_De_Cliente(customerDocType, customerDocNumber, productType, productId, fromCheck, toCheck, lockReasonId)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result = New LockCheck With {.requestId = RequestId}

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Estado de cuentas/tarjetas
    ''' </summary>
    ''' <remarks>Obtiene el estado de cuenta en formato PDF de una cuenta a partir del identificador del usuario, la cuenta y del estado solicitado.</remarks>
    Public Function getAccountStatementPDF(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal TypeStatus As TypeStatus, ByVal Currency As Currencies) As AccountStatementPDF
        Dim result As AccountStatementPDF = Nothing
        Dim url As String = String.Empty
        Dim _oper As New AChannel.Data.OracleOperations

        Try

            'fnc_IB_Estados (CustomerDocType   In Number, 
            '            CustomerDocNumber In Varchar2, 
            '            ProductType       In Number,
            '            ProductId         In Varchar2, 
            '            TypeStatus        In Number,
            '            Currency          In Number) Return varchar2

            url = _oper.PRC_IB_Estados(customerDocType, customerDocNumber, productType, productId, TypeStatus, Currency)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            result = New AccountStatementPDF With {.url = url}

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Obtiene detalles de un producto
    ''' </summary>
    ''' <remarks>Obtiene el detalle de un producto, asociadas al identificador (tipo y número de documento).</remarks>
    Public Function getProductData(ByVal productType As ProductTypes, ByVal productId As String, ByVal currency As Currencies) As ProductData
        Dim result As New ProductData
        Dim _dt1 As New DataTable

        Try
            Select Case productType

                Case ProductTypes.CheckingAccount, ProductTypes.SavingAccount, ProductTypes.Investment, ProductTypes.Loan
                    Dim _oper As New AChannel.Data.OracleOperations
                    _dt1 = _oper.PRC_OBTIENE_INFO_PRODUCTO(productType, productId, currency)
                    Me.OperationResult = _oper.CodigoError
                    Me.OperationDescription = _oper.ErrorDescription
                    Me.OperationId = _oper.Secuencia

                Case ProductTypes.PesosCaribe
                    Dim _oper As New AChannel.Data.SqlOperations
                    Dim _resultDB As AChannel.Data.AL_getProductDataResult

                    _resultDB = _oper.getALoyalData(productType, productId)

                    If _resultDB IsNot Nothing Then
                        result.customerName = _resultDB.CustomerName
                        result.Currency = _resultDB.Currency
                        result.ProductId = productId
                        result.ProductType = productType
                    End If

                    Me.OperationDescription = _oper.ErrorDescription
                    Me.OperationId = _oper.Secuencia
                    Me.OperationResult = ConvertErrorCode(_oper.CodigoError)

                Case ProductTypes.CreditCard, ProductTypes.ExtraLimitCard
                    Dim _oper As New AChannel.Data.DB2Operations
                    Dim _resultdb As New AChannel.Data.OperationsDB

                    _dt1 = _oper.GETPRODUCTDATA(productType, productId, currency)

                    Me.OperationResult = _resultdb.CodigoError
                    Me.OperationDescription = _resultdb.ErrorDescription
                    Me.OperationId = _oper.Secuencia

                    If _dt1.Rows.Count > 0 Then
                        _resultdb = (From i In _dt1.AsEnumerable() Select New Data.OperationsDB With {.CodigoError = i("OPERATIONRESULT"), _
                                                                                                  .ErrorDescription = i("OPERATIONDESCRIPTION")}).ToList.FirstOrDefault
                        Me.OperationResult = _resultdb.CodigoError
                        Me.OperationDescription = _resultdb.ErrorDescription
                        Me.OperationId = _oper.Secuencia
                    End If


                Case Else
                    Me.OperationResult = 56
                    Me.OperationDescription = "Tipo de Producto Invalido"
                    Me.OperationId = 999999

            End Select

            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() _
                          Select New ProductData With {.ProductType = i("ProductType"), _
                                                       .ProductId = i("ProductId"), _
                                                       .Currency = i("Currency"), _
                                                       .customerName = i("customerName")}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Transferencias entre Productos Propios o de Terceros
    ''' </summary>
    ''' <remarks>El servicio realiza una transferencia de un producto (propio) a otro (propio, de terceros mismo banco o de terceros otro banco). Los datos enviados en el servicio corresponden a la transacción a realizar.</remarks>
    Public Function Transfer(ByVal transferType As TransferTypes, ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As ProductTypes, ByVal sourceProductId As String, ByVal destinationDocType As DocumentType, ByVal destinationDocNum As String, ByVal destinationName As String, ByVal destinationBank As Short, ByVal destinationProductType As ProductTypes, ByVal destinationProductId As String, ByVal amount As Decimal, ByVal currency As Currencies, ByVal concept As String, ByVal sourceName As String, ByVal destinationBankSwift As String) As Long
        Dim result As New Long
        Dim _dt1 As New DataTable
        Dim debitDone As Boolean = False
        Dim _randomNumber As New Random()
        Dim _fisaOperationId As Integer = 0
        Dim _internalTransfer As Boolean = (sourceProductType = ProductTypes.PesosCaribe And destinationProductType = ProductTypes.PesosCaribe) Or
                                            ((sourceProductType = ProductTypes.CheckingAccount Or sourceProductType = ProductTypes.SavingAccount) And
                                              (destinationProductType = ProductTypes.CheckingAccount Or destinationProductType = ProductTypes.SavingAccount))

        Me.OperationResult = 1
        Me.OperationId = _randomNumber.Next(100000, 999999)
        Me.OperationDescription = " -- "

        Try
            Select Case transferType

                Case TransferTypes.ExtraBank
                    ' Envia requerimiento de transferencia por ACH a procedure Fisa.
                    Dim _oper As New AChannel.Data.OracleOperations
                    result = _oper.TransferAch(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)

                    Me.OperationResult = _oper.CodigoError
                    Me.OperationDescription = _oper.ErrorDescription
                    Me.OperationId = _oper.Secuencia

                Case TransferTypes.IntraBank
                    ' Determina si los productos son del mismo sistema y tratara de enviarlos a ese sistema
                    If _internalTransfer Then
                        Select Case sourceProductType
                            Case ProductTypes.PesosCaribe
                                ' Invoca procedure de Transferencia en ALoyal 
                                Dim _oper As New AChannel.Data.SqlOperations
                                result = _oper.TransferALoyal(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                Me.OperationDescription = _oper.ErrorDescription
                                Me.OperationId = _oper.Secuencia
                                Me.OperationResult = ConvertErrorCode(_oper.CodigoError)

                            Case ProductTypes.CheckingAccount, ProductTypes.SavingAccount
                                ' Invoca procedure de Transferencia en FISA
                                Dim _oper As New AChannel.Data.OracleOperations
                                result = _oper.TransferFisa(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                Me.OperationResult = _oper.CodigoError
                                Me.OperationDescription = _oper.ErrorDescription
                                Me.OperationId = _oper.Secuencia

                            Case Else
                                'Error de tipo de Producto Origen Invalido
                                Me.OperationResult = 39
                                Me.OperationDescription = "Tipo de Producto Invalido"

                        End Select
                    Else
                        'Llamara una funcion de debito en el Sistema Origen y si tuvo Exito, llamara una funcion de credito en el Sistema Destino
                        Select Case sourceProductType
                            Case ProductTypes.PesosCaribe
                                'Debito Pesos Caribe
                                Dim _oper As New AChannel.Data.SqlOperations

                                'MCordero 5 Feb 2019, Caso ACBC-2
                                'Modificar AChannel para que rechace las transacciones de pago con moneda igual a dolar
                                If AChannel.Core.Enumerations.Currencies.PesoDominicano = currency Then
                                    result = _oper.DebitoALoyal(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                    Me.OperationDescription = _oper.ErrorDescription
                                    Me.OperationId = _oper.Secuencia
                                    Me.OperationResult = ConvertErrorCode(_oper.CodigoError)
                                    debitDone = (Me.OperationResult = 0)
                                Else
                                    Me.OperationDescription = "Debe utilizar moneda igual a Pesos Dominicanos"
                                    Me.OperationId = 0
                                    Me.OperationResult = 12
                                    debitDone = False
                                End If

                            Case ProductTypes.CheckingAccount, ProductTypes.SavingAccount
                                'Debito Fisa
                                Dim _oper As New AChannel.Data.OracleOperations
                                result = _oper.DebitoFisa(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                Me.OperationResult = _oper.CodigoError
                                Me.OperationDescription = _oper.ErrorDescription
                                Me.OperationId = _oper.Secuencia
                                _fisaOperationId = result
                                debitDone = (Me.OperationResult = 0)

                                'Case ProductTypes.CreditCard, ProductTypes.ExtraLimitCard
                                '    'Debito iTrasnCard
                                '    Dim _oper As New AChannel.Data.DB2Operations
                                '    result = _oper.DebitoITransCard(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                '    Me.OperationResult = _oper.CodigoError
                                '    Me.OperationDescription = _oper.ErrorDescription
                                '    Me.OperationId = _oper.Secuencia
                                '    debitDone = (Me.OperationResult = 0)

                            Case Else
                                'Error de tipo de Producto Origen Invalido
                                Me.OperationResult = 39
                                Me.OperationDescription = "Tipo de Producto origen Invalido"

                        End Select

                        'Si el Debito fue Exitoso entonces procedera a realizar el Credito
                        If debitDone Then
                            Select Case destinationProductType
                                Case ProductTypes.PesosCaribe
                                    'Credito a Pesos Caribe
                                    Dim _oper1 As New AChannel.Data.SqlOperations
                                    result = _oper1.CreditoALoyal(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                    Me.OperationDescription = _oper1.ErrorDescription
                                    Me.OperationId = _oper1.Secuencia
                                    Me.OperationResult = ConvertErrorCode(_oper1.CodigoError)

                                Case ProductTypes.CheckingAccount, ProductTypes.SavingAccount
                                    'Credito a cuenta Fisa
                                    Dim _oper1 As New AChannel.Data.OracleOperations
                                    result = _oper1.CreditoFisa(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                    Me.OperationResult = _oper1.CodigoError
                                    Me.OperationDescription = _oper1.ErrorDescription
                                    Me.OperationId = _oper1.Secuencia

                                Case ProductTypes.CreditCard, ProductTypes.ExtraLimitCard
                                    'Credito a Tarjeta de Credito (ItransCard)
                                    Dim _oper1 As New AChannel.Data.DB2Operations
                                    Dim _resultdb As New AChannel.Data.OperationsDB

                                    _dt1 = _oper1.CreditoITransCard(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)

                                    Me.OperationResult = _resultdb.CodigoError
                                    Me.OperationDescription = _resultdb.ErrorDescription
                                    Me.OperationId = _oper1.Secuencia

                                    If _dt1.Rows.Count > 0 Then
                                        _resultdb = (From i In _dt1.AsEnumerable() Select New Data.OperationsDB With {.CodigoError = i("OPERATIONRESULT"),
                                                                                                                      .Secuencia = i("transferid"),
                                                                                                                      .ErrorDescription = i("OPERATIONDESCRIPTION")}).ToList.FirstOrDefault
                                        Me.OperationResult = _resultdb.CodigoError
                                        Me.OperationDescription = _resultdb.ErrorDescription
                                        Me.OperationId = _resultdb.Secuencia
                                    End If



                                    'Me.OperationResult = _oper1.CodigoError
                                    'Me.OperationDescription = _oper1.ErrorDescription
                                    'Me.OperationId = _oper1.Secuencia

                                    'Case ProductTypes.Loan
                                    '    'Credito a Prestamo en Fisa
                                    '    Dim _oper1 As New AChannel.Data.OracleOperations
                                    '    'result = _oper1.CreditoFisa(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                    '    Me.OperationResult = _oper1.CodigoError
                                    '    Me.OperationDescription = _oper1.ErrorDescription
                                    '    Me.OperationId = _oper1.Secuencia

                                    'Case ProductTypes.Investment
                                    '    'Credito a Inversion en Fisa
                                    '    Dim _oper1 As New AChannel.Data.OracleOperations
                                    '    'result = _oper1.CreditoFisa(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                    '    Me.OperationResult = _oper1.CodigoError
                                    '    Me.OperationDescription = _oper1.ErrorDescription
                                    '    Me.OperationId = _oper1.Secuencia

                                Case Else
                                    Me.OperationResult = 39
                                    Me.OperationDescription = "Tipo de Producto destino Invalido"

                            End Select

                            ' Si el Debito fue hecho y el Credito no tuvo exito procedera a enviar un Reverso al Sistema el Producto Origen
                            If debitDone AndAlso Not (Me.OperationResult = 0) Then
                                Select Case sourceProductType
                                    Case ProductTypes.PesosCaribe
                                        'Credito de Reverso a Pesos Caribe
                                        Dim _oper2 As New AChannel.Data.SqlOperations
                                        result = _oper2.ReversoALoyal(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)

                                        If _oper2.CodigoError <> 0 Then
                                            Me.OperationDescription += " - No se completo la operacion de Reverso! Resultado:" + ConvertErrorCode(_oper2.CodigoError.ToString) + " Secuencia:" + _oper2.Secuencia.ToString
                                        Else
                                            Me.OperationDescription += " - Se completo la operacion de Reverso! Resultado:" + ConvertErrorCode(_oper2.CodigoError.ToString) + " Secuencia:" + _oper2.Secuencia.ToString
                                        End If

                                    Case ProductTypes.CheckingAccount, ProductTypes.SavingAccount
                                        'Credito de Reverso a Fisa
                                        Dim _oper2 As New AChannel.Data.OracleOperations
                                        result = _oper2.ReversoFisa(_fisaOperationId, sourceProductId, amount, currency, concept)
                                        'Me.OperationResult = _oper2.CodigoError
                                        'Me.OperationDescription = _oper2.ErrorDescription
                                        'Me.OperationId = _oper2.Secuencia

                                        If _oper2.CodigoError <> 0 Then
                                            Me.OperationDescription += " - No se completo la operacion de Reverso! Resultado:" + _oper2.CodigoError.ToString + " Secuencia:" + _oper2.Secuencia.ToString
                                        Else
                                            Me.OperationDescription += " - Se completo la operacion de Reverso! Resultado:" + _oper2.CodigoError.ToString + " Secuencia:" + _oper2.Secuencia.ToString
                                        End If

                                        'Case ProductTypes.CreditCard, ProductTypes.ExtraLimitCard
                                        '    'Credito de Reverso a iTrasnCard
                                        '    Dim _oper2 As New AChannel.Data.DB2Operations
                                        '    'result = _oper2.CreditoITransCard(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept)
                                        '    Me.OperationResult = _oper2.CodigoError
                                        '    Me.OperationDescription = _oper2.ErrorDescription
                                        '    Me.OperationId = _oper2.Secuencia

                                    Case Else
                                        'Error de tipo de Producto Origen Invalido
                                        Me.OperationResult = 39
                                        Me.OperationDescription = "Tipo de Producto origen Invalido"

                                End Select

                            End If
                        End If
                    End If

                Case TransferTypes.ExtraBankLBTR
                    ' Envia requerimiento de transferencia por LBTR a sistema ePayit.
                    Dim _oper As New AChannel.Data.SqlOperations
                    result = _oper.TransferLBTR(customerDocType, customerDocNumber, sourceProductType, sourceProductId, destinationDocType, destinationDocNum, destinationName, destinationBank, destinationProductType, destinationProductId, amount, currency, concept, sourceName, destinationBankSwift)

                    Me.OperationResult = _oper.CodigoError
                    Me.OperationDescription = _oper.ErrorDescription
                    Me.OperationId = _oper.Secuencia

                Case Else
                    Me.OperationResult = 39
                    Me.OperationDescription = "Tipo de Transferencia Invalida"

            End Select

            'result = 0    ' set to 00 for dummy porpose
            'Me.OperationResult = 0 ' set to 00 for dummy porpose 
            'Me.OperationDescription = "Transferencia Exitosa" ' set for dummy porpose

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function DebitTransac(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As ProductTypes, ByVal sourceProductId As String, ByVal amount As Decimal, ByVal currency As Currencies, ByVal concept As String, ByVal RubroCode As Integer) As Long
        Dim result As New Long
        Dim _randomNumber As New Random()

        Me.OperationResult = 1
        Me.OperationId = _randomNumber.Next(100000, 999999)
        Me.OperationDescription = " -- "

        Try
            Dim _oper As New AChannel.Data.OracleOperations
            result = _oper.DebitoFisa(customerDocType, customerDocNumber, sourceProductType, sourceProductId, 1, "", "", 1, 1, 1, amount, currency, concept, RubroCode)
            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function MassTransfer(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal fileName As String) As MassTransferOutput
        Dim result As New MassTransferOutput
        Dim _randomNumber As New Random()

        Me.OperationResult = 1
        Me.OperationId = _randomNumber.Next(100000, 999999)
        Me.OperationDescription = Nothing

        Try
            ' Envia requerimiento de transferencia a procedure Fisa.
            Dim _oper As New AChannel.Data.OracleOperations
            Dim resultDB As Hashtable = _oper.MassTransfer(customerDocType, customerDocNumber, fileName)

            result = New MassTransferOutput With {.transferId = resultDB("po_transferid"),
                                                  .totalAmount = resultDB("po_totalamount"),
                                                  .movmentCount = resultDB("po_movmentcount"),
                                                  .sourceProductId = resultDB("po_sourceproductid"),
                                                  .sourceProductType = resultDB("po_sourceproducttype")}

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function
    Public Function getTransferCommission(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal productType As ProductTypes, ByVal productId As String, ByVal transfervia As TransferTypes) As TransferCommission
        Dim result As New TransferCommission
        Dim _randomNumber As New Random()

        Me.OperationResult = 1
        Me.OperationId = _randomNumber.Next(100000, 999999)
        Me.OperationDescription = Nothing

        '        Procedure Prc_ComisionTransaccionRedes(PI_TipoID     In Varchar2,
        '                                       PI_IDCliente  In Varchar2,
        '                                       PI_TipoCuenta In Varchar2,
        '                                       PI_NoCuenta   In Varchar2,
        '                                       PI_RedTransaccion In Varchar2,
        '                                       PO_TipoCuenta Out Varchar2,
        '                                       PO_NoCuenta   Out Varchar2,
        '                                       PO_Comision   Out Number,
        '                                       PO_Respuesta  Out Integer,
        '                                       PO_DescRespuesta Out Varchar2)


        'Los códigos de respuesta hasta ahora devueltos son

        '01-CUENTA NO PERTENECE AL CLIENTE
        '00-CONSULTA EXITOSA


        Try
            ' Envia requerimiento de transferencia a procedure Fisa.
            Dim _oper As New AChannel.Data.OracleOperations
            Dim resultDB As Hashtable = _oper.getTransferCommission(customerDocType, customerDocNumber, productType, productId, transfervia)

            result = New TransferCommission With {.productType = resultDB("PO_TipoCuenta"),
                                                  .productId = resultDB("PO_NoCuenta"),
                                                  .commission = resultDB("PO_Comision"),
                                                  .OperationResult = resultDB("PO_Respuesta"),
                                                  .OperationDescription = resultDB("PO_DescRespuesta")}

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function doMassTransfer(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal transferId As Long) As MassTransferResult
        Dim result As New MassTransferResult
        Dim _randomNumber As New Random()

        Me.OperationResult = 1
        Me.OperationId = _randomNumber.Next(100000, 999999)
        Me.OperationDescription = Nothing

        Try
            ' Envia requerimiento de transferencia a procedure Fisa.
            Dim _oper As New AChannel.Data.OracleOperations
            Dim resultDB As Hashtable = _oper.doMassTransfer(customerDocType, customerDocNumber, transferId)

            result = New MassTransferResult With {.transferId = resultDB("po_transferid"), _
                                                  .statusId = resultDB("po_statusid")}

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function GetMassTransferDetail(ByVal customerDocType As DocumentType, ByVal customerDocNumber As String, ByVal transferId As Long) As List(Of TransferDetail)
        Dim result As New List(Of TransferDetail)
        Dim _dt1 As DataTable
        Dim _randomNumber As New Random()

        Me.OperationResult = 1
        Me.OperationId = _randomNumber.Next(100000, 999999)
        Me.OperationDescription = Nothing

        Try
            ' Envia requerimiento de transferencia a procedure Fisa.
            Dim _oper As New AChannel.Data.OracleOperations
            _dt1 = _oper.getMassTransferDetail(customerDocType, customerDocNumber, transferId)

            result = (From i In _dt1.AsEnumerable() Select New TransferDetail With {.amount = i("amount"), _
                                                                                 .concept = i("concept"), _
                                                                                 .Currency = i("Currency"), _
                                                                                 .destinationBank = i("destinationBank"), _
                                                                                 .destinationDocNum = i("destinationDocNum"), _
                                                                                 .destinationDocType = i("destinationDocType"), _
                                                                                 .destinationName = i("destinationName"), _
                                                                                 .destinationProductId = i("destinationProductId"), _
                                                                                 .destinationProductType = i("destinationProductType"), _
                                                                                 .statusId = i("statusId")}).ToList

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function ConvertErrorCode(ByVal _error As String) As String
        'convert OperationDescription tu nonnull value
        If IsDBNull(Me.OperationDescription) Then
            Me.OperationDescription = " -- "
        End If

        Select Case _error
            Case 0
                Return 0
            Case 12
                Return 30
            Case 51
                Return 30
            Case 56
                Return 32
            Case 51
                Return 33
            Case 57
                Return 34
            Case 61
                Return 34
            Case 62
                Return 35
            Case 63
                Return 36
            Case Else
                Return 1
        End Select

    End Function

    ''' <summary>
    ''' Cambia el estado de distribucion de un producto
    ''' </summary>
    ''' <remarks>Cambia el estado de distribucion de un producto.</remarks>
    Public Function ChangeDistributionStatus(ByVal productType As ProductTypes, ByVal productId As String, _
                                             ByVal RequestId As String, ByVal Status As Integer, ByVal Fecha As String, ByVal Hora As String) As DistributionStatus
        Dim result As New DistributionStatus
        Dim _dt1 As New DataTable

        Try
            Dim _oper As New AChannel.Data.DB2Operations
            Dim _resultdb As New AChannel.Data.OperationsDB

            _dt1 = _oper.ChangeDistributionStatus(productType, productId, RequestId, Status, Fecha, Hora)

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

            'result = New DistributionStatus With {.ProductType = productType, .ProductId = productId, _
            '                                        .RequestId = RequestId, .Status = Status, _
            '                                        .Fecha = Fecha, .Hora = Hora, _
            '                                        .OperationResult = "0", .OperationDescription = "OK"}
            If _dt1.Rows.Count > 0 Then
                result = (From i In _dt1.AsEnumerable() Select New DistributionStatus With {.ProductType = productType, _
                                                                                            .ProductId = i("ProductId"), _
                                                                                            .RequestId = i("RequestId"), _
                                                                                            .Status = i("Status"), _
                                                                                            .Fecha = i("Fecha"), _
                                                                                            .Hora = i("Hora"), _
                                                                                            .OperationResult = i("OperationResult"), _
                                                                                            .OperationDescription = i("OperationDescription")}).ToList.FirstOrDefault
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function
    ''' <summary>
    ''' Obtiene datos de un cliente a partir de la tarjeta de debito.. 
    ''' </summary>
    ''' <remarks>Obtiene datos de un cliente a partir de la tarjeta de debito.</remarks>
    Public Function getCustomerData(ByVal productid As String, ByVal requestId As Integer) As Customer
        Dim result As Customer = Nothing
        Dim _oper As New AChannel.Data.OracleOperations
        Try
            Dim resultDB As Hashtable = _oper.PRC_OBTIENE_DATOS_CLIENTE(productid, requestId)

            'Public Property CustomerCode() As String
            'Public Property customerDocType() As String
            'Public Property customerDocNumber() As String
            'Public Property CustomerName() As String
            'Public Property CustomerLastName() As String
            'Public Property CustomerFullName() As String
            'Public Property PlasticName() As String
            'Public Property BirthDate() As String
            'Public Property Estatus() As String
            'Public Property Sex() As String
            'Public Property OfficePhone() As String
            'Public Property CellPhone() As String
            'Public Property HomePhone() As String
            'Public Property email() As String

            '_cmd.Parameters.Add("p_codigo_cliente", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_tipo_identificacion", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_identificacion", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_nombre_cliente", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_apellido_cliente", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_nombre_completo", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_nombre_plastico", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_fecha_nacimiento", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_estado_civil", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_sexo", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_tel_oficina", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_tel_celular", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_tel_casa", OracleDbType.Char).Direction = ParameterDirection.Output
            '_cmd.Parameters.Add("p_email", OracleDbType.Char).Direction = ParameterDirection.Output

            result = New Customer With {.CustomerCode = resultDB("p_codigo_cliente"), _
                                        .customerDocType = resultDB("p_tipo_identificacion"), _
                                        .customerDocNumber = resultDB("p_identificacion"), _
                                        .CustomerName = resultDB("p_nombre_cliente"), _
                                        .CustomerLastName = resultDB("p_apellido_cliente"), _
                                        .CustomerFullName = resultDB("p_nombre_completo"), _
                                        .PlasticName = resultDB("p_nombre_plastico"), _
                                        .BirthDate = resultDB("p_fecha_nacimiento"), _
                                        .Estatus = resultDB("p_estado_civil"), _
                                        .Sex = resultDB("p_sexo"), _
                                        .OfficePhone = resultDB("p_tel_oficina"), _
                                        .CellPhone = resultDB("p_tel_celular"), _
                                        .HomePhone = resultDB("p_tel_casa"), _
                                        .email = resultDB("p_email")}

            Me.OperationResult = _oper.CodigoError
            Me.OperationDescription = _oper.ErrorDescription
            Me.OperationId = _oper.Secuencia

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function validateCreditCardCashAdvance(sourceProductId As String, sourceProductType As ProductTypes, concept As String, amount As Decimal, currency As Currencies, customerDocType As DocumentType, customerDocNumber As String, via As ViaType) As CashAdvanceValidation
        Dim result As New CashAdvanceValidation
        result.amount = amount
        result.concept = concept
        result.sourceProductId = sourceProductId
        result.sourceProductType = sourceProductType

        Try
            Dim _oper As New AChannel.Data.DB2Operations
            Dim _dt1 As DataTable = _oper.VCASH_ADVC(via, sourceProductId, sourceProductType, concept, amount)

            'Si el Datatable no tiene filas, entonces hubo un error ejecutando el SP
            If _dt1.Rows.Count = 0 Then
                Me.OperationResult = _oper.CodigoError
                Me.OperationDescription = _oper.ErrorDescription
                Return result
            End If

            Me.OperationResult = _dt1.Rows(0)("CODIGO_RESPUESTA")
            Me.OperationDescription = _dt1.Rows(0)("DESCRIPCION_RESPUESTA")
            Me.OperationId = _dt1.Rows(0)("REFERENCE_ID")

            'Me.OperationResult = _oper.CodigoError
            'Me.OperationDescription = _oper.ErrorDescription
            'Me.OperationId = _oper.Secuencia

            'Si es diferente de cero, entonces la validación no fue exitosa
            If OperationResult.ToString <> "0" Then
                Return result
            End If

            If sourceProductType = ProductTypes.ExtraLimitCard Then
                result.rate = _dt1.Rows(0)("Tasa_Interes")
                result.monthlyPayment = _dt1.Rows(0)("Valor_Cuota")
                'El term del datatable es el número de meses/cuotas
                Dim termMonths = _dt1.Rows(0)("Plazo")
                result.term = Now.AddMonths(termMonths)
            End If

            If sourceProductType = ProductTypes.CreditCard Then
                result.commission = _dt1.Rows(0)("Comision")
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

    Public Function storeCreditCardCashAdvance(sourceProductId As String, sourceProductType As Integer, destinationProductId As String, destinationProductType As String, concept As String, amount As Decimal, currency As Currencies, via As ViaType, customerDocType As Integer, customerDocId As String) As CashAdvanceStore
        Dim result As New CashAdvanceStore
        result.amount = amount
        result.concept = concept
        result.destinationProductId = destinationProductId
        result.destinationProductType = destinationProductType
        result.sourceProductId = sourceProductId
        result.sourceproductType = sourceProductType
        Try
            Dim _operDB2 As New AChannel.Data.DB2Operations
            Dim _dt1 As DataTable = _operDB2.SCASH_ADVC(via, sourceProductId, sourceProductType, concept, amount)

            'Si el Datatable no tiene filas, entonces hubo un error ejecutando el SP
            If _dt1.Rows.Count = 0 Then
                Me.OperationResult = _operDB2.CodigoError
                Me.OperationDescription = _operDB2.ErrorDescription
                Me.OperationId = _operDB2.Secuencia
                Return result
            End If

            result.transactionId = _dt1.Rows(0)("TRANSFERID")
            Me.OperationResult = _dt1.Rows(0)("OPERATION_RESULT").ToString
            Me.OperationDescription = _dt1.Rows(0)("OPERATIONDESCRIPTION").ToString
            Me.OperationId = _operDB2.Secuencia

            'Si es diferente de cero entonces hubo un error
            If OperationResult.ToString <> "0" Then
                Return result
            End If

            'La tarjeta fue debitada
            'Invoco creditoFisa para creditar la cuenta
            Dim _operOracle As New AChannel.Data.OracleOperations
            Dim longResult = _operOracle.CreditoFisaCashAdvance(customerDocType, customerDocId, destinationProductType, destinationProductId, concept, amount, currency)

            Me.OperationResult = _operOracle.CodigoError
            Me.OperationDescription = _operOracle.ErrorDescription
            Me.OperationId = _operOracle.Secuencia

            If OperationResult <> 0 Then
                'Hubo un Error, reverso el avance de efectivo
                Dim _operDB3 As New AChannel.Data.DB2Operations
                Dim _dt2 As DataTable = _operDB3.RCASH_ADVC(via, sourceProductId, sourceProductType, amount, result.transactionId)
                If _dt2.Rows.Count = 0 Then
                    Me.OperationResult = _operDB2.CodigoError
                    Me.OperationDescription = _operDB2.ErrorDescription
                    Return result
                End If

                Me.OperationResult = _dt2.Rows(0)("CODIGO_RESPUESTA")
                Me.OperationDescription = _dt2.Rows(0)("DESCRIPCION_RESPUESTA")
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

#Region "ALoyalProcess"

    ''' <summary>
    ''' Datos de Producto Pesos Caribe 
    ''' </summary>
    ''' <remarks>Obtiene Datos de Producto de Pesos Caribe a partir del producto dado.</remarks>
    Public Function getALoyalBalance(ByVal productId As String) As Double
        Dim resultDB As New AChannel.Data.AL_getProductDataResult
        Dim result As Double = 0
        Try
            Dim _oper As New AChannel.Data.SqlOperations
            resultDB = _oper.getALoyalData(ProductTypes.PesosCaribe, productId)

            If resultDB IsNot Nothing Then
                result = resultDB.AvailableBal
            End If

        Catch ex As Exception
            Logger.Error($"Exception:{ex.Message}  || StackTrace: {If(Not String.IsNullOrEmpty(ex.StackTrace), ex.StackTrace, "Nothing")} ")
            Me.OperationResult = 1
            Me.OperationDescription = ex.Message
        End Try

        Return result
    End Function

#End Region


End Class

