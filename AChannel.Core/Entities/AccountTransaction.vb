﻿Imports AChannel.Core.Enumerations

Public Class AccountTransaction


    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    <HashInclude(0)> _
    Public Property ProductId() As String


    ''' <summary>
    ''' Identificador Unico de Transaccion
    ''' </summary>
    <HashInclude(1)> _
    Public Property TransactionId() As Long


    ''' <summary>
    ''' Fecha de la Transaccion
    ''' </summary>
    Public Property TransactionDate() As Date

    ''' <summary>
    ''' Descripcion de la Transaccion
    ''' </summary>
    Public Property Description() As String


    ''' <summary>
    ''' Numero de Cheque (Cuenta Corriente)
    ''' </summary>
    Public Property CheckNum() As String

    ''' <summary>
    ''' Monto de la Transaccion
    ''' </summary>
    <HashInclude(2)> _
    Public Property Amount() As Decimal


    ''' <summary>
    ''' Si es Debito o Credito
    ''' </summary>
    Public Property DebitOrCredit() As Short


    ''' <summary>
    ''' Moneda
    ''' </summary>
    Public Property Currency() As Currencies


    ''' <summary>
    ''' Balance Total
    ''' </summary>
    Public Property TotalBalance() As Decimal

End Class
