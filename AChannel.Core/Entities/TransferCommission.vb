﻿Imports AChannel.Core.Enumerations

Public Class TransferCommission

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property productType As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    Public Property productId As String

    ''' <summary>
    ''' commission
    ''' </summary>
    Public Property commission As Decimal

    ''' <summary>
    ''' Resultado de la Operacion
    ''' </summary>
    Public Property OperationResult As Integer

    ''' <summary>
    ''' Descripcion del Resultado
    ''' </summary>
    Public Property OperationDescription As String


End Class
