﻿Imports AChannel.Core.Enumerations

Public Class CreditCardDetail
    ''' <summary>
    ''' Tipo Producto	
    ''' </summary>
    ''' <remarks>Ver catálogo ProductType. Este servicio debe devolver únicamente:</remarks>
    Public Property productType As ProductTypes

    ''' <summary>
    ''' IDProducto
    ''' </summary>
    ''' <remarks>Número de Tarjeta</remarks>
    Public Property productId As String

    ''' <summary>
    ''' Moneda	
    ''' </summary>
    ''' <remarks>Ver Catálogo Currency</remarks>
    Public Property currency As Currencies

    ''' <summary>
    ''' Balance al corte	
    ''' </summary>
    ''' <remarks>Separador de miles es la coma y el de decimales es el punto. (ej.: 21,500.70)</remarks>
    Public Property closingBal As Decimal

    ''' <summary>
    ''' Límite de crédito	
    ''' </summary>
    ''' <remarks>Límite de crédito</remarks>
    Public Property creditLineCash As Decimal

    ''' <summary>
    ''' Fecha límite de pago	
    ''' </summary>
    ''' <remarks>Formato día/mes/año con el mes con tres letras (ej.: 16/mar/2010)</remarks>
    Public Property paymentDeadline As Date

    ''' <summary>
    ''' Pago mínimo	
    ''' </summary>
    ''' <remarks>Pago mínimo</remarks>
    Public Property minPayment As Decimal

    ''' <summary>
    ''' Límite disponible	
    ''' </summary>
    ''' <remarks>(14,2)	Separador de miles es la coma y el de decimales es el punto. (ej.: 21,500.70)</remarks>
    Public Property availableLimit As Decimal

    ''' <summary>
    ''' Fecha de corte
    ''' </summary>
    ''' <remarks>Formato día/mes/año con el mes con tres letras (ej.: 16/mar/2010)</remarks>
    Public Property closingDate As Date

    ''' <summary>
    ''' Fecha de vencimiento
    ''' </summary>
    ''' <remarks>Formato día/mes/año con el mes con tres letras (ej.: 16/mar/2010)</remarks>
    Public Property expirationDate As Date

    ''' <summary>
    ''' Fecha del último pago
    ''' </summary>
    ''' <remarks>Formato día/mes/año con el mes con tres letras (ej.: 16/mar/2010)</remarks>
    Public Property lastPaymentDate As Date

    ''' <summary>
    ''' Monto del último pago
    ''' </summary>
    ''' <remarks>Monto del último pago</remarks>
    Public Property lastPaymentAmount As Decimal

    ''' <summary>
    ''' Monto en Atraso	
    ''' </summary>
    ''' <remarks>Monto en atraso de la TC</remarks>
    Public Property overdueAmount As Decimal

    ''' <summary>
    ''' Balance contado	
    ''' </summary>
    ''' <remarks>Balance contado</remarks>
    Public Property cashBal As Decimal

    ''' <summary>
    ''' Balance a la fecha
    ''' </summary>
    ''' <remarks>Balance a la fecha</remarks>
    Public Property sheetDateBal As Decimal

    ''' <summary>
    ''' Cuotas vencidas
    ''' </summary>
    ''' <remarks>Cuotas vencidas</remarks>
    Public Property overdueFees As Integer

    ''' <summary>
    ''' Pesos Caribe
    ''' </summary>
    ''' <remarks>Pesos Caribe</remarks>
    Public Property pesosCaribeAmount As Decimal

End Class
