﻿Imports AChannel.Core.Enumerations

Public Class AdditionalCreditCard
     ''' <summary>
    ''' IDProducto
    ''' </summary>
    ''' <remarks>Número de Tarjeta</remarks>
    Public Property productId As String

    ''' <summary>
    ''' Número Tarjeta	
    ''' </summary>
    ''' <remarks>Número Tarjeta</remarks>
    Public Property additionalCardNum As String

    ''' <summary>
    ''' Nombre	
    ''' </summary>
    ''' <remarks>Nombre</remarks>
    Public Property additionalCardHolder As String

End Class
