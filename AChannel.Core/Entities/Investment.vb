﻿
Public Class Investment

    ''' <summary>
    ''' Numero de Inversion
    ''' </summary>
    <HashInclude(0)> _
    Public Property ProductId() As String

    Public Property SocialReason() As String

    Public Property OpenDate() As Date

    <HashInclude(1)> _
    Public Property CreditLineCash() As Decimal

    Public Property Currency() As Enumerations.Currencies

    Public Property ExpirationDate() As Date


End Class
