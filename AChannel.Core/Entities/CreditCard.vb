﻿Imports AChannel.Core.Enumerations

Public Class CreditCard
    ''' <summary>
    ''' Tipo Producto	
    ''' </summary>
    ''' <remarks>Ver catálogo ProductType. Este servicio debe devolver únicamente:</remarks>
    Public Property productType As ProductTypes

    ''' <summary>
    ''' IDProducto
    ''' </summary>
    ''' <remarks>Número de Tarjeta</remarks>
    <HashInclude(0)> _
    Public Property productId As String

    ''' <summary>
    ''' Tipo Tarjeta	
    ''' </summary>
    ''' <remarks>Ver Catálogo CardBrand</remarks>
    Public Property cardBrand As CardBrand

    ''' <summary>
    ''' Moneda	
    ''' </summary>
    ''' <remarks>Ver Catálogo Currency</remarks>
    Public Property currency As Currencies

    ''' <summary>
    ''' Balance al corte	
    ''' </summary>
    ''' <remarks>Separador de miles es la coma y el de decimales es el punto. (ej.: 21,500.70)</remarks>
    Public Property closingBal As Decimal

    ''' <summary>
    ''' Fecha límite de pago	
    ''' </summary>
    ''' <remarks>Formato día/mes/año con el mes con tres letras (ej.: 16/mar/2010)</remarks>
    Public Property paymentDeadline As Date

    ''' <summary>
    ''' Límite disponible	
    ''' </summary>
    ''' <remarks>(14,2)	Separador de miles es la coma y el de decimales es el punto. (ej.: 21,500.70)</remarks>
    <HashInclude(1)> _
    Public Property availableLimit As Decimal

    ''' <summary>
    ''' Monto en Atraso	
    ''' </summary>
    ''' <remarks>Monto en atraso de la TC</remarks>
    Public Property overdueAmount As Decimal

End Class
