﻿Imports AChannel.Core.Enumerations

Public Class TransferDetail

    ''' <summary>
    ''' Tipo de Documento
    ''' </summary>
    Public Property destinationDocType As DocumentType

    ''' <summary>
    ''' numero de documento
    ''' </summary>
    Public Property destinationDocNum As String

    ''' <summary>
    ''' destination Name
    ''' </summary>
    Public Property destinationName As String

    ''' <summary>
    ''' Numero de Banco
    ''' </summary>
    Public Property destinationBank As Short

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property destinationProductType As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    Public Property destinationProductId As String

    ''' <summary>
    ''' amount
    ''' </summary>
    Public Property amount As Decimal

    ''' <summary>
    ''' Moneda
    ''' </summary>
    Public Property Currency As Currencies

    ''' <summary>
    ''' Concepto
    ''' </summary>
    ''' <remarks>concepto</remarks>
    Public Property concept As String

    ''' <summary>
    ''' status
    ''' </summary>
    ''' <remarks>status</remarks>
    Public Property statusId As Integer

End Class
