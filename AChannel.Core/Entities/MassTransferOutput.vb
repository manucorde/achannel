﻿Imports AChannel.Core.Enumerations

Public Class MassTransferOutput

    ''' <summary>
    ''' transferId
    ''' </summary>
    ''' <value>transferId</value>
    Public Property transferId As Long

    ''' <summary>
    ''' Saldo Total
    ''' </summary>
    Public Property totalAmount As Decimal

    ''' <summary>
    ''' Cantidad de Movimientos
    ''' </summary>
    Public Property movmentCount As Integer

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    ''' <remarks></remarks>
    Public Property sourceProductType As ProductTypes

    ''' <summary>
    ''' producto origen
    ''' </summary>
    Public Property sourceProductId As String

End Class
