﻿Imports AChannel.Core.Enumerations

Public Class Loan


    <HashInclude(0)> _
    Public Property ProductId() As String

    Public Property SocialReason() As String

    Public Property LoanType() As LoanType

    Public Property Currency() As Currencies

    <HashInclude(1)> _
    Public Property MonthlyAmount() As Decimal

    Public Property NextPaymentDate() As Date

    <HashInclude(2)> _
    Public Property OverdueAmount() As Decimal

End Class
