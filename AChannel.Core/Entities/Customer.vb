﻿Imports AChannel.Core.Enumerations
Imports AChannel.Support

Public Class Customer


    ''' <summary>
    ''' Codigo Cliente
    ''' </summary>
    Public Property CustomerCode() As String

    ''' <summary>
    ''' Tipo de Identificacion
    ''' </summary>
    Public Property customerDocType() As String

    ''' <summary>
    ''' Numero de Identificacion
    ''' </summary>
    Public Property customerDocNumber() As String

    ''' <summary>
    ''' MoNombre
    ''' </summary>
    Public Property CustomerName() As String

    ''' <summary>
    ''' Apellidos
    ''' </summary>
    ''' <remarks></remarks>
    Public Property CustomerLastName() As String

    ''' <summary>
    ''' Nombre completo
    ''' </summary>
    ''' <remarks></remarks>
    Public Property CustomerFullName() As String

    ''' <summary>
    ''' Nombre Plastico
    ''' </summary>
    Public Property PlasticName() As String

    ''' <summary>
    ''' Fecha Nacimiento
    ''' </summary>
    Public Property BirthDate() As Date

    ''' <summary>
    ''' estado Civil
    ''' </summary>
    Public Property Estatus() As String

    ''' <summary>
    ''' Sexo
    ''' </summary>
    Public Property Sex() As String

    ''' <summary>
    ''' TElefono Oficina
    ''' </summary>
    Public Property OfficePhone() As String

    ''' <summary>
    ''' telefono celular
    ''' </summary>
    Public Property CellPhone() As String

    ''' <summary>
    ''' telefono casa
    ''' </summary>
    Public Property HomePhone() As String

    ''' <summary>
    ''' email
    ''' </summary>
    Public Property email() As String


End Class
