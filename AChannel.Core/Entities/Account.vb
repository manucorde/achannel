﻿Imports AChannel.Core.Enumerations

Public Class Account


    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property ProductType() As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    <HashInclude(0)> _
    Public Property ProductId() As String

    ''' <summary>
    ''' Razon Social de la Empresa
    ''' </summary>
    Public Property SocialReason() As String

    ''' <summary>
    ''' Moneda
    ''' </summary>
    Public Property Currency() As Currencies

    ''' <summary>
    ''' Si es cuenta Mancomunada
    ''' </summary>
    ''' <remarks>Si la cuenta es mancomunada devuelve true, de lo contrario false</remarks>
    Public Property JointAccount() As Boolean

    ''' <summary>
    ''' Habilitada para Transferencias
    ''' </summary>
    ''' <remarks>Si la cuenta está habilitada para transferencias devuelve true, de lo contrario false.</remarks>
    Public Property TransferEnable() As Boolean

    ''' <summary>
    ''' Balance Disponible
    ''' </summary>
    <HashInclude(1)> _
    Public Property AvailableBal() As Decimal

    ''' <summary>
    ''' Saldo Total
    ''' </summary>
    <HashInclude(2)> _
    Public Property TotalAmount() As Decimal


    ''' <summary>
    ''' Identificador de cuenta regional
    ''' </summary>
    Public Property regionalAccountID() As String


End Class
