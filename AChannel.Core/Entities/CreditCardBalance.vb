﻿Imports AChannel.Core.Enumerations

Public Class CreditCardBalance
    ''' <summary>
    ''' Tipo Producto	
    ''' </summary>
    ''' <remarks>Ver catálogo ProductType. Este servicio debe devolver únicamente:</remarks>
    Public Property productType As ProductTypes

    ''' <summary>
    ''' IDProducto
    ''' </summary>
    ''' <remarks>Número de Tarjeta</remarks>
    Public Property productId As String

    ''' <summary>
    ''' Moneda	
    ''' </summary>
    ''' <remarks>Ver Catálogo Currency</remarks>
    Public Property currency As Currencies

    ''' <summary>
    ''' Balance al corte	
    ''' </summary>
    ''' <remarks>Separador de miles es la coma y el de decimales es el punto. (ej.: 21,500.70)</remarks>
    Public Property closingBal As Decimal

    ''' <summary>
    ''' Pago Minimo
    ''' </summary>
    ''' <remarks>Pago Minimo</remarks>
    Public Property minPayment As Decimal

    ''' <summary>
    ''' Disponible
    ''' </summary>
    ''' <remarks>Disponible</remarks>
    'Public Property Available As Decimal

End Class
