﻿Imports AChannel.Core.Enumerations

Public Class Products


    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property ProductType() As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    Public Property ProductId() As String

    ''' <summary>
    ''' Si es cuenta Mancomunada
    ''' </summary>
    ''' <remarks>Si la cuenta es mancomunada devuelve true, de lo contrario false</remarks>
    Public Property JointAccount() As Boolean

    ''' <summary>
    ''' Habilitada para Transferencias
    ''' </summary>
    ''' <remarks>Si la cuenta está habilitada para transferencias devuelve true, de lo contrario false.</remarks>
    Public Property TransferEnable() As Boolean

    ''' <summary>
    ''' Razon Social de la Empresa
    ''' </summary>
    Public Property SocialReason() As String

    ''' <summary>
    ''' Moneda
    ''' </summary>
    Public Property Currency() As Currencies

End Class
