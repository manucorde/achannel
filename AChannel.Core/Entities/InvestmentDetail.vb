﻿Imports AChannel.Core.Enumerations

Public Class InvestmentDetail

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    ''' <value>_productId</value>
    Public Property ProductId As String

    ''' <summary>
    ''' Fecha de Apertura
    ''' </summary>
    ''' <value>_openDate</value>
    Public Property openDate As Date

    ''' <summary>
    ''' Monto de apertura
    ''' </summary>
    ''' <remarks>Separador de miles es la coma y el de decimales es el punto.</remarks>
    Public Property creditLineCash As Decimal

    ''' <summary>
    ''' Moneda de la Transaccion
    ''' </summary>
    Public Property Currency As Currencies

    ''' <summary>
    ''' Fecha de vencimiento
    ''' </summary>
    Public Property expirationDate As Date

    ''' <summary>
    ''' Saldo a la fecha
    ''' </summary>
    ''' <remarks>Saldo a la fecha</remarks>
    Public Property sheetDateBal As Decimal

    ''' <summary>
    ''' Forma de pago
    ''' </summary>
    Public Property paymentType As PaymentType

    ''' <summary>
    ''' Monto embargado
    ''' </summary>
    Public Property seizedAmount As Decimal

    ''' <summary>
    ''' Monto pignorado
    ''' </summary>
    Public Property pledgedAmount As Decimal

    ''' <summary>
    ''' Tasa
    ''' </summary>
    Public Property interestRate As Decimal

    ''' <summary>
    ''' Plazo
    ''' </summary>
    Public Property term As Short

    ''' <summary>
    ''' Última renovación
    ''' </summary>
    Public Property lastRenovation As Date

    ''' <summary>
    ''' Intereses pendientes
    ''' </summary>
    Public Property lateCharge As Decimal

    ''' <summary>
    ''' Cuenta a Acreditar
    ''' </summary>
    Public Property CreditAccount As String

    ''' <summary>
    ''' Nombre del Oficial
    ''' </summary>
    Public Property officerName As String

    ''' <summary>
    ''' Apellidos del Oficial
    ''' </summary>
    Public Property officerLastName As String

    ''' <summary>
    ''' email del Oficial
    ''' </summary>
    Public Property officermail As String

End Class
