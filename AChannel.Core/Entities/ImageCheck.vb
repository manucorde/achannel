﻿Imports AChannel.Core.Enumerations

Public Class ImageCheck

    ''' <summary>
    ''' Ruta de Imagen del Anverso del Cheque
    ''' </summary>
    ''' <value>UrlCheckImageObverse</value>
    Public Property UrlCheckImageObverse As String

    ''' <summary>
    ''' Ruta de Imagen del Reverso del Cheque
    ''' </summary>
    ''' <value>UrlCheckImageReverse</value>
    Public Property UrlCheckImageReverse As String


End Class
