﻿Imports AChannel.Core.Enumerations

Public Class AccountBalance

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property ProductType() As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    Public Property ProductId() As String

    ''' <summary>
    ''' Moneda
    ''' </summary>
    Public Property Currency() As Currencies

    ''' <summary>
    ''' Balance Disponible
    ''' </summary>
    Public Property AvailableBal() As Decimal


End Class
