﻿Imports AChannel.Core.Enumerations

Public Class ExtraLimitTransaction

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    ''' <value>_productId</value>
     Public Property ProductId As String

    ''' <summary>
    ''' ID de la Transaccion
    ''' </summary>
    Public Property TransactionId As Long

    ''' <summary>
    ''' Fecha de la Transaccion
    ''' </summary>
    ''' <remarks>El formato debe ser día/mes/año, con el mes con tres letras (ej: 16/mar/10)</remarks>
    Public Property TransactionDate As Date

    ''' <summary>
    ''' Descripción del movimiento
    ''' </summary>
    Public Property Description As String

    ''' <summary>
    ''' Monto de la Transaccion
    ''' </summary>
    Public Property Amount As Decimal

    ''' <summary>
    ''' Debito o Credito
    ''' </summary>
    ''' <remarks>
    ''' 0 debito (-) 1 crédito (+) 
    ''' Se utiliza para mostrar amount
    ''' </remarks>
    ''' <value>0</value>
    Public Property DebitOrCredit As Short

    ''' <summary>
    ''' Moneda de la Transaccion
    ''' </summary>
    Public Property Currency As Currencies

    ''' <summary>
    ''' Balance Actual
    ''' </summary>
    Public Property cashBal As Decimal

    ''' <summary>
    ''' Plazo
    ''' </summary>
    Public Property term As Short

    ''' <summary>
    ''' Cuotas Pendientes
    ''' </summary>
    Public Property outstandingQuota As Short

End Class
