﻿Imports AChannel.Core.Enumerations

Public Class LoanDetail

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    ''' <value>_productId</value>
    Public Property ProductId As String

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    ''' <value>_loanType</value>
    Public Property loanType As LoanType

    ''' <summary>
    ''' Próxima fecha de pago
    ''' </summary>
    ''' <remarks>Formato día/mes/año con el mes con tres letras (ej.: 16/mar/2010)</remarks>
    Public Property nextPaymentDate As Date

    ''' <summary>
    ''' Cuota Mensual
    ''' </summary>
    Public Property monthlyAmount() As Decimal

    ''' <summary>
    ''' Monto en atraso
    ''' </summary><remarks>Separador de miles es la coma y el de decimales es el punto. (ej.: 21,500.70)</remarks>
    Public Property overdueAmount() As Decimal

    ''' <summary>
    ''' Moneda de la Transaccion
    ''' </summary>
    Public Property Currency As Currencies

    ''' <summary>
    ''' Fecha de último pago
    ''' </summary>
    Public Property lastPaymentDate As Date

    ''' <summary>
    ''' Monto del último pago
    ''' </summary>
    ''' <remarks>Monto del último pago</remarks>
    Public Property lastPaymentAmount As Decimal

    ''' <summary>
    ''' Fecha de desembolso
    ''' </summary>
    Public Property disbursementDate As Date

    ''' <summary>
    ''' Monto de desembolso
    ''' </summary>
    Public Property disbursementAmount As Decimal

    ''' <summary>
    ''' Saldo de cancelación
    ''' </summary>
    Public Property cancellationBal As Decimal

    ''' <summary>
    ''' Tasa
    ''' </summary>
    Public Property interestRate As Decimal

    ''' <summary>
    ''' Plazo
    ''' </summary>
    Public Property loanTerm As Short

    ''' <summary>
    ''' Pagos realizados
    ''' </summary>
    Public Property paymentsMade As Short

    ''' <summary>
    ''' Cuenta a debitar
    ''' </summary>
    Public Property debitAccount As String

    ''' <summary>
    ''' Nombre del Oficial
    ''' </summary>
    Public Property officerName As String

    ''' <summary>
    ''' Apellidos del Oficial
    ''' </summary>
    Public Property officerLastName As String

    ''' <summary>
    ''' email del Oficial
    ''' </summary>
    Public Property officermail As String

End Class
