﻿Imports AChannel.Core.Enumerations

Public Class AccountDetail

    ''' <summary>
    ''' Typo de Producto
    ''' </summary>
    ''' <value>_productId</value>
    Public Property ProductType As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    ''' <value>_productId</value>
    <HashInclude(0)> _
    Public Property ProductId As String

    ''' <summary>
    ''' Moneda de la Transaccion
    ''' </summary>
    Public Property Currency As Currencies

    ''' <summary>
    ''' Si es cuenta Mancomunada
    ''' </summary>
    ''' <remarks>Si la cuenta es mancomunada devuelve true, de lo contrario false</remarks>
    Public Property JointAccount() As Boolean

    ''' <summary>
    ''' Balance Disponible
    ''' </summary>
    Public Property AvailableBal() As Decimal

    ''' <summary>
    ''' Saldo Total
    ''' </summary>
    Public Property TotalAmount() As Decimal

    ''' <summary>
    ''' Monto Embargado
    ''' </summary>
    Public Property seizedAmount As Decimal

    ''' <summary>
    ''' Monto pignorado
    ''' </summary>
    ''' <remarks>Monto pignorado</remarks>
    Public Property pledgedAmount As Decimal

    ''' <summary>
    ''' unpaid Interests
    ''' </summary>
    Public Property unpaidInterests As Decimal

    ''' <summary>
    ''' receivable Balance
    ''' </summary>
    Public Property receivableBal As Decimal

    ''' <summary>
    ''' Nombre del Oficial
    ''' </summary>
    Public Property officerName As String

    ''' <summary>
    ''' Apellidos del Oficial
    ''' </summary>
    Public Property officerLastName As String

    ''' <summary>
    ''' email del Oficial
    ''' </summary>
    Public Property officermail As String

    ''' <summary>
    ''' Transitos a 1 dia
    ''' </summary>
    Public Property transit1 As Decimal

    ''' <summary>
    ''' Transitos a 2 dia
    ''' </summary>
    Public Property transit2 As Decimal

    ''' <summary>
    ''' Transitos a 3 dia
    ''' </summary>
    Public Property transit3 As Decimal

    ''' <summary>
    ''' Transitos a 4 dia
    ''' </summary>
    Public Property transit4 As Decimal

    ''' <summary>
    ''' Transitos a 5 dia
    ''' </summary>
    Public Property transit5 As Decimal

    ''' <summary>
    ''' Transitos a N dia
    ''' </summary>
    Public Property transitN As Decimal

End Class
