﻿
Public Class FilterExpr


    Public Property TransactionId() As Integer

    Public Property DateBegin() As Date

    Public Property DateEnd() As Date

    Public Property MinAmount() As Decimal

    Public Property MaxAmount() As Decimal

    Public Property TransactionType() As String

    Public Property Count() As Short

End Class
