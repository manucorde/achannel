﻿Imports AChannel.Core.Enumerations

Public Class MassTransferResult

    ''' <summary>
    ''' transferId
    ''' </summary>
    ''' <value>transferId</value>
    Public Property transferId As Long

    ''' <summary>
    ''' estatus de la transferencia
    ''' </summary>
    Public Property statusId As Integer

End Class
