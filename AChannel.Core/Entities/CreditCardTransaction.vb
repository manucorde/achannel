﻿Imports AChannel.Core.Enumerations

Public Class CreditCardTransaction

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    ''' <value>_productId</value>
    <HashInclude(0)> _
    Public Property ProductId As String

    ''' <summary>
    ''' ID de la Transaccion
    ''' </summary>
    <HashInclude(1)> _
    Public Property TransactionId As Long

    ''' <summary>
    ''' Fecha de la Transaccion
    ''' </summary>
    ''' <remarks>El formato debe ser día/mes/año, con el mes con tres letras (ej: 16/mar/10)</remarks>
    Public Property TransactionDate As Date

    ''' <summary>
    ''' Descripción del movimiento
    ''' </summary>
    Public Property Description As String

    ''' <summary>
    ''' Nombre de Tarjetahabiente
    ''' </summary>
    Public Property cardHolder As String

    ''' <summary>
    ''' Monto de la Transaccion
    ''' </summary>
    <HashInclude(2)> _
    Public Property Amount As Decimal

    ''' <summary>
    ''' Debito o Credito
    ''' </summary>
    ''' <remarks>
    ''' 0 debito (-) 1 crédito (+) 
    ''' Se utiliza para mostrar amount
    ''' </remarks>
    ''' <value>0</value>
    Public Property DebitOrCredit As Short

    ''' <summary>
    ''' Moneda de la Transaccion
    ''' </summary>
    Public Property Currency As Currencies

End Class
