﻿Imports AChannel.Core.Enumerations

Public Class ALoyalAccount

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property productType() As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    Public Property productId() As String

    ''' <summary>
    ''' Saldo Total
    ''' </summary>
    Public Property totalAmount() As Decimal

End Class
