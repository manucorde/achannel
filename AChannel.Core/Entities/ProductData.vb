﻿Imports AChannel.Core.Enumerations

Public Class ProductData

    ''' <summary>
    ''' Tipo de Producto
    ''' </summary>
    Public Property ProductType() As ProductTypes

    ''' <summary>
    ''' Numero de Producto
    ''' </summary>
    Public Property ProductId() As String

    ''' <summary>
    ''' Moneda
    ''' </summary>
    Public Property Currency() As Currencies

    ''' <summary>
    ''' Razon Social de la Empresa
    ''' </summary>
    Public Property customerName() As String


End Class
