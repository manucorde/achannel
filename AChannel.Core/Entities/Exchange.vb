﻿Imports AChannel.Core.Enumerations

Public Class Exchange

    ''' <summary>
    ''' Tase de conversion
    ''' </summary>
    ''' <value>Tase de conversion </value>
    Public Property exchangeRate As Decimal

    ''' <summary>
    ''' Moneda destino
    ''' </summary>
    ''' <value>Moneda destino</value>
    Public Property currency As Currencies

    ''' <summary>
    ''' Monto convertido a la moneda destino
    ''' </summary>
    ''' <value>translatedAmount </value>
    Public Property translatedAmount As Decimal


End Class
