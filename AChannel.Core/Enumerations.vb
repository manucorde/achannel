﻿Imports System.ComponentModel
Imports System.Runtime.Serialization

Public Class Enumerations

    Public Enum CardPaymentType As Integer
        MinimumPayment = 0
        BalancePayment = 1
        Ninguno = 2
    End Enum

    Public Enum Currencies As Integer
        NoValue = 0
        PesoDominicano = 1
        Dolar = 2
        Euro = 3
    End Enum

    Public Enum PaymentType As Integer
        NoValue = 0
        Reinvertido = 1
        PagoCuenta = 2
        PagoCheque = 3
    End Enum

    Public Enum TransferTypes As Integer
        NoValue = 0
        IntraBank = 1
        ExtraBank = 2
        ExtraBankLBTR = 3
    End Enum

    Public Enum DocumentType As Integer
        NoValue = 0
        <Description("Cedula")> _
        Cedula = 1
        <Description("Pasaporte")> _
        Pasaporte = 2
        <Description("RNC")> _
        Rnc = 3
        <Description("Grupo Economico")> _
        Grupo = 4
    End Enum

    Public Enum TypeStatus As Integer
        NoValue = 0
        <Description("Ultimo")> _
        Ultimo = 1
        <Description("Penúltimo")> _
        Penultimo = 2
        <Description("Antepenúltimo")> _
        Antepenultimo = 3
    End Enum

    Public Enum ProductTypes As Integer
        NoValue = 0

        <Description("Cuenta Corriente")> _
        CheckingAccount = 1

        <Description("Cuenta de Ahorro")> _
        SavingAccount = 2

        <Description("Préstamo")> _
        Loan = 3

        <Description("Inversión")> _
        Investment = 4

        <Description("Tarjeta de Crédito")> _
        CreditCard = 5

        <Description("Tarjeta ExtraLímite")> _
        ExtraLimitCard = 6

        <Description("Pesos Caribe")> _
        PesosCaribe = 7
    End Enum

    Public Enum CardBrand As Integer
        NoValue = 0
        VisaLocal = 1
        VisaInternacional = 2
        VisaOro = 3
        VisaPlatinum = 4
        VisaFleet = 5
        VisaExtraLimite = 6
        VisaOroLocal = 7
        Tarjeta8 = 8
        Tarjeta9 = 9
        Tarjeta10 = 10
        Tarjeta11 = 11
        Tarjeta12 = 12
        Tarjeta13 = 13
        Tarjeta14 = 14
        Tarjeta15 = 15
        Tarjeta16 = 16
        Tarjeta17 = 17
        Tarjeta18 = 18
        Tarjeta19 = 19
        Tarjeta20 = 20
    End Enum

    Public Enum LoanType As Short
        NoValue = 0

        <Description("Autocaribe")> _
            ConsumoAutoCaribe = 1

        <Description("Personal")> _
            ConsumoPersonal = 2

        <Description("Personal")> _
            Reestructurado = 3

        <Description("Comercial")> _
            Comercial = 4

        <Description("Línea de Crédito")> _
            LineaCredito = 5

        <Description("Hipotecario")> _
            Hipotecario = 6

        <Description("Comercial")> _
             ReestructuradoComercial = 7

        <Description("Línea de Crédito")> _
            LineaPrincipal = 8

        <Description("Línea de Crédito")> _
            LineaRevolvente = 9

    End Enum

    'Public Enum Banks As Integer
    '    NoValue = 0
    '    Popular = 1
    '    BHD = 2
    '    Progreso = 3
    '    Reservas = 4
    '    Leon = 5
    '    SantaCruz = 6
    '    LopezHaro = 7
    '    BDI = 8
    '    CitiBank = 9
    '    Promerica = 10
    '    ScotiaBank = 11
    '    Vimenca = 12
    '    AsocPopular = 13
    '    AsocCibao = 14
    '    Caribe = 35
    'End Enum

    Public Enum CheckLockReason As Integer
        NoValue = 0
        <Description("Cheque Perdido")> _
        CheckLost = 1
        <Description("Otras Razones")> _
        OtherReason = 2
        Ninguno = 3
    End Enum

    Public Enum ViaType As Integer
        Internet_Banking = 1
    End Enum

End Class
