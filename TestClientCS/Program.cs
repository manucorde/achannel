﻿using System;
using System.Configuration;
using System.Text;
using 

namespace TestClientCS
{
	class Program
	{
		static void Main(string[] args)
		{
			String Cedula = "001-0526987-2";
			if (args.Length > 0)
			{
				Cedula = args[0];
			};

			AChannel.DocumentType TipoDoc = AChannel.DocumentType.Cedula;

			AChannel.IntegrationWSSoapClient svc = new AChannel.IntegrationWSSoapClient();
			svc.Open();

			AChannel.ResponseHeader _responseHeader = null;
			AChannel.getAccountsRequest _request = new AChannel.getAccountsRequest();
			_request.customerDocNumber = Cedula;
			_request.customerDocType = TipoDoc;

			_request.RequestHeader = new AChannel.RequestHeader();
			_request.RequestHeader.IPUser = "127.0.0.1";
			_request.RequestHeader.MarchantKey = "ABCDEF";
			_request.RequestHeader.UserID = "jSanchez";
			_request.RequestHeader.SessionID = "jkhsuUIYKjkjh298LKJKs==";
			SetMessageKey(_request.RequestHeader, _request.customerDocNumber);

			TestClientCS.AChannel.Account[] getAccountsResult = null;

			_responseHeader = svc.getAccounts(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, out  getAccountsResult);

			if (validateResponseHeader(_responseHeader))
			{
				foreach (AChannel.Account acc in getAccountsResult)
				{
					Console.WriteLine(String.Format("  >> Cuenta:{0} Disponible:{1:c} Total:{2:c}", acc.ProductId, acc.AvailableBal, acc.TotalAmount));
				};
			}
			else
			{
				Console.WriteLine(String.Format("Respuesta no valida. MessageKey:{0}", _responseHeader.MessageKey));
			}

			AChannel.ImageCheck _ImageCheck = new AChannel.ImageCheck();

			System.DateTime _fecha = new System.DateTime(2010,1,1);
			TestClientCS.AChannel.ProductTypes _productType = AChannel.ProductTypes.CreditCard;

			SetMessageKey(_request.RequestHeader, _request.customerDocNumber + "1"+"1"+_fecha.ToString("dd/MM/yyyy"));
			_responseHeader = svc.getImageCheck(_request.RequestHeader, _request.customerDocType, _request.customerDocNumber, _productType, "1", "1", _fecha, out _ImageCheck);
			Console.WriteLine(string.Format("  >> Producto:{0} Imagen Anverso:{1} Imagen Reverso:{2}", "cd", _ImageCheck.UrlCheckImageObverse, _ImageCheck.UrlCheckImageReverse));

			svc.Close();
			Console.WriteLine("Presione cualquier tecla");
			Console.ReadLine();
		}

		private static bool validateResponseHeader(AChannel.ResponseHeader header)
		{
			string salt = ConfigurationManager.AppSettings["HashSalt"].ToString();
			byte[] saltBytes = Encoding.Default.GetBytes(salt);

			string messageString = header.OperationID.ToString() +
									header.OperationResult.ToString() + header.OperationDescription +
									header.MarchantKey + header.UserID.ToString() +
									header.IPUser + header.SessionID;
			//+ HashPropertyReflector.GetHashFromArray(responseFields);

			string calculatedHash = SHA256Wrapper.ComputeHash(messageString, saltBytes);

			return calculatedHash == header.MessageKey;
		}

		/// <summary>
		/// Computes the hash value to be sent on the request header 
		/// and sets the value of the response object
		/// </summary>
		private static void SetMessageKey(AChannel.RequestHeader requestHeader, string contentHash)
		{
			string salt = ConfigurationManager.AppSettings["HashSalt"].ToString();
			byte[] saltBytes = Encoding.Default.GetBytes(salt);

			string valueToHash = requestHeader.MarchantKey +
									requestHeader.UserID + requestHeader.IPUser +
									requestHeader.SessionID + contentHash;

			requestHeader.MessageKey = SHA256Wrapper.ComputeHash(valueToHash, saltBytes);

			Console.WriteLine("Message key:{0}", requestHeader.MessageKey);
		}
	}
}
