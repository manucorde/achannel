﻿Imports AChannel.Support
Imports log4net
Imports System.Web.Script.Serialization

Module Module1
    Dim _responseHeader As AChannel.ResponseHeader
    Dim _request As New AChannel.getAccountsRequest
    Dim svc As New AChannel.IntegrationWSSoapClient()
    Private js As JavaScriptSerializer = New JavaScriptSerializer()

    Private ReadOnly _customerDocType As AChannel.DocumentType = CShort(Web.Configuration.WebConfigurationManager.AppSettings("customerDocType"))
    Private ReadOnly _customerDocNumber As String = Web.Configuration.WebConfigurationManager.AppSettings("customerDocNumber")
    Private ReadOnly _sourceProductType As AChannel.ProductTypes = CShort(Web.Configuration.WebConfigurationManager.AppSettings("sourceProductType"))
    Private ReadOnly _sourceProductId As String = Web.Configuration.WebConfigurationManager.AppSettings("sourceProductId")
    Private ReadOnly _amount As Decimal = CType(Web.Configuration.WebConfigurationManager.AppSettings("amount"), Decimal)
    Private ReadOnly _currency As AChannel.Currencies = CShort(Web.Configuration.WebConfigurationManager.AppSettings("currency"))
    Private ReadOnly _concept As String = "TestClientDebito Transaction"
    Private ReadOnly _rubroCode As Integer = CInt(Web.Configuration.WebConfigurationManager.AppSettings("rubroCode"))

    Private ReadOnly _ipuser As String = Web.Configuration.WebConfigurationManager.AppSettings("IPUser")
    Private ReadOnly _marchantkey As String = Web.Configuration.WebConfigurationManager.AppSettings("MarchantKey")
    Private ReadOnly _userid As String = Web.Configuration.WebConfigurationManager.AppSettings("UserID")
    Private ReadOnly _sessionid As String = Web.Configuration.WebConfigurationManager.AppSettings("SessionID")

    Sub Main()
        LogManager.GetLogger("root")
        Try
            Logger.Info("------ INICIO -------")

            svc.Open()

            ''Set RequestHeader
            With _request
                .customerDocNumber = _customerDocNumber
                .customerDocType = _customerDocType

                .RequestHeader = New AChannel.RequestHeader
                .RequestHeader.IPUser = _ipuser
                .RequestHeader.MarchantKey = _marchantkey
                .RequestHeader.UserID = _userid
                .RequestHeader.SessionID = _sessionid
            End With

            DebitTransac(_customerDocType, _customerDocNumber, _sourceProductType, _sourceProductId, _amount, _currency, _concept, _rubroCode)
            'Console.ReadLine()

            svc.Close()
        Catch ex As Exception
            Logger.Info(String.Format("Error: Exception:{0} StackTrace:{1}", ex.Message, ex.StackTrace))
        End Try
        Logger.Info("------ FIN -------")

    End Sub

    Private Sub DebitTransac(ByVal customerDocType As AChannel.DocumentType, ByVal customerDocNumber As String, ByVal sourceProductType As AChannel.ProductTypes, ByVal sourceProductId As String, ByVal amount As Decimal, ByVal currency As AChannel.Currencies, ByVal concept As String, ByVal RubroCode As Integer)
        SetMessageKey(New Object(4) {customerDocNumber, sourceProductId, amount, currency, RubroCode}, _request.RequestHeader)

        Logger.Info("-------------------- EJECUTANDO TRANSFER -----------------")
        Logger.InfoFormat("Ejecutando: DebitTransac(RequestHeader={0}, customerDocType={1}, customerDocNumber={2}, sourceProductType={3}, sourceProductId={4}, amount={5}, currency={6}, concept={7}, RubroCode={8})", js.Serialize(_request), customerDocType, customerDocNumber, sourceProductType, sourceProductId, amount, currency, concept, RubroCode)

        Dim _result As Long = 99

        _responseHeader = svc.DebitTransac(_request.RequestHeader, customerDocType, customerDocNumber, sourceProductType, sourceProductId, amount, currency, concept, RubroCode, _result)

        Logger.InfoFormat("  >> Respuesta recibida. MessageKey:{0} Descripcion:{1} Resultado:{2}", _responseHeader.MessageKey, _responseHeader.OperationDescription, _responseHeader.OperationResult)
        If ValidateResponseHeader(_responseHeader) Then
            Logger.Info("  >> Respuesta Validada")
            Logger.InfoFormat("  >> Resultado={0}", _result)
        Else
            Logger.InfoFormat("Mensaje de Respuesta Invalido. {0}", _result)
        End If
    End Sub
End Module
