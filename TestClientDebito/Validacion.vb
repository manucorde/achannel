﻿Imports System.Configuration
Imports Agilisa.Security
Imports AChannel.Support

Module Validacion

    Public Sub SetMessageKey(ByVal contentFields As Object(), ByVal header As AChannel.RequestHeader)
        Dim salt As String = Web.Configuration.WebConfigurationManager.AppSettings("HashSalt").ToString()
        Dim valueToHash As String = header.MarchantKey + header.UserID + header.IPUser + header.SessionID + HashPropertyReflector.GetHashFromArray(contentFields)
        Logger.DebugFormat("Valor concatenado:{0} Salt:{1}", valueToHash, salt)

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(valueToHash), New Encryption.Data(salt))
        header.MessageKey = _security.Value.ToBase64
        Logger.DebugFormat("MessageKey:{0}", header.MessageKey)
    End Sub

    Public Function ValidateResponseHeader(ByVal contentFields As Object(), ByVal header As AChannel.ResponseHeader) As Boolean

        If header Is Nothing Then Throw New Exception("Invalid headers")

        Dim salt As String = Web.Configuration.WebConfigurationManager.AppSettings("HashSalt").ToString()
        Dim saltBytes As Byte() = System.Text.Encoding.[Default].GetBytes(salt)

        Dim valueToHash As String = header.MarchantKey + header.UserID + header.IPUser + header.SessionID + HashPropertyReflector.GetHashFromArray(contentFields)

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(valueToHash), New Encryption.Data(salt))
        Dim calculatedMessageKey As String = _security.Value.ToBase64

        Return header.MessageKey = calculatedMessageKey
    End Function

    Public Function ValidateResponseHeader(ByVal header As AChannel.ResponseHeader) As Boolean

        If header Is Nothing Then Throw New Exception("Invalid headers")

        Dim salt As String = Web.Configuration.WebConfigurationManager.AppSettings("HashSalt").ToString()
        Dim saltBytes As Byte() = System.Text.Encoding.[Default].GetBytes(salt)

        Dim valueToHash As String = header.OperationID.ToString() + header.OperationResult.ToString() + header.OperationDescription + header.MarchantKey + header.UserID.ToString() + header.IPUser + header.SessionID '+ HashPropertyReflector.GetHashFromArray(New Object(0) {header.OperationID})

        Dim _security As New Agilisa.Security.Encryption.Hash(Agilisa.Security.Encryption.Hash.Provider.SHA256)
        _security.Calculate(New Encryption.Data(valueToHash), New Encryption.Data(salt))
        Dim calculatedMessageKey As String = _security.Value.ToBase64

        Logger.InfoFormat("value={0}", valueToHash)
        Logger.InfoFormat("Message Key Calculado={0}", calculatedMessageKey)
        Logger.InfoFormat("Message Key Recibido={0}", header.MessageKey)
        
        Return header.MessageKey = calculatedMessageKey

    End Function

End Module
